# API2Cart


- API version: 1.1


## Requirements

PHP 5.6 and above



## Installation & Usage
### Composer



- `composer require api2cart/api2cart-php-sdk`


## Documentation For Authorization


## api_key

- **Type**: API key
- **API key parameter name**: api_key
- **Location**: URL query string

## store_key

- **Type**: API key
- **API key parameter name**: store_key
- **Location**: URL query string




## Documentation for API Endpoints

All URIs are relative to *https://api.api2cart.com/v1.1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountApi* | [**accountCartAdd**](src/docs/Api/AccountApi.md#accountcartadd) | **POST** /account.cart.add.json | 
*AccountApi* | [**accountCartList**](src/docs/Api/AccountApi.md#accountcartlist) | **GET** /account.cart.list.json | 
*AccountApi* | [**accountConfigUpdate**](src/docs/Api/AccountApi.md#accountconfigupdate) | **PUT** /account.config.update.json | 
*AccountApi* | [**accountFailedWebhooks**](src/docs/Api/AccountApi.md#accountfailedwebhooks) | **GET** /account.failed_webhooks.json | 
*AccountApi* | [**accountSupportedPlatforms**](src/docs/Api/AccountApi.md#accountsupportedplatforms) | **GET** /account.supported_platforms.json | 
*AttributeApi* | [**attributeAdd**](src/docs/Api/AttributeApi.md#attributeadd) | **POST** /attribute.add.json | 
*AttributeApi* | [**attributeAssignGroup**](src/docs/Api/AttributeApi.md#attributeassigngroup) | **POST** /attribute.assign.group.json | 
*AttributeApi* | [**attributeAssignSet**](src/docs/Api/AttributeApi.md#attributeassignset) | **POST** /attribute.assign.set.json | 
*AttributeApi* | [**attributeAttributesetList**](src/docs/Api/AttributeApi.md#attributeattributesetlist) | **GET** /attribute.attributeset.list.json | 
*AttributeApi* | [**attributeCount**](src/docs/Api/AttributeApi.md#attributecount) | **GET** /attribute.count.json | 
*AttributeApi* | [**attributeDelete**](src/docs/Api/AttributeApi.md#attributedelete) | **DELETE** /attribute.delete.json | 
*AttributeApi* | [**attributeGroupList**](src/docs/Api/AttributeApi.md#attributegrouplist) | **GET** /attribute.group.list.json | 
*AttributeApi* | [**attributeInfo**](src/docs/Api/AttributeApi.md#attributeinfo) | **GET** /attribute.info.json | 
*AttributeApi* | [**attributeList**](src/docs/Api/AttributeApi.md#attributelist) | **GET** /attribute.list.json | 
*AttributeApi* | [**attributeTypeList**](src/docs/Api/AttributeApi.md#attributetypelist) | **GET** /attribute.type.list.json | 
*AttributeApi* | [**attributeUnassignGroup**](src/docs/Api/AttributeApi.md#attributeunassigngroup) | **POST** /attribute.unassign.group.json | 
*AttributeApi* | [**attributeUnassignSet**](src/docs/Api/AttributeApi.md#attributeunassignset) | **POST** /attribute.unassign.set.json | 
*BasketApi* | [**basketInfo**](src/docs/Api/BasketApi.md#basketinfo) | **GET** /basket.info.json | 
*BasketApi* | [**basketItemAdd**](src/docs/Api/BasketApi.md#basketitemadd) | **POST** /basket.item.add.json | 
*BasketApi* | [**basketLiveShippingServiceCreate**](src/docs/Api/BasketApi.md#basketliveshippingservicecreate) | **POST** /basket.live_shipping_service.create.json | 
*BasketApi* | [**basketLiveShippingServiceDelete**](src/docs/Api/BasketApi.md#basketliveshippingservicedelete) | **DELETE** /basket.live_shipping_service.delete.json | 
*BasketApi* | [**basketLiveShippingServiceList**](src/docs/Api/BasketApi.md#basketliveshippingservicelist) | **GET** /basket.live_shipping_service.list.json | 
*BridgeApi* | [**bridgeDelete**](src/docs/Api/BridgeApi.md#bridgedelete) | **POST** /bridge.delete.json | 
*BridgeApi* | [**bridgeUpdate**](src/docs/Api/BridgeApi.md#bridgeupdate) | **POST** /bridge.update.json | 
*CartApi* | [**bridgeDownload**](src/docs/Api/CartApi.md#bridgedownload) | **GET** /bridge.download.file | 
*CartApi* | [**cartBridge**](src/docs/Api/CartApi.md#cartbridge) | **GET** /cart.bridge.json | 
*CartApi* | [**cartClearCache**](src/docs/Api/CartApi.md#cartclearcache) | **POST** /cart.clear_cache.json | 
*CartApi* | [**cartConfig**](src/docs/Api/CartApi.md#cartconfig) | **GET** /cart.config.json | 
*CartApi* | [**cartConfigUpdate**](src/docs/Api/CartApi.md#cartconfigupdate) | **PUT** /cart.config.update.json | 
*CartApi* | [**cartCouponAdd**](src/docs/Api/CartApi.md#cartcouponadd) | **POST** /cart.coupon.add.json | 
*CartApi* | [**cartCouponConditionAdd**](src/docs/Api/CartApi.md#cartcouponconditionadd) | **POST** /cart.coupon.condition.add.json | 
*CartApi* | [**cartCouponCount**](src/docs/Api/CartApi.md#cartcouponcount) | **GET** /cart.coupon.count.json | 
*CartApi* | [**cartCouponDelete**](src/docs/Api/CartApi.md#cartcoupondelete) | **DELETE** /cart.coupon.delete.json | 
*CartApi* | [**cartCouponList**](src/docs/Api/CartApi.md#cartcouponlist) | **GET** /cart.coupon.list.json | 
*CartApi* | [**cartCreate**](src/docs/Api/CartApi.md#cartcreate) | **POST** /cart.create.json | 
*CartApi* | [**cartDelete**](src/docs/Api/CartApi.md#cartdelete) | **DELETE** /cart.delete.json | 
*CartApi* | [**cartDisconnect**](src/docs/Api/CartApi.md#cartdisconnect) | **GET** /cart.disconnect.json | 
*CartApi* | [**cartGiftcardAdd**](src/docs/Api/CartApi.md#cartgiftcardadd) | **POST** /cart.giftcard.add.json | 
*CartApi* | [**cartGiftcardCount**](src/docs/Api/CartApi.md#cartgiftcardcount) | **GET** /cart.giftcard.count.json | 
*CartApi* | [**cartGiftcardList**](src/docs/Api/CartApi.md#cartgiftcardlist) | **GET** /cart.giftcard.list.json | 
*CartApi* | [**cartInfo**](src/docs/Api/CartApi.md#cartinfo) | **GET** /cart.info.json | 
*CartApi* | [**cartList**](src/docs/Api/CartApi.md#cartlist) | **GET** /cart.list.json | 
*CartApi* | [**cartMethods**](src/docs/Api/CartApi.md#cartmethods) | **GET** /cart.methods.json | 
*CartApi* | [**cartPluginList**](src/docs/Api/CartApi.md#cartpluginlist) | **GET** /cart.plugin.list.json | 
*CartApi* | [**cartScriptAdd**](src/docs/Api/CartApi.md#cartscriptadd) | **POST** /cart.script.add.json | 
*CartApi* | [**cartScriptDelete**](src/docs/Api/CartApi.md#cartscriptdelete) | **DELETE** /cart.script.delete.json | 
*CartApi* | [**cartScriptList**](src/docs/Api/CartApi.md#cartscriptlist) | **GET** /cart.script.list.json | 
*CartApi* | [**cartShippingZonesList**](src/docs/Api/CartApi.md#cartshippingzoneslist) | **GET** /cart.shipping_zones.list.json | 
*CartApi* | [**cartValidate**](src/docs/Api/CartApi.md#cartvalidate) | **GET** /cart.validate.json | 
*CategoryApi* | [**categoryAdd**](src/docs/Api/CategoryApi.md#categoryadd) | **POST** /category.add.json | 
*CategoryApi* | [**categoryAssign**](src/docs/Api/CategoryApi.md#categoryassign) | **POST** /category.assign.json | 
*CategoryApi* | [**categoryCount**](src/docs/Api/CategoryApi.md#categorycount) | **GET** /category.count.json | 
*CategoryApi* | [**categoryDelete**](src/docs/Api/CategoryApi.md#categorydelete) | **DELETE** /category.delete.json | 
*CategoryApi* | [**categoryFind**](src/docs/Api/CategoryApi.md#categoryfind) | **GET** /category.find.json | 
*CategoryApi* | [**categoryImageAdd**](src/docs/Api/CategoryApi.md#categoryimageadd) | **POST** /category.image.add.json | 
*CategoryApi* | [**categoryImageDelete**](src/docs/Api/CategoryApi.md#categoryimagedelete) | **DELETE** /category.image.delete.json | 
*CategoryApi* | [**categoryInfo**](src/docs/Api/CategoryApi.md#categoryinfo) | **GET** /category.info.json | 
*CategoryApi* | [**categoryList**](src/docs/Api/CategoryApi.md#categorylist) | **GET** /category.list.json | 
*CategoryApi* | [**categoryUnassign**](src/docs/Api/CategoryApi.md#categoryunassign) | **POST** /category.unassign.json | 
*CategoryApi* | [**categoryUpdate**](src/docs/Api/CategoryApi.md#categoryupdate) | **PUT** /category.update.json | 
*CustomerApi* | [**customerAdd**](src/docs/Api/CustomerApi.md#customeradd) | **POST** /customer.add.json | 
*CustomerApi* | [**customerCount**](src/docs/Api/CustomerApi.md#customercount) | **GET** /customer.count.json | 
*CustomerApi* | [**customerFind**](src/docs/Api/CustomerApi.md#customerfind) | **GET** /customer.find.json | 
*CustomerApi* | [**customerGroupAdd**](src/docs/Api/CustomerApi.md#customergroupadd) | **POST** /customer.group.add.json | 
*CustomerApi* | [**customerGroupList**](src/docs/Api/CustomerApi.md#customergrouplist) | **GET** /customer.group.list.json | 
*CustomerApi* | [**customerInfo**](src/docs/Api/CustomerApi.md#customerinfo) | **GET** /customer.info.json | 
*CustomerApi* | [**customerList**](src/docs/Api/CustomerApi.md#customerlist) | **GET** /customer.list.json | 
*OrderApi* | [**orderAbandonedList**](src/docs/Api/OrderApi.md#orderabandonedlist) | **GET** /order.abandoned.list.json | 
*OrderApi* | [**orderAdd**](src/docs/Api/OrderApi.md#orderadd) | **POST** /order.add.json | 
*OrderApi* | [**orderCount**](src/docs/Api/OrderApi.md#ordercount) | **GET** /order.count.json | 
*OrderApi* | [**orderFinancialStatusList**](src/docs/Api/OrderApi.md#orderfinancialstatuslist) | **GET** /order.financial_status.list.json | 
*OrderApi* | [**orderFind**](src/docs/Api/OrderApi.md#orderfind) | **GET** /order.find.json | 
*OrderApi* | [**orderInfo**](src/docs/Api/OrderApi.md#orderinfo) | **GET** /order.info.json | 
*OrderApi* | [**orderList**](src/docs/Api/OrderApi.md#orderlist) | **GET** /order.list.json | 
*OrderApi* | [**orderRefundAdd**](src/docs/Api/OrderApi.md#orderrefundadd) | **POST** /order.refund.add.json | 
*OrderApi* | [**orderShipmentAdd**](src/docs/Api/OrderApi.md#ordershipmentadd) | **POST** /order.shipment.add.json | 
*OrderApi* | [**orderShipmentList**](src/docs/Api/OrderApi.md#ordershipmentlist) | **GET** /order.shipment.list.json | 
*OrderApi* | [**orderShipmentTrackingAdd**](src/docs/Api/OrderApi.md#ordershipmenttrackingadd) | **POST** /order.shipment.tracking.add.json | 
*OrderApi* | [**orderShipmentUpdate**](src/docs/Api/OrderApi.md#ordershipmentupdate) | **PUT** /order.shipment.update.json | 
*OrderApi* | [**orderStatusList**](src/docs/Api/OrderApi.md#orderstatuslist) | **GET** /order.status.list.json | 
*OrderApi* | [**orderUpdate**](src/docs/Api/OrderApi.md#orderupdate) | **PUT** /order.update.json | 
*ProductApi* | [**productAdd**](src/docs/Api/ProductApi.md#productadd) | **POST** /product.add.json | 
*ProductApi* | [**productAttributeList**](src/docs/Api/ProductApi.md#productattributelist) | **GET** /product.attribute.list.json | 
*ProductApi* | [**productAttributeValueSet**](src/docs/Api/ProductApi.md#productattributevalueset) | **POST** /product.attribute.value.set.json | 
*ProductApi* | [**productAttributeValueUnset**](src/docs/Api/ProductApi.md#productattributevalueunset) | **POST** /product.attribute.value.unset.json | 
*ProductApi* | [**productBrandList**](src/docs/Api/ProductApi.md#productbrandlist) | **GET** /product.brand.list.json | 
*ProductApi* | [**productChildItemFind**](src/docs/Api/ProductApi.md#productchilditemfind) | **GET** /product.child_item.find.json | 
*ProductApi* | [**productChildItemInfo**](src/docs/Api/ProductApi.md#productchilditeminfo) | **GET** /product.child_item.info.json | 
*ProductApi* | [**productChildItemList**](src/docs/Api/ProductApi.md#productchilditemlist) | **GET** /product.child_item.list.json | 
*ProductApi* | [**productCount**](src/docs/Api/ProductApi.md#productcount) | **GET** /product.count.json | 
*ProductApi* | [**productCurrencyAdd**](src/docs/Api/ProductApi.md#productcurrencyadd) | **POST** /product.currency.add.json | 
*ProductApi* | [**productCurrencyList**](src/docs/Api/ProductApi.md#productcurrencylist) | **GET** /product.currency.list.json | 
*ProductApi* | [**productDelete**](src/docs/Api/ProductApi.md#productdelete) | **DELETE** /product.delete.json | 
*ProductApi* | [**productFields**](src/docs/Api/ProductApi.md#productfields) | **GET** /product.fields.json | 
*ProductApi* | [**productFind**](src/docs/Api/ProductApi.md#productfind) | **GET** /product.find.json | 
*ProductApi* | [**productImageAdd**](src/docs/Api/ProductApi.md#productimageadd) | **POST** /product.image.add.json | 
*ProductApi* | [**productImageDelete**](src/docs/Api/ProductApi.md#productimagedelete) | **DELETE** /product.image.delete.json | 
*ProductApi* | [**productImageUpdate**](src/docs/Api/ProductApi.md#productimageupdate) | **PUT** /product.image.update.json | 
*ProductApi* | [**productInfo**](src/docs/Api/ProductApi.md#productinfo) | **GET** /product.info.json | 
*ProductApi* | [**productList**](src/docs/Api/ProductApi.md#productlist) | **GET** /product.list.json | 
*ProductApi* | [**productManufacturerAdd**](src/docs/Api/ProductApi.md#productmanufactureradd) | **POST** /product.manufacturer.add.json | 
*ProductApi* | [**productOptionAdd**](src/docs/Api/ProductApi.md#productoptionadd) | **POST** /product.option.add.json | 
*ProductApi* | [**productOptionAssign**](src/docs/Api/ProductApi.md#productoptionassign) | **POST** /product.option.assign.json | 
*ProductApi* | [**productOptionList**](src/docs/Api/ProductApi.md#productoptionlist) | **GET** /product.option.list.json | 
*ProductApi* | [**productOptionValueAdd**](src/docs/Api/ProductApi.md#productoptionvalueadd) | **POST** /product.option.value.add.json | 
*ProductApi* | [**productOptionValueAssign**](src/docs/Api/ProductApi.md#productoptionvalueassign) | **POST** /product.option.value.assign.json | 
*ProductApi* | [**productOptionValueUpdate**](src/docs/Api/ProductApi.md#productoptionvalueupdate) | **PUT** /product.option.value.update.json | 
*ProductApi* | [**productPriceAdd**](src/docs/Api/ProductApi.md#productpriceadd) | **POST** /product.price.add.json | 
*ProductApi* | [**productPriceDelete**](src/docs/Api/ProductApi.md#productpricedelete) | **DELETE** /product.price.delete.json | 
*ProductApi* | [**productPriceUpdate**](src/docs/Api/ProductApi.md#productpriceupdate) | **PUT** /product.price.update.json | 
*ProductApi* | [**productReviewList**](src/docs/Api/ProductApi.md#productreviewlist) | **GET** /product.review.list.json | 
*ProductApi* | [**productStoreAssign**](src/docs/Api/ProductApi.md#productstoreassign) | **POST** /product.store.assign.json | 
*ProductApi* | [**productTaxAdd**](src/docs/Api/ProductApi.md#producttaxadd) | **POST** /product.tax.add.json | 
*ProductApi* | [**productUpdate**](src/docs/Api/ProductApi.md#productupdate) | **PUT** /product.update.json | 
*ProductApi* | [**productVariantAdd**](src/docs/Api/ProductApi.md#productvariantadd) | **POST** /product.variant.add.json | 
*ProductApi* | [**productVariantCount**](src/docs/Api/ProductApi.md#productvariantcount) | **GET** /product.variant.count.json | 
*ProductApi* | [**productVariantDelete**](src/docs/Api/ProductApi.md#productvariantdelete) | **DELETE** /product.variant.delete.json | 
*ProductApi* | [**productVariantInfo**](src/docs/Api/ProductApi.md#productvariantinfo) | **GET** /product.variant.info.json | 
*ProductApi* | [**productVariantList**](src/docs/Api/ProductApi.md#productvariantlist) | **GET** /product.variant.list.json | 
*ProductApi* | [**productVariantPriceAdd**](src/docs/Api/ProductApi.md#productvariantpriceadd) | **POST** /product.variant.price.add.json | 
*ProductApi* | [**productVariantPriceDelete**](src/docs/Api/ProductApi.md#productvariantpricedelete) | **DELETE** /product.variant.price.delete.json | 
*ProductApi* | [**productVariantPriceUpdate**](src/docs/Api/ProductApi.md#productvariantpriceupdate) | **PUT** /product.variant.price.update.json | 
*ProductApi* | [**productVariantUpdate**](src/docs/Api/ProductApi.md#productvariantupdate) | **PUT** /product.variant.update.json | 
*SubscriberApi* | [**subscriberList**](src/docs/Api/SubscriberApi.md#subscriberlist) | **GET** /subscriber.list.json | 
*TaxApi* | [**taxClassInfo**](src/docs/Api/TaxApi.md#taxclassinfo) | **GET** /tax.class.info.json | 
*WebhookApi* | [**webhookCount**](src/docs/Api/WebhookApi.md#webhookcount) | **GET** /webhook.count.json | 
*WebhookApi* | [**webhookCreate**](src/docs/Api/WebhookApi.md#webhookcreate) | **POST** /webhook.create.json | 
*WebhookApi* | [**webhookDelete**](src/docs/Api/WebhookApi.md#webhookdelete) | **DELETE** /webhook.delete.json | 
*WebhookApi* | [**webhookEvents**](src/docs/Api/WebhookApi.md#webhookevents) | **GET** /webhook.events.json | 
*WebhookApi* | [**webhookList**](src/docs/Api/WebhookApi.md#webhooklist) | **GET** /webhook.list.json | 
*WebhookApi* | [**webhookUpdate**](src/docs/Api/WebhookApi.md#webhookupdate) | **PUT** /webhook.update.json | 


## Documentation For Models

 - [A2CDateTime](src/docs/Model/A2CDateTime.md)
 - [BaseCustomer](src/docs/Model/BaseCustomer.md)
 - [Basket](src/docs/Model/Basket.md)
 - [BasketItem](src/docs/Model/BasketItem.md)
 - [BasketItemOption](src/docs/Model/BasketItemOption.md)
 - [BasketLiveShippingService](src/docs/Model/BasketLiveShippingService.md)
 - [Brand](src/docs/Model/Brand.md)
 - [Carrier](src/docs/Model/Carrier.md)
 - [Cart](src/docs/Model/Cart.md)
 - [CartConfigUpdate](src/docs/Model/CartConfigUpdate.md)
 - [CartCouponAdd](src/docs/Model/CartCouponAdd.md)
 - [CartShippingZone](src/docs/Model/CartShippingZone.md)
 - [CartStoreInfo](src/docs/Model/CartStoreInfo.md)
 - [CartWarehouse](src/docs/Model/CartWarehouse.md)
 - [Category](src/docs/Model/Category.md)
 - [Child](src/docs/Model/Child.md)
 - [ChildCombination](src/docs/Model/ChildCombination.md)
 - [Country](src/docs/Model/Country.md)
 - [Coupon](src/docs/Model/Coupon.md)
 - [CouponAction](src/docs/Model/CouponAction.md)
 - [CouponCode](src/docs/Model/CouponCode.md)
 - [CouponCondition](src/docs/Model/CouponCondition.md)
 - [CouponHistory](src/docs/Model/CouponHistory.md)
 - [Currency](src/docs/Model/Currency.md)
 - [Customer](src/docs/Model/Customer.md)
 - [CustomerAdd](src/docs/Model/CustomerAdd.md)
 - [CustomerAddAddress](src/docs/Model/CustomerAddAddress.md)
 - [CustomerAddress](src/docs/Model/CustomerAddress.md)
 - [CustomerGroup](src/docs/Model/CustomerGroup.md)
 - [GiftCard](src/docs/Model/GiftCard.md)
 - [Image](src/docs/Model/Image.md)
 - [Info](src/docs/Model/Info.md)
 - [InlineResponse200](src/docs/Model/InlineResponse200.md)
 - [InlineResponse2001](src/docs/Model/InlineResponse2001.md)
 - [InlineResponse20010](src/docs/Model/InlineResponse20010.md)
 - [InlineResponse20010Result](src/docs/Model/InlineResponse20010Result.md)
 - [InlineResponse20011](src/docs/Model/InlineResponse20011.md)
 - [InlineResponse20011Result](src/docs/Model/InlineResponse20011Result.md)
 - [InlineResponse20012](src/docs/Model/InlineResponse20012.md)
 - [InlineResponse20012Result](src/docs/Model/InlineResponse20012Result.md)
 - [InlineResponse20013](src/docs/Model/InlineResponse20013.md)
 - [InlineResponse20013Result](src/docs/Model/InlineResponse20013Result.md)
 - [InlineResponse20014](src/docs/Model/InlineResponse20014.md)
 - [InlineResponse20015](src/docs/Model/InlineResponse20015.md)
 - [InlineResponse20015Result](src/docs/Model/InlineResponse20015Result.md)
 - [InlineResponse20016](src/docs/Model/InlineResponse20016.md)
 - [InlineResponse20016Result](src/docs/Model/InlineResponse20016Result.md)
 - [InlineResponse20017](src/docs/Model/InlineResponse20017.md)
 - [InlineResponse20017Result](src/docs/Model/InlineResponse20017Result.md)
 - [InlineResponse20018](src/docs/Model/InlineResponse20018.md)
 - [InlineResponse20018Result](src/docs/Model/InlineResponse20018Result.md)
 - [InlineResponse20019](src/docs/Model/InlineResponse20019.md)
 - [InlineResponse20019Result](src/docs/Model/InlineResponse20019Result.md)
 - [InlineResponse2001Result](src/docs/Model/InlineResponse2001Result.md)
 - [InlineResponse2002](src/docs/Model/InlineResponse2002.md)
 - [InlineResponse20020](src/docs/Model/InlineResponse20020.md)
 - [InlineResponse20020Result](src/docs/Model/InlineResponse20020Result.md)
 - [InlineResponse20021](src/docs/Model/InlineResponse20021.md)
 - [InlineResponse20021Result](src/docs/Model/InlineResponse20021Result.md)
 - [InlineResponse20022](src/docs/Model/InlineResponse20022.md)
 - [InlineResponse20023](src/docs/Model/InlineResponse20023.md)
 - [InlineResponse20023Result](src/docs/Model/InlineResponse20023Result.md)
 - [InlineResponse20024](src/docs/Model/InlineResponse20024.md)
 - [InlineResponse20024Result](src/docs/Model/InlineResponse20024Result.md)
 - [InlineResponse20025](src/docs/Model/InlineResponse20025.md)
 - [InlineResponse20026](src/docs/Model/InlineResponse20026.md)
 - [InlineResponse20026Result](src/docs/Model/InlineResponse20026Result.md)
 - [InlineResponse20026ResultCarts](src/docs/Model/InlineResponse20026ResultCarts.md)
 - [InlineResponse20027](src/docs/Model/InlineResponse20027.md)
 - [InlineResponse20027Result](src/docs/Model/InlineResponse20027Result.md)
 - [InlineResponse20028](src/docs/Model/InlineResponse20028.md)
 - [InlineResponse20028Result](src/docs/Model/InlineResponse20028Result.md)
 - [InlineResponse20028ResultWebhook](src/docs/Model/InlineResponse20028ResultWebhook.md)
 - [InlineResponse20029](src/docs/Model/InlineResponse20029.md)
 - [InlineResponse20029Result](src/docs/Model/InlineResponse20029Result.md)
 - [InlineResponse2002Result](src/docs/Model/InlineResponse2002Result.md)
 - [InlineResponse2003](src/docs/Model/InlineResponse2003.md)
 - [InlineResponse20030](src/docs/Model/InlineResponse20030.md)
 - [InlineResponse20030Result](src/docs/Model/InlineResponse20030Result.md)
 - [InlineResponse20031](src/docs/Model/InlineResponse20031.md)
 - [InlineResponse20031Result](src/docs/Model/InlineResponse20031Result.md)
 - [InlineResponse20032](src/docs/Model/InlineResponse20032.md)
 - [InlineResponse20033](src/docs/Model/InlineResponse20033.md)
 - [InlineResponse20033Result](src/docs/Model/InlineResponse20033Result.md)
 - [InlineResponse20033ResultProduct](src/docs/Model/InlineResponse20033ResultProduct.md)
 - [InlineResponse20034](src/docs/Model/InlineResponse20034.md)
 - [InlineResponse20034Result](src/docs/Model/InlineResponse20034Result.md)
 - [InlineResponse20035](src/docs/Model/InlineResponse20035.md)
 - [InlineResponse20035Result](src/docs/Model/InlineResponse20035Result.md)
 - [InlineResponse20036](src/docs/Model/InlineResponse20036.md)
 - [InlineResponse20036Result](src/docs/Model/InlineResponse20036Result.md)
 - [InlineResponse20037](src/docs/Model/InlineResponse20037.md)
 - [InlineResponse20037Result](src/docs/Model/InlineResponse20037Result.md)
 - [InlineResponse20038](src/docs/Model/InlineResponse20038.md)
 - [InlineResponse20039](src/docs/Model/InlineResponse20039.md)
 - [InlineResponse20039Result](src/docs/Model/InlineResponse20039Result.md)
 - [InlineResponse20039ResultCategory](src/docs/Model/InlineResponse20039ResultCategory.md)
 - [InlineResponse2003Result](src/docs/Model/InlineResponse2003Result.md)
 - [InlineResponse2003ResultEvents](src/docs/Model/InlineResponse2003ResultEvents.md)
 - [InlineResponse2004](src/docs/Model/InlineResponse2004.md)
 - [InlineResponse20040](src/docs/Model/InlineResponse20040.md)
 - [InlineResponse20040Result](src/docs/Model/InlineResponse20040Result.md)
 - [InlineResponse20041](src/docs/Model/InlineResponse20041.md)
 - [InlineResponse20041Result](src/docs/Model/InlineResponse20041Result.md)
 - [InlineResponse20042](src/docs/Model/InlineResponse20042.md)
 - [InlineResponse20043](src/docs/Model/InlineResponse20043.md)
 - [InlineResponse20043Result](src/docs/Model/InlineResponse20043Result.md)
 - [InlineResponse20044](src/docs/Model/InlineResponse20044.md)
 - [InlineResponse20044Result](src/docs/Model/InlineResponse20044Result.md)
 - [InlineResponse20045](src/docs/Model/InlineResponse20045.md)
 - [InlineResponse20045Result](src/docs/Model/InlineResponse20045Result.md)
 - [InlineResponse20045ResultCartOrderStatuses](src/docs/Model/InlineResponse20045ResultCartOrderStatuses.md)
 - [InlineResponse20046](src/docs/Model/InlineResponse20046.md)
 - [InlineResponse20046Result](src/docs/Model/InlineResponse20046Result.md)
 - [InlineResponse20047](src/docs/Model/InlineResponse20047.md)
 - [InlineResponse20047Result](src/docs/Model/InlineResponse20047Result.md)
 - [InlineResponse20048](src/docs/Model/InlineResponse20048.md)
 - [InlineResponse20048Result](src/docs/Model/InlineResponse20048Result.md)
 - [InlineResponse20049](src/docs/Model/InlineResponse20049.md)
 - [InlineResponse20049Result](src/docs/Model/InlineResponse20049Result.md)
 - [InlineResponse2004Result](src/docs/Model/InlineResponse2004Result.md)
 - [InlineResponse2005](src/docs/Model/InlineResponse2005.md)
 - [InlineResponse20050](src/docs/Model/InlineResponse20050.md)
 - [InlineResponse20050Result](src/docs/Model/InlineResponse20050Result.md)
 - [InlineResponse20051](src/docs/Model/InlineResponse20051.md)
 - [InlineResponse20052](src/docs/Model/InlineResponse20052.md)
 - [InlineResponse20052Result](src/docs/Model/InlineResponse20052Result.md)
 - [InlineResponse20053](src/docs/Model/InlineResponse20053.md)
 - [InlineResponse20053Result](src/docs/Model/InlineResponse20053Result.md)
 - [InlineResponse20054](src/docs/Model/InlineResponse20054.md)
 - [InlineResponse20055](src/docs/Model/InlineResponse20055.md)
 - [InlineResponse20055Result](src/docs/Model/InlineResponse20055Result.md)
 - [InlineResponse20056](src/docs/Model/InlineResponse20056.md)
 - [InlineResponse20056Result](src/docs/Model/InlineResponse20056Result.md)
 - [InlineResponse20057](src/docs/Model/InlineResponse20057.md)
 - [InlineResponse20057Result](src/docs/Model/InlineResponse20057Result.md)
 - [InlineResponse20058](src/docs/Model/InlineResponse20058.md)
 - [InlineResponse20059](src/docs/Model/InlineResponse20059.md)
 - [InlineResponse20059Result](src/docs/Model/InlineResponse20059Result.md)
 - [InlineResponse2005Result](src/docs/Model/InlineResponse2005Result.md)
 - [InlineResponse2006](src/docs/Model/InlineResponse2006.md)
 - [InlineResponse20060](src/docs/Model/InlineResponse20060.md)
 - [InlineResponse20060Result](src/docs/Model/InlineResponse20060Result.md)
 - [InlineResponse20061](src/docs/Model/InlineResponse20061.md)
 - [InlineResponse20061Result](src/docs/Model/InlineResponse20061Result.md)
 - [InlineResponse20062](src/docs/Model/InlineResponse20062.md)
 - [InlineResponse20062Result](src/docs/Model/InlineResponse20062Result.md)
 - [InlineResponse20063](src/docs/Model/InlineResponse20063.md)
 - [InlineResponse20064](src/docs/Model/InlineResponse20064.md)
 - [InlineResponse20064Result](src/docs/Model/InlineResponse20064Result.md)
 - [InlineResponse20065](src/docs/Model/InlineResponse20065.md)
 - [InlineResponse20065Result](src/docs/Model/InlineResponse20065Result.md)
 - [InlineResponse20066](src/docs/Model/InlineResponse20066.md)
 - [InlineResponse20066Result](src/docs/Model/InlineResponse20066Result.md)
 - [InlineResponse20067](src/docs/Model/InlineResponse20067.md)
 - [InlineResponse20067Result](src/docs/Model/InlineResponse20067Result.md)
 - [InlineResponse20068](src/docs/Model/InlineResponse20068.md)
 - [InlineResponse20068Result](src/docs/Model/InlineResponse20068Result.md)
 - [InlineResponse20069](src/docs/Model/InlineResponse20069.md)
 - [InlineResponse20069Result](src/docs/Model/InlineResponse20069Result.md)
 - [InlineResponse2006Result](src/docs/Model/InlineResponse2006Result.md)
 - [InlineResponse2007](src/docs/Model/InlineResponse2007.md)
 - [InlineResponse20070](src/docs/Model/InlineResponse20070.md)
 - [InlineResponse20070Result](src/docs/Model/InlineResponse20070Result.md)
 - [InlineResponse20071](src/docs/Model/InlineResponse20071.md)
 - [InlineResponse20071Result](src/docs/Model/InlineResponse20071Result.md)
 - [InlineResponse20072](src/docs/Model/InlineResponse20072.md)
 - [InlineResponse20072Result](src/docs/Model/InlineResponse20072Result.md)
 - [InlineResponse20073](src/docs/Model/InlineResponse20073.md)
 - [InlineResponse20074](src/docs/Model/InlineResponse20074.md)
 - [InlineResponse20074Result](src/docs/Model/InlineResponse20074Result.md)
 - [InlineResponse20075](src/docs/Model/InlineResponse20075.md)
 - [InlineResponse20075Result](src/docs/Model/InlineResponse20075Result.md)
 - [InlineResponse20076](src/docs/Model/InlineResponse20076.md)
 - [InlineResponse20076Result](src/docs/Model/InlineResponse20076Result.md)
 - [InlineResponse20077](src/docs/Model/InlineResponse20077.md)
 - [InlineResponse20077Result](src/docs/Model/InlineResponse20077Result.md)
 - [InlineResponse20078](src/docs/Model/InlineResponse20078.md)
 - [InlineResponse20078Result](src/docs/Model/InlineResponse20078Result.md)
 - [InlineResponse20079](src/docs/Model/InlineResponse20079.md)
 - [InlineResponse20079Result](src/docs/Model/InlineResponse20079Result.md)
 - [InlineResponse2007Result](src/docs/Model/InlineResponse2007Result.md)
 - [InlineResponse2008](src/docs/Model/InlineResponse2008.md)
 - [InlineResponse20080](src/docs/Model/InlineResponse20080.md)
 - [InlineResponse20080Result](src/docs/Model/InlineResponse20080Result.md)
 - [InlineResponse20081](src/docs/Model/InlineResponse20081.md)
 - [InlineResponse20082](src/docs/Model/InlineResponse20082.md)
 - [InlineResponse20082Result](src/docs/Model/InlineResponse20082Result.md)
 - [InlineResponse20083](src/docs/Model/InlineResponse20083.md)
 - [InlineResponse20083Result](src/docs/Model/InlineResponse20083Result.md)
 - [InlineResponse2008Result](src/docs/Model/InlineResponse2008Result.md)
 - [InlineResponse2008ResultSupportedCarts](src/docs/Model/InlineResponse2008ResultSupportedCarts.md)
 - [InlineResponse2009](src/docs/Model/InlineResponse2009.md)
 - [InlineResponse2009Result](src/docs/Model/InlineResponse2009Result.md)
 - [InlineResponse200Result](src/docs/Model/InlineResponse200Result.md)
 - [Language](src/docs/Model/Language.md)
 - [ModelResponseCartCouponList](src/docs/Model/ModelResponseCartCouponList.md)
 - [ModelResponseCartGiftCardList](src/docs/Model/ModelResponseCartGiftCardList.md)
 - [ModelResponseCartScriptList](src/docs/Model/ModelResponseCartScriptList.md)
 - [ModelResponseCategoryList](src/docs/Model/ModelResponseCategoryList.md)
 - [ModelResponseCustomerGroupList](src/docs/Model/ModelResponseCustomerGroupList.md)
 - [ModelResponseCustomerList](src/docs/Model/ModelResponseCustomerList.md)
 - [ModelResponseOrderAbandonedList](src/docs/Model/ModelResponseOrderAbandonedList.md)
 - [ModelResponseOrderList](src/docs/Model/ModelResponseOrderList.md)
 - [ModelResponseOrderShipmentList](src/docs/Model/ModelResponseOrderShipmentList.md)
 - [ModelResponseProductAttributeList](src/docs/Model/ModelResponseProductAttributeList.md)
 - [ModelResponseProductChildItemList](src/docs/Model/ModelResponseProductChildItemList.md)
 - [ModelResponseProductList](src/docs/Model/ModelResponseProductList.md)
 - [Order](src/docs/Model/Order.md)
 - [OrderAbandoned](src/docs/Model/OrderAbandoned.md)
 - [OrderAdd](src/docs/Model/OrderAdd.md)
 - [OrderAddOrderItem](src/docs/Model/OrderAddOrderItem.md)
 - [OrderAddOrderItemOption](src/docs/Model/OrderAddOrderItemOption.md)
 - [OrderAddOrderItemProperty](src/docs/Model/OrderAddOrderItemProperty.md)
 - [OrderItem](src/docs/Model/OrderItem.md)
 - [OrderItemOption](src/docs/Model/OrderItemOption.md)
 - [OrderPaymentMethod](src/docs/Model/OrderPaymentMethod.md)
 - [OrderRefund](src/docs/Model/OrderRefund.md)
 - [OrderRefundAdd](src/docs/Model/OrderRefundAdd.md)
 - [OrderRefundAddItems](src/docs/Model/OrderRefundAddItems.md)
 - [OrderShipmentAdd](src/docs/Model/OrderShipmentAdd.md)
 - [OrderShipmentAddItems](src/docs/Model/OrderShipmentAddItems.md)
 - [OrderShipmentAddTrackingNumbers](src/docs/Model/OrderShipmentAddTrackingNumbers.md)
 - [OrderShipmentTrackingAdd](src/docs/Model/OrderShipmentTrackingAdd.md)
 - [OrderShipmentUpdate](src/docs/Model/OrderShipmentUpdate.md)
 - [OrderShippingMethod](src/docs/Model/OrderShippingMethod.md)
 - [OrderStatus](src/docs/Model/OrderStatus.md)
 - [OrderStatusHistoryItem](src/docs/Model/OrderStatusHistoryItem.md)
 - [OrderStatusRefund](src/docs/Model/OrderStatusRefund.md)
 - [OrderStatusRefundItem](src/docs/Model/OrderStatusRefundItem.md)
 - [OrderTotal](src/docs/Model/OrderTotal.md)
 - [OrderTotals](src/docs/Model/OrderTotals.md)
 - [OrderTotalsNewDiscount](src/docs/Model/OrderTotalsNewDiscount.md)
 - [Pagination](src/docs/Model/Pagination.md)
 - [Plugin](src/docs/Model/Plugin.md)
 - [PluginList](src/docs/Model/PluginList.md)
 - [Product](src/docs/Model/Product.md)
 - [ProductAdd](src/docs/Model/ProductAdd.md)
 - [ProductAddGroupPrices](src/docs/Model/ProductAddGroupPrices.md)
 - [ProductAddShippingDetails](src/docs/Model/ProductAddShippingDetails.md)
 - [ProductAddTierPrices](src/docs/Model/ProductAddTierPrices.md)
 - [ProductAdvancedPrice](src/docs/Model/ProductAdvancedPrice.md)
 - [ProductAttribute](src/docs/Model/ProductAttribute.md)
 - [ProductGroupPrice](src/docs/Model/ProductGroupPrice.md)
 - [ProductInventory](src/docs/Model/ProductInventory.md)
 - [ProductOption](src/docs/Model/ProductOption.md)
 - [ProductOptionItem](src/docs/Model/ProductOptionItem.md)
 - [ProductPriceAdd](src/docs/Model/ProductPriceAdd.md)
 - [ProductPriceUpdate](src/docs/Model/ProductPriceUpdate.md)
 - [ProductPriceUpdateGroupPrices](src/docs/Model/ProductPriceUpdateGroupPrices.md)
 - [ProductReview](src/docs/Model/ProductReview.md)
 - [ProductReviewRating](src/docs/Model/ProductReviewRating.md)
 - [ProductTaxAdd](src/docs/Model/ProductTaxAdd.md)
 - [ProductTaxAddTaxRates](src/docs/Model/ProductTaxAddTaxRates.md)
 - [ProductTierPrice](src/docs/Model/ProductTierPrice.md)
 - [ProductVariantAdd](src/docs/Model/ProductVariantAdd.md)
 - [ProductVariantPriceAdd](src/docs/Model/ProductVariantPriceAdd.md)
 - [ProductVariantPriceUpdate](src/docs/Model/ProductVariantPriceUpdate.md)
 - [ResponseCartCouponListResult](src/docs/Model/ResponseCartCouponListResult.md)
 - [ResponseCartGiftcardListResult](src/docs/Model/ResponseCartGiftcardListResult.md)
 - [ResponseCartScriptListResult](src/docs/Model/ResponseCartScriptListResult.md)
 - [ResponseCategoryListResult](src/docs/Model/ResponseCategoryListResult.md)
 - [ResponseCustomerGroupListResult](src/docs/Model/ResponseCustomerGroupListResult.md)
 - [ResponseCustomerListResult](src/docs/Model/ResponseCustomerListResult.md)
 - [ResponseOrderAbandonedListResult](src/docs/Model/ResponseOrderAbandonedListResult.md)
 - [ResponseOrderListResult](src/docs/Model/ResponseOrderListResult.md)
 - [ResponseOrderShipmentListResult](src/docs/Model/ResponseOrderShipmentListResult.md)
 - [ResponseProductAttributeListResult](src/docs/Model/ResponseProductAttributeListResult.md)
 - [ResponseProductChildItemListResult](src/docs/Model/ResponseProductChildItemListResult.md)
 - [ResponseProductListResult](src/docs/Model/ResponseProductListResult.md)
 - [Script](src/docs/Model/Script.md)
 - [Shipment](src/docs/Model/Shipment.md)
 - [ShipmentItems](src/docs/Model/ShipmentItems.md)
 - [ShipmentTrackingNumber](src/docs/Model/ShipmentTrackingNumber.md)
 - [SpecialPrice](src/docs/Model/SpecialPrice.md)
 - [State](src/docs/Model/State.md)
 - [StoreAttribute](src/docs/Model/StoreAttribute.md)
 - [StoreAttributeGroup](src/docs/Model/StoreAttributeGroup.md)
 - [Subscriber](src/docs/Model/Subscriber.md)
 - [TaxClass](src/docs/Model/TaxClass.md)
 - [TaxClassRate](src/docs/Model/TaxClassRate.md)
 - [Webhook](src/docs/Model/Webhook.md)



## Author

contact@api2cart.com
