<?php

use PHPUnit\Framework\TestCase;
use Api2Cart\Client as ApiClient;

class ApiTest extends TestCase
{

    public function __construct(string $name = null, array $data = [], $dataName = '')
    {
        $this->config = new ApiClient\Configuration();

        $this->account = new ApiClient\Api\AccountApi(null, $this->config);
        $this->cart = new ApiClient\Api\CartApi(null, $this->config);
        $this->category = new ApiClient\Api\CategoryApi(null, $this->config);
        $this->customer = new ApiClient\Api\CustomerApi(null, $this->config);
        $this->order = new ApiClient\Api\OrderApi(null, $this->config);
        $this->product = new ApiClient\Api\ProductApi(null, $this->config);
        $this->subscriber = new ApiClient\Api\SubscriberApi(null, $this->config);

        $this->config->setApiKey('api_key', getenv('API_KEY'));

        parent::__construct($name, $data, $dataName);
    }

    public function testProductList()
    {
        /**
         * Get cart details
         */
        $account = $this->account->accountCartList();

        if ($account->getReturnCode() == 2) {
            echo "\nPlease use right API key on your ENV variable API_KEY\n";
            $this->assertTrue(false);
        }

        $store_id = $account->getResult()->getCarts()[0]->getStoreKey();
        $this->product->getConfig()->setApiKey('store_key', $store_id);

        $items = $this->product->productList();


        $this->assertTrue($items->getReturnCode() == 0);
    }

    public function testOrdersList()
    {
        /**
         * Get cart details
         */
        $account = $this->account->accountCartList();

        if ($account->getReturnCode() == 2) {
            echo "\nPlease use right API key on your ENV variable API_KEY\n";
            $this->assertTrue(false);
        }

        $store_id = $account->getResult()->getCarts()[0]->getStoreKey();
        $this->order->getConfig()->setApiKey('store_key', $store_id);

        $items = $this->order->orderList();

        $this->assertTrue($items->getReturnCode() == 0);
    }

    public function testCategoryList()
    {
        /**
         * Get cart details
         */
        $account = $this->account->accountCartList();

        if ($account->getReturnCode() == 2) {
            echo "\nPlease use right API key on your ENV variable API_KEY\n";
            $this->assertTrue(false);
        }

        $store_id = $account->getResult()->getCarts()[0]->getStoreKey();
        $this->category->getConfig()->setApiKey('store_key', $store_id);

        $items = $this->category->categoryList();

        $this->assertTrue($items->getReturnCode() == 0);
    }
}
