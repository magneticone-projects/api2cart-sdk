<?php
/**
 * OrderTotalTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Api2Cart\Client;

/**
 * OrderTotalTest Class Doc Comment
 *
 * @category    Class
 * @description OrderTotal
 * @package     Api2Cart\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OrderTotalTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "OrderTotal"
     */
    public function testOrderTotal()
    {
    }

    /**
     * Test attribute "subtotal_ex_tax"
     */
    public function testPropertySubtotalExTax()
    {
    }

    /**
     * Test attribute "wrapping_ex_tax"
     */
    public function testPropertyWrappingExTax()
    {
    }

    /**
     * Test attribute "shipping_ex_tax"
     */
    public function testPropertyShippingExTax()
    {
    }

    /**
     * Test attribute "total_discount"
     */
    public function testPropertyTotalDiscount()
    {
    }

    /**
     * Test attribute "total_tax"
     */
    public function testPropertyTotalTax()
    {
    }

    /**
     * Test attribute "total"
     */
    public function testPropertyTotal()
    {
    }

    /**
     * Test attribute "total_paid"
     */
    public function testPropertyTotalPaid()
    {
    }

    /**
     * Test attribute "additional_fields"
     */
    public function testPropertyAdditionalFields()
    {
    }

    /**
     * Test attribute "custom_fields"
     */
    public function testPropertyCustomFields()
    {
    }
}
