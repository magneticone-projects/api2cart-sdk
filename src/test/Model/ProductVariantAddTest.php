<?php
/**
 * ProductVariantAddTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Api2Cart\Client;

/**
 * ProductVariantAddTest Class Doc Comment
 *
 * @category    Class
 * @description ProductVariantAdd
 * @package     Api2Cart\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ProductVariantAddTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ProductVariantAdd"
     */
    public function testProductVariantAdd()
    {
    }

    /**
     * Test attribute "product_id"
     */
    public function testPropertyProductId()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "model"
     */
    public function testPropertyModel()
    {
    }

    /**
     * Test attribute "sku"
     */
    public function testPropertySku()
    {
    }

    /**
     * Test attribute "barcode"
     */
    public function testPropertyBarcode()
    {
    }

    /**
     * Test attribute "gtin"
     */
    public function testPropertyGtin()
    {
    }

    /**
     * Test attribute "price"
     */
    public function testPropertyPrice()
    {
    }

    /**
     * Test attribute "old_price"
     */
    public function testPropertyOldPrice()
    {
    }

    /**
     * Test attribute "cost_price"
     */
    public function testPropertyCostPrice()
    {
    }

    /**
     * Test attribute "fixed_cost_shipping_price"
     */
    public function testPropertyFixedCostShippingPrice()
    {
    }

    /**
     * Test attribute "attributes"
     */
    public function testPropertyAttributes()
    {
    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {
    }

    /**
     * Test attribute "special_price"
     */
    public function testPropertySpecialPrice()
    {
    }

    /**
     * Test attribute "sprice_create"
     */
    public function testPropertySpriceCreate()
    {
    }

    /**
     * Test attribute "sprice_modified"
     */
    public function testPropertySpriceModified()
    {
    }

    /**
     * Test attribute "sprice_expire"
     */
    public function testPropertySpriceExpire()
    {
    }

    /**
     * Test attribute "available_for_view"
     */
    public function testPropertyAvailableForView()
    {
    }

    /**
     * Test attribute "available_for_sale"
     */
    public function testPropertyAvailableForSale()
    {
    }

    /**
     * Test attribute "weight"
     */
    public function testPropertyWeight()
    {
    }

    /**
     * Test attribute "width"
     */
    public function testPropertyWidth()
    {
    }

    /**
     * Test attribute "height"
     */
    public function testPropertyHeight()
    {
    }

    /**
     * Test attribute "length"
     */
    public function testPropertyLength()
    {
    }

    /**
     * Test attribute "weight_unit"
     */
    public function testPropertyWeightUnit()
    {
    }

    /**
     * Test attribute "short_description"
     */
    public function testPropertyShortDescription()
    {
    }

    /**
     * Test attribute "warehouse_id"
     */
    public function testPropertyWarehouseId()
    {
    }

    /**
     * Test attribute "quantity"
     */
    public function testPropertyQuantity()
    {
    }

    /**
     * Test attribute "created_at"
     */
    public function testPropertyCreatedAt()
    {
    }

    /**
     * Test attribute "manufacturer"
     */
    public function testPropertyManufacturer()
    {
    }

    /**
     * Test attribute "tax_class_id"
     */
    public function testPropertyTaxClassId()
    {
    }

    /**
     * Test attribute "meta_title"
     */
    public function testPropertyMetaTitle()
    {
    }

    /**
     * Test attribute "meta_keywords"
     */
    public function testPropertyMetaKeywords()
    {
    }

    /**
     * Test attribute "meta_description"
     */
    public function testPropertyMetaDescription()
    {
    }

    /**
     * Test attribute "url"
     */
    public function testPropertyUrl()
    {
    }

    /**
     * Test attribute "store_id"
     */
    public function testPropertyStoreId()
    {
    }

    /**
     * Test attribute "lang_id"
     */
    public function testPropertyLangId()
    {
    }

    /**
     * Test attribute "clear_cache"
     */
    public function testPropertyClearCache()
    {
    }

    /**
     * Test attribute "taxable"
     */
    public function testPropertyTaxable()
    {
    }

    /**
     * Test attribute "harmonized_system_code"
     */
    public function testPropertyHarmonizedSystemCode()
    {
    }

    /**
     * Test attribute "country_of_origin"
     */
    public function testPropertyCountryOfOrigin()
    {
    }

    /**
     * Test attribute "manage_stock"
     */
    public function testPropertyManageStock()
    {
    }

    /**
     * Test attribute "upc"
     */
    public function testPropertyUpc()
    {
    }

    /**
     * Test attribute "mpn"
     */
    public function testPropertyMpn()
    {
    }

    /**
     * Test attribute "ean"
     */
    public function testPropertyEan()
    {
    }

    /**
     * Test attribute "isbn"
     */
    public function testPropertyIsbn()
    {
    }

    /**
     * Test attribute "stores_ids"
     */
    public function testPropertyStoresIds()
    {
    }

    /**
     * Test attribute "is_default"
     */
    public function testPropertyIsDefault()
    {
    }

    /**
     * Test attribute "is_free_shipping"
     */
    public function testPropertyIsFreeShipping()
    {
    }

    /**
     * Test attribute "marketplace_item_properties"
     */
    public function testPropertyMarketplaceItemProperties()
    {
    }

    /**
     * Test attribute "in_stock"
     */
    public function testPropertyInStock()
    {
    }
}
