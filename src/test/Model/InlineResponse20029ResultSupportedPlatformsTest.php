<?php
/**
 * InlineResponse20029ResultSupportedPlatformsTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.15-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Api2Cart\Client;

/**
 * InlineResponse20029ResultSupportedPlatformsTest Class Doc Comment
 *
 * @category    Class
 * @description InlineResponse20029ResultSupportedPlatforms
 * @package     Api2Cart\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class InlineResponse20029ResultSupportedPlatformsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "InlineResponse20029ResultSupportedPlatforms"
     */
    public function testInlineResponse20029ResultSupportedPlatforms()
    {
    }

    /**
     * Test attribute "cart_id"
     */
    public function testPropertyCartId()
    {
    }

    /**
     * Test attribute "cart_name"
     */
    public function testPropertyCartName()
    {
    }

    /**
     * Test attribute "cart_versions"
     */
    public function testPropertyCartVersions()
    {
    }

    /**
     * Test attribute "params"
     */
    public function testPropertyParams()
    {
    }
}
