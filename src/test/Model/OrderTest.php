<?php
/**
 * OrderTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Api2Cart\Client;

/**
 * OrderTest Class Doc Comment
 *
 * @category    Class
 * @description Order
 * @package     Api2Cart\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OrderTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Order"
     */
    public function testOrder()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "order_id"
     */
    public function testPropertyOrderId()
    {
    }

    /**
     * Test attribute "basket_id"
     */
    public function testPropertyBasketId()
    {
    }

    /**
     * Test attribute "channel_id"
     */
    public function testPropertyChannelId()
    {
    }

    /**
     * Test attribute "customer"
     */
    public function testPropertyCustomer()
    {
    }

    /**
     * Test attribute "create_at"
     */
    public function testPropertyCreateAt()
    {
    }

    /**
     * Test attribute "currency"
     */
    public function testPropertyCurrency()
    {
    }

    /**
     * Test attribute "shipping_address"
     */
    public function testPropertyShippingAddress()
    {
    }

    /**
     * Test attribute "billing_address"
     */
    public function testPropertyBillingAddress()
    {
    }

    /**
     * Test attribute "payment_method"
     */
    public function testPropertyPaymentMethod()
    {
    }

    /**
     * Test attribute "shipping_method"
     */
    public function testPropertyShippingMethod()
    {
    }

    /**
     * Test attribute "shipping_methods"
     */
    public function testPropertyShippingMethods()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "totals"
     */
    public function testPropertyTotals()
    {
    }

    /**
     * Test attribute "total"
     */
    public function testPropertyTotal()
    {
    }

    /**
     * Test attribute "discounts"
     */
    public function testPropertyDiscounts()
    {
    }

    /**
     * Test attribute "order_products"
     */
    public function testPropertyOrderProducts()
    {
    }

    /**
     * Test attribute "bundles"
     */
    public function testPropertyBundles()
    {
    }

    /**
     * Test attribute "modified_at"
     */
    public function testPropertyModifiedAt()
    {
    }

    /**
     * Test attribute "finished_time"
     */
    public function testPropertyFinishedTime()
    {
    }

    /**
     * Test attribute "comment"
     */
    public function testPropertyComment()
    {
    }

    /**
     * Test attribute "store_id"
     */
    public function testPropertyStoreId()
    {
    }

    /**
     * Test attribute "warehouses_ids"
     */
    public function testPropertyWarehousesIds()
    {
    }

    /**
     * Test attribute "refunds"
     */
    public function testPropertyRefunds()
    {
    }

    /**
     * Test attribute "gift_message"
     */
    public function testPropertyGiftMessage()
    {
    }

    /**
     * Test attribute "order_details_url"
     */
    public function testPropertyOrderDetailsUrl()
    {
    }

    /**
     * Test attribute "additional_fields"
     */
    public function testPropertyAdditionalFields()
    {
    }

    /**
     * Test attribute "custom_fields"
     */
    public function testPropertyCustomFields()
    {
    }
}
