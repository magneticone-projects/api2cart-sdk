<?php
/**
 * OrderReturnAddOrderProductsTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Api2Cart\Client;

/**
 * OrderReturnAddOrderProductsTest Class Doc Comment
 *
 * @category    Class
 * @description OrderReturnAddOrderProducts
 * @package     Api2Cart\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OrderReturnAddOrderProductsTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "OrderReturnAddOrderProducts"
     */
    public function testOrderReturnAddOrderProducts()
    {
    }

    /**
     * Test attribute "order_product_id"
     */
    public function testPropertyOrderProductId()
    {
    }

    /**
     * Test attribute "order_product_quantity"
     */
    public function testPropertyOrderProductQuantity()
    {
    }

    /**
     * Test attribute "order_product_reason_id"
     */
    public function testPropertyOrderProductReasonId()
    {
    }

    /**
     * Test attribute "order_product_action_id"
     */
    public function testPropertyOrderProductActionId()
    {
    }

    /**
     * Test attribute "order_product_customer_comment"
     */
    public function testPropertyOrderProductCustomerComment()
    {
    }

    /**
     * Test attribute "order_product_handling_status"
     */
    public function testPropertyOrderProductHandlingStatus()
    {
    }

    /**
     * Test attribute "order_product_condition"
     */
    public function testPropertyOrderProductCondition()
    {
    }

    /**
     * Test attribute "order_product_reason"
     */
    public function testPropertyOrderProductReason()
    {
    }

    /**
     * Test attribute "order_product_status"
     */
    public function testPropertyOrderProductStatus()
    {
    }
}
