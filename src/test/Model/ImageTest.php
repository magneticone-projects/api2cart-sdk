<?php
/**
 * ImageTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Api2Cart\Client;

/**
 * ImageTest Class Doc Comment
 *
 * @category    Class
 * @description Image
 * @package     Api2Cart\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ImageTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Image"
     */
    public function testImage()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "http_path"
     */
    public function testPropertyHttpPath()
    {
    }

    /**
     * Test attribute "file_name"
     */
    public function testPropertyFileName()
    {
    }

    /**
     * Test attribute "mime_type"
     */
    public function testPropertyMimeType()
    {
    }

    /**
     * Test attribute "size"
     */
    public function testPropertySize()
    {
    }

    /**
     * Test attribute "create_at"
     */
    public function testPropertyCreateAt()
    {
    }

    /**
     * Test attribute "modified_at"
     */
    public function testPropertyModifiedAt()
    {
    }

    /**
     * Test attribute "alt"
     */
    public function testPropertyAlt()
    {
    }

    /**
     * Test attribute "avail"
     */
    public function testPropertyAvail()
    {
    }

    /**
     * Test attribute "sort_order"
     */
    public function testPropertySortOrder()
    {
    }

    /**
     * Test attribute "type"
     */
    public function testPropertyType()
    {
    }

    /**
     * Test attribute "additional_fields"
     */
    public function testPropertyAdditionalFields()
    {
    }

    /**
     * Test attribute "custom_fields"
     */
    public function testPropertyCustomFields()
    {
    }
}
