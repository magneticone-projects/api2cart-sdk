<?php
/**
 * ChildTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Api2Cart\Client;

/**
 * ChildTest Class Doc Comment
 *
 * @category    Class
 * @description Child
 * @package     Api2Cart\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ChildTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Child"
     */
    public function testChild()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "parent_id"
     */
    public function testPropertyParentId()
    {
    }

    /**
     * Test attribute "sku"
     */
    public function testPropertySku()
    {
    }

    /**
     * Test attribute "upc"
     */
    public function testPropertyUpc()
    {
    }

    /**
     * Test attribute "ean"
     */
    public function testPropertyEan()
    {
    }

    /**
     * Test attribute "mpn"
     */
    public function testPropertyMpn()
    {
    }

    /**
     * Test attribute "gtin"
     */
    public function testPropertyGtin()
    {
    }

    /**
     * Test attribute "isbn"
     */
    public function testPropertyIsbn()
    {
    }

    /**
     * Test attribute "url"
     */
    public function testPropertyUrl()
    {
    }

    /**
     * Test attribute "seo_url"
     */
    public function testPropertySeoUrl()
    {
    }

    /**
     * Test attribute "sort_order"
     */
    public function testPropertySortOrder()
    {
    }

    /**
     * Test attribute "created_time"
     */
    public function testPropertyCreatedTime()
    {
    }

    /**
     * Test attribute "modified_time"
     */
    public function testPropertyModifiedTime()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "short_description"
     */
    public function testPropertyShortDescription()
    {
    }

    /**
     * Test attribute "full_description"
     */
    public function testPropertyFullDescription()
    {
    }

    /**
     * Test attribute "images"
     */
    public function testPropertyImages()
    {
    }

    /**
     * Test attribute "combination"
     */
    public function testPropertyCombination()
    {
    }

    /**
     * Test attribute "default_price"
     */
    public function testPropertyDefaultPrice()
    {
    }

    /**
     * Test attribute "cost_price"
     */
    public function testPropertyCostPrice()
    {
    }

    /**
     * Test attribute "list_price"
     */
    public function testPropertyListPrice()
    {
    }

    /**
     * Test attribute "wholesale_price"
     */
    public function testPropertyWholesalePrice()
    {
    }

    /**
     * Test attribute "advanced_price"
     */
    public function testPropertyAdvancedPrice()
    {
    }

    /**
     * Test attribute "tax_class_id"
     */
    public function testPropertyTaxClassId()
    {
    }

    /**
     * Test attribute "avail_for_sale"
     */
    public function testPropertyAvailForSale()
    {
    }

    /**
     * Test attribute "allow_backorders"
     */
    public function testPropertyAllowBackorders()
    {
    }

    /**
     * Test attribute "in_stock"
     */
    public function testPropertyInStock()
    {
    }

    /**
     * Test attribute "manage_stock"
     */
    public function testPropertyManageStock()
    {
    }

    /**
     * Test attribute "inventory_level"
     */
    public function testPropertyInventoryLevel()
    {
    }

    /**
     * Test attribute "inventory"
     */
    public function testPropertyInventory()
    {
    }

    /**
     * Test attribute "min_quantity"
     */
    public function testPropertyMinQuantity()
    {
    }

    /**
     * Test attribute "default_qty_in_pack"
     */
    public function testPropertyDefaultQtyInPack()
    {
    }

    /**
     * Test attribute "is_qty_in_pack_fixed"
     */
    public function testPropertyIsQtyInPackFixed()
    {
    }

    /**
     * Test attribute "weight_unit"
     */
    public function testPropertyWeightUnit()
    {
    }

    /**
     * Test attribute "weight"
     */
    public function testPropertyWeight()
    {
    }

    /**
     * Test attribute "dimensions_unit"
     */
    public function testPropertyDimensionsUnit()
    {
    }

    /**
     * Test attribute "width"
     */
    public function testPropertyWidth()
    {
    }

    /**
     * Test attribute "height"
     */
    public function testPropertyHeight()
    {
    }

    /**
     * Test attribute "length"
     */
    public function testPropertyLength()
    {
    }

    /**
     * Test attribute "meta_title"
     */
    public function testPropertyMetaTitle()
    {
    }

    /**
     * Test attribute "meta_description"
     */
    public function testPropertyMetaDescription()
    {
    }

    /**
     * Test attribute "meta_keywords"
     */
    public function testPropertyMetaKeywords()
    {
    }

    /**
     * Test attribute "discounts"
     */
    public function testPropertyDiscounts()
    {
    }

    /**
     * Test attribute "additional_fields"
     */
    public function testPropertyAdditionalFields()
    {
    }

    /**
     * Test attribute "custom_fields"
     */
    public function testPropertyCustomFields()
    {
    }
}
