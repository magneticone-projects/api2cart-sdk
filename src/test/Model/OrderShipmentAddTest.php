<?php
/**
 * OrderShipmentAddTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Api2Cart\Client;

/**
 * OrderShipmentAddTest Class Doc Comment
 *
 * @category    Class
 * @description OrderShipmentAdd
 * @package     Api2Cart\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OrderShipmentAddTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "OrderShipmentAdd"
     */
    public function testOrderShipmentAdd()
    {
    }

    /**
     * Test attribute "order_id"
     */
    public function testPropertyOrderId()
    {
    }

    /**
     * Test attribute "store_id"
     */
    public function testPropertyStoreId()
    {
    }

    /**
     * Test attribute "warehouse_id"
     */
    public function testPropertyWarehouseId()
    {
    }

    /**
     * Test attribute "shipment_provider"
     */
    public function testPropertyShipmentProvider()
    {
    }

    /**
     * Test attribute "shipping_method"
     */
    public function testPropertyShippingMethod()
    {
    }

    /**
     * Test attribute "items"
     */
    public function testPropertyItems()
    {
    }

    /**
     * Test attribute "send_notifications"
     */
    public function testPropertySendNotifications()
    {
    }

    /**
     * Test attribute "tracking_numbers"
     */
    public function testPropertyTrackingNumbers()
    {
    }

    /**
     * Test attribute "adjust_stock"
     */
    public function testPropertyAdjustStock()
    {
    }

    /**
     * Test attribute "enable_cache"
     */
    public function testPropertyEnableCache()
    {
    }

    /**
     * Test attribute "tracking_link"
     */
    public function testPropertyTrackingLink()
    {
    }

    /**
     * Test attribute "is_shipped"
     */
    public function testPropertyIsShipped()
    {
    }

    /**
     * Test attribute "check_process_status"
     */
    public function testPropertyCheckProcessStatus()
    {
    }

    /**
     * Test attribute "use_latest_api_version"
     */
    public function testPropertyUseLatestApiVersion()
    {
    }
}
