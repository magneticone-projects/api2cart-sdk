<?php
/**
 * ResponseMarketplaceProductFindResultTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Api2Cart\Client;

/**
 * ResponseMarketplaceProductFindResultTest Class Doc Comment
 *
 * @category    Class
 * @description ResponseMarketplaceProductFindResult
 * @package     Api2Cart\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ResponseMarketplaceProductFindResultTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ResponseMarketplaceProductFindResult"
     */
    public function testResponseMarketplaceProductFindResult()
    {
    }

    /**
     * Test attribute "marketplace_products_count"
     */
    public function testPropertyMarketplaceProductsCount()
    {
    }

    /**
     * Test attribute "marketplace_product"
     */
    public function testPropertyMarketplaceProduct()
    {
    }

    /**
     * Test attribute "additional_fields"
     */
    public function testPropertyAdditionalFields()
    {
    }

    /**
     * Test attribute "custom_fields"
     */
    public function testPropertyCustomFields()
    {
    }
}
