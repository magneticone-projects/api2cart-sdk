<?php
/**
 * CartStoreInfoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Api2Cart\Client;

/**
 * CartStoreInfoTest Class Doc Comment
 *
 * @category    Class
 * @description CartStoreInfo
 * @package     Api2Cart\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CartStoreInfoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "CartStoreInfo"
     */
    public function testCartStoreInfo()
    {
    }

    /**
     * Test attribute "store_id"
     */
    public function testPropertyStoreId()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "language"
     */
    public function testPropertyLanguage()
    {
    }

    /**
     * Test attribute "store_languages"
     */
    public function testPropertyStoreLanguages()
    {
    }

    /**
     * Test attribute "currency"
     */
    public function testPropertyCurrency()
    {
    }

    /**
     * Test attribute "store_currencies"
     */
    public function testPropertyStoreCurrencies()
    {
    }

    /**
     * Test attribute "timezone"
     */
    public function testPropertyTimezone()
    {
    }

    /**
     * Test attribute "country"
     */
    public function testPropertyCountry()
    {
    }

    /**
     * Test attribute "root_category_id"
     */
    public function testPropertyRootCategoryId()
    {
    }

    /**
     * Test attribute "multi_store_url"
     */
    public function testPropertyMultiStoreUrl()
    {
    }

    /**
     * Test attribute "active"
     */
    public function testPropertyActive()
    {
    }

    /**
     * Test attribute "weight_unit"
     */
    public function testPropertyWeightUnit()
    {
    }

    /**
     * Test attribute "dimension_unit"
     */
    public function testPropertyDimensionUnit()
    {
    }

    /**
     * Test attribute "prices_include_tax"
     */
    public function testPropertyPricesIncludeTax()
    {
    }

    /**
     * Test attribute "carrier_info"
     */
    public function testPropertyCarrierInfo()
    {
    }

    /**
     * Test attribute "store_owner_info"
     */
    public function testPropertyStoreOwnerInfo()
    {
    }

    /**
     * Test attribute "default_warehouse_id"
     */
    public function testPropertyDefaultWarehouseId()
    {
    }

    /**
     * Test attribute "channels"
     */
    public function testPropertyChannels()
    {
    }

    /**
     * Test attribute "additional_fields"
     */
    public function testPropertyAdditionalFields()
    {
    }

    /**
     * Test attribute "custom_fields"
     */
    public function testPropertyCustomFields()
    {
    }
}
