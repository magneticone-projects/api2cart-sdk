<?php
/**
 * CartApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Api2Cart\Client;

use \Api2Cart\Client\Configuration;
use \Api2Cart\Client\ApiException;
use \Api2Cart\Client\ObjectSerializer;

/**
 * CartApiTest Class Doc Comment
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class CartApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for cartBridge
     *
     * .
     *
     */
    public function testCartBridge()
    {
    }

    /**
     * Test case for cartCatalogPriceRulesCount
     *
     * .
     *
     */
    public function testCartCatalogPriceRulesCount()
    {
    }

    /**
     * Test case for cartCatalogPriceRulesList
     *
     * .
     *
     */
    public function testCartCatalogPriceRulesList()
    {
    }

    /**
     * Test case for cartClearCache
     *
     * .
     *
     */
    public function testCartClearCache()
    {
    }

    /**
     * Test case for cartConfig
     *
     * .
     *
     */
    public function testCartConfig()
    {
    }

    /**
     * Test case for cartConfigUpdate
     *
     * .
     *
     */
    public function testCartConfigUpdate()
    {
    }

    /**
     * Test case for cartCouponAdd
     *
     * .
     *
     */
    public function testCartCouponAdd()
    {
    }

    /**
     * Test case for cartCouponConditionAdd
     *
     * .
     *
     */
    public function testCartCouponConditionAdd()
    {
    }

    /**
     * Test case for cartCouponCount
     *
     * .
     *
     */
    public function testCartCouponCount()
    {
    }

    /**
     * Test case for cartCouponDelete
     *
     * .
     *
     */
    public function testCartCouponDelete()
    {
    }

    /**
     * Test case for cartCouponList
     *
     * .
     *
     */
    public function testCartCouponList()
    {
    }

    /**
     * Test case for cartCreate
     *
     * .
     *
     */
    public function testCartCreate()
    {
    }

    /**
     * Test case for cartDelete
     *
     * .
     *
     */
    public function testCartDelete()
    {
    }

    /**
     * Test case for cartDisconnect
     *
     * .
     *
     */
    public function testCartDisconnect()
    {
    }

    /**
     * Test case for cartGiftcardAdd
     *
     * .
     *
     */
    public function testCartGiftcardAdd()
    {
    }

    /**
     * Test case for cartGiftcardCount
     *
     * .
     *
     */
    public function testCartGiftcardCount()
    {
    }

    /**
     * Test case for cartGiftcardDelete
     *
     * .
     *
     */
    public function testCartGiftcardDelete()
    {
    }

    /**
     * Test case for cartGiftcardList
     *
     * .
     *
     */
    public function testCartGiftcardList()
    {
    }

    /**
     * Test case for cartInfo
     *
     * .
     *
     */
    public function testCartInfo()
    {
    }

    /**
     * Test case for cartList
     *
     * .
     *
     */
    public function testCartList()
    {
    }

    /**
     * Test case for cartMetaDataList
     *
     * .
     *
     */
    public function testCartMetaDataList()
    {
    }

    /**
     * Test case for cartMetaDataSet
     *
     * .
     *
     */
    public function testCartMetaDataSet()
    {
    }

    /**
     * Test case for cartMetaDataUnset
     *
     * .
     *
     */
    public function testCartMetaDataUnset()
    {
    }

    /**
     * Test case for cartMethods
     *
     * .
     *
     */
    public function testCartMethods()
    {
    }

    /**
     * Test case for cartPluginList
     *
     * .
     *
     */
    public function testCartPluginList()
    {
    }

    /**
     * Test case for cartScriptAdd
     *
     * .
     *
     */
    public function testCartScriptAdd()
    {
    }

    /**
     * Test case for cartScriptDelete
     *
     * .
     *
     */
    public function testCartScriptDelete()
    {
    }

    /**
     * Test case for cartScriptList
     *
     * .
     *
     */
    public function testCartScriptList()
    {
    }

    /**
     * Test case for cartShippingZonesList
     *
     * .
     *
     */
    public function testCartShippingZonesList()
    {
    }

    /**
     * Test case for cartValidate
     *
     * .
     *
     */
    public function testCartValidate()
    {
    }
}
