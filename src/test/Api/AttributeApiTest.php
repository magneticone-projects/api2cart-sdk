<?php
/**
 * AttributeApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Api2Cart\Client;

use \Api2Cart\Client\Configuration;
use \Api2Cart\Client\ApiException;
use \Api2Cart\Client\ObjectSerializer;

/**
 * AttributeApiTest Class Doc Comment
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class AttributeApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for attributeAdd
     *
     * .
     *
     */
    public function testAttributeAdd()
    {
    }

    /**
     * Test case for attributeAssignGroup
     *
     * .
     *
     */
    public function testAttributeAssignGroup()
    {
    }

    /**
     * Test case for attributeAssignSet
     *
     * .
     *
     */
    public function testAttributeAssignSet()
    {
    }

    /**
     * Test case for attributeAttributesetList
     *
     * .
     *
     */
    public function testAttributeAttributesetList()
    {
    }

    /**
     * Test case for attributeCount
     *
     * .
     *
     */
    public function testAttributeCount()
    {
    }

    /**
     * Test case for attributeDelete
     *
     * .
     *
     */
    public function testAttributeDelete()
    {
    }

    /**
     * Test case for attributeGroupList
     *
     * .
     *
     */
    public function testAttributeGroupList()
    {
    }

    /**
     * Test case for attributeInfo
     *
     * .
     *
     */
    public function testAttributeInfo()
    {
    }

    /**
     * Test case for attributeList
     *
     * .
     *
     */
    public function testAttributeList()
    {
    }

    /**
     * Test case for attributeTypeList
     *
     * .
     *
     */
    public function testAttributeTypeList()
    {
    }

    /**
     * Test case for attributeUnassignGroup
     *
     * .
     *
     */
    public function testAttributeUnassignGroup()
    {
    }

    /**
     * Test case for attributeUnassignSet
     *
     * .
     *
     */
    public function testAttributeUnassignSet()
    {
    }

    /**
     * Test case for attributeUpdate
     *
     * .
     *
     */
    public function testAttributeUpdate()
    {
    }

    /**
     * Test case for attributeValueAdd
     *
     * .
     *
     */
    public function testAttributeValueAdd()
    {
    }

    /**
     * Test case for attributeValueDelete
     *
     * .
     *
     */
    public function testAttributeValueDelete()
    {
    }

    /**
     * Test case for attributeValueUpdate
     *
     * .
     *
     */
    public function testAttributeValueUpdate()
    {
    }
}
