# ProductAddShippingDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_type** | **string** |  | [optional] 
**shipping_service** | **string** |  | [optional] 
**shipping_cost** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


