# InlineResponse20030ResultParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**required** | [**object[][]**](array.md) |  | [optional] 
**additional** | [**\Api2Cart\Client\Model\InlineResponse20030ResultParamsAdditional[]**](InlineResponse20030ResultParamsAdditional.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


