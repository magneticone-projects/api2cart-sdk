# ProductAddSpecifics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**value** | **string** |  | [optional] 
**values** | **string[]** |  | [optional] 
**used_for_variations** | **bool** |  | [optional] [default to false]
**food_details** | [**\Api2Cart\Client\Model\ProductAddFoodDetails**](ProductAddFoodDetails.md) |  | [optional] 
**group_products_details** | [**\Api2Cart\Client\Model\ProductAddGroupProductsDetails[]**](ProductAddGroupProductsDetails.md) |  | [optional] 
**booking_details** | [**\Api2Cart\Client\Model\ProductAddBookingDetails**](ProductAddBookingDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


