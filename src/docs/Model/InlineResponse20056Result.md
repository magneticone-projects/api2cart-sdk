# InlineResponse20056Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_financial_statuses** | [**\Api2Cart\Client\Model\InlineResponse20056ResultOrderFinancialStatuses[]**](InlineResponse20056ResultOrderFinancialStatuses.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


