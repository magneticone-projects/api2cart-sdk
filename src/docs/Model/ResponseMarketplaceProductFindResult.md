# ResponseMarketplaceProductFindResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**marketplace_products_count** | **int** |  | [optional] 
**marketplace_product** | [**\Api2Cart\Client\Model\MarketplaceProduct[]**](MarketplaceProduct.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


