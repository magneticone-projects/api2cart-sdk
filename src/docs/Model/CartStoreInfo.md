# CartStoreInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**store_id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**language** | **string** |  | [optional] 
**store_languages** | [**\Api2Cart\Client\Model\Language[]**](Language.md) |  | [optional] 
**currency** | [**\Api2Cart\Client\Model\Currency**](Currency.md) |  | [optional] 
**store_currencies** | [**\Api2Cart\Client\Model\Currency[]**](Currency.md) |  | [optional] 
**timezone** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**root_category_id** | **string** |  | [optional] 
**multi_store_url** | **string** |  | [optional] 
**active** | **bool** |  | [optional] 
**weight_unit** | **string** |  | [optional] 
**dimension_unit** | **string** |  | [optional] 
**prices_include_tax** | **bool** |  | [optional] 
**carrier_info** | [**\Api2Cart\Client\Model\Carrier[]**](Carrier.md) |  | [optional] 
**store_owner_info** | [**\Api2Cart\Client\Model\Info**](Info.md) |  | [optional] 
**default_warehouse_id** | **string** |  | [optional] 
**channels** | [**\Api2Cart\Client\Model\CartChannel[]**](CartChannel.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


