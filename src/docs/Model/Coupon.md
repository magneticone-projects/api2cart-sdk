# Coupon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**code** | **string** |  | [optional] 
**codes** | [**\Api2Cart\Client\Model\CouponCode[]**](CouponCode.md) |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**actions** | [**\Api2Cart\Client\Model\CouponAction[]**](CouponAction.md) |  | [optional] 
**date_start** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**date_end** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**avail** | **bool** |  | [optional] 
**priority** | **int** |  | [optional] 
**used_times** | **int** |  | [optional] 
**usage_limit** | **int** |  | [optional] 
**usage_limit_per_customer** | **int** |  | [optional] 
**logic_operator** | **string** |  | [optional] 
**conditions** | [**\Api2Cart\Client\Model\CouponCondition[]**](CouponCondition.md) |  | [optional] 
**usage_history** | [**\Api2Cart\Client\Model\CouponHistory[]**](CouponHistory.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


