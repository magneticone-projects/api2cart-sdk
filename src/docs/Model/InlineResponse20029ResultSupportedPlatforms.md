# InlineResponse20029ResultSupportedPlatforms

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cart_id** | **string** |  | [optional] 
**cart_name** | **string** |  | [optional] 
**cart_versions** | **string** |  | [optional] 
**params** | [**\Api2Cart\Client\Model\InlineResponse20029ResultParams**](InlineResponse20029ResultParams.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


