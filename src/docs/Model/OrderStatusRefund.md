# OrderStatusRefund

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping** | **float** |  | [optional] 
**fee** | **float** |  | [optional] 
**tax** | **float** |  | [optional] 
**total_refunded** | **float** |  | [optional] 
**time** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**comment** | **string** |  | [optional] 
**refunded_items** | [**\Api2Cart\Client\Model\OrderStatusRefundItem[]**](OrderStatusRefundItem.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


