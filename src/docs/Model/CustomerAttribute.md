# CustomerAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attribute_id** | **string** |  | [optional] 
**code** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**values** | [**\Api2Cart\Client\Model\CustomerAttributeValue[]**](CustomerAttributeValue.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


