# TaxClassCountries

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**code** | **string** |  | [optional] 
**tax** | **float** |  | [optional] 
**tax_type** | **int** |  | [optional] 
**states** | [**\Api2Cart\Client\Model\TaxClassStates[]**](TaxClassStates.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


