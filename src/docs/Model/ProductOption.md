# ProductOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**product_option_id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**sort_order** | **int** |  | [optional] 
**type** | **string** |  | [optional] 
**required** | **bool** |  | [optional] 
**available** | **bool** |  | [optional] 
**used_in_combination** | **bool** |  | [optional] 
**option_items** | [**\Api2Cart\Client\Model\ProductOptionItem[]**](ProductOptionItem.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


