# ProductAddBookingDetailsAvailabilities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | **string** |  | 
**is_available** | **bool** |  | [optional] [default to true]
**times** | [**\Api2Cart\Client\Model\ProductAddBookingDetailsTimes[]**](ProductAddBookingDetailsTimes.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


