# ProductTaxAdd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_id** | **string** | Defines products specified by product id | [optional] 
**name** | **string** | Defines tax class name where tax has to be added | 
**tax_rates** | [**\Api2Cart\Client\Model\ProductTaxAddTaxRates[]**](ProductTaxAddTaxRates.md) | Defines tax rates of specified tax classes | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


