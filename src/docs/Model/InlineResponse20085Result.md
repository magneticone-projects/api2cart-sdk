# InlineResponse20085Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tax_class_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


