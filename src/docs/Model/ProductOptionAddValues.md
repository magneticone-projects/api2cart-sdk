# ProductOptionAddValues

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **string** |  | 
**display_value** | **string** |  | [optional] 
**is_default** | **bool** |  | [optional] [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


