# TaxClassZipCodes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_range** | **bool** |  | [optional] 
**range** | **string[]** |  | [optional] 
**fields** | [**\Api2Cart\Client\Model\TaxClassZipCodesRange[]**](TaxClassZipCodesRange.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


