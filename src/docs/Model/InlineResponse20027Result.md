# InlineResponse20027Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cache_cleared** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


