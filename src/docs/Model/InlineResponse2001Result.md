# InlineResponse2001Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supported_platforms** | [**\Api2Cart\Client\Model\InlineResponse2001ResultSupportedPlatforms[]**](InlineResponse2001ResultSupportedPlatforms.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


