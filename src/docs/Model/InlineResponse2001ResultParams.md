# InlineResponse2001ResultParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**required** | [**object[][]**](array.md) |  | [optional] 
**additional** | [**\Api2Cart\Client\Model\InlineResponse2001ResultParamsAdditional[]**](InlineResponse2001ResultParamsAdditional.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


