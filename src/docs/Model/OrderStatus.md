# OrderStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**history** | [**\Api2Cart\Client\Model\OrderStatusHistoryItem[]**](OrderStatusHistoryItem.md) |  | [optional] 
**refund_info** | [**\Api2Cart\Client\Model\OrderStatusRefund**](OrderStatusRefund.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


