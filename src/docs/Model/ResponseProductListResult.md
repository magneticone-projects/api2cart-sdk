# ResponseProductListResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products_count** | **int** |  | [optional] 
**product** | [**\Api2Cart\Client\Model\Product[]**](Product.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


