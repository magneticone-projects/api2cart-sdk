# ResponseAttributeListResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes_count** | **int** |  | [optional] 
**attribute** | [**\Api2Cart\Client\Model\StoreAttribute[]**](StoreAttribute.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


