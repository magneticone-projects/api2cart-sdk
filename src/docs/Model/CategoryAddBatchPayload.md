# CategoryAddBatchPayload

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | 
**avail** | **bool** |  | [optional] 
**description** | **string** |  | [optional] 
**meta_title** | **string** |  | [optional] 
**meta_description** | **string** |  | [optional] 
**meta_keywords** | **string[]** |  | [optional] 
**parent_id** | **string** |  | [optional] 
**sort_order** | **int** |  | [optional] 
**seo_url** | **string** |  | [optional] 
**store_id** | **string** |  | [optional] 
**images** | [**\Api2Cart\Client\Model\CategoryAddBatchImages[]**](CategoryAddBatchImages.md) |  | [optional] 
**stores_ids** | **string[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


