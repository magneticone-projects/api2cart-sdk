# InlineResponse20031ResultParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**required** | [**object[][]**](array.md) |  | [optional] 
**additional** | [**\Api2Cart\Client\Model\InlineResponse20031ResultParamsAdditional[]**](InlineResponse20031ResultParamsAdditional.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


