# ProductVariantAddBatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clear_cache** | **bool** |  | [optional] [default to false]
**reindex** | **bool** |  | [optional] [default to false]
**payload** | [**\Api2Cart\Client\Model\ProductVariantAddBatchPayload[]**](ProductVariantAddBatchPayload.md) | Contains an array of product variants objects. The list of properties may vary depending on the specific platform. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


