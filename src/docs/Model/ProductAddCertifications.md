# ProductAddCertifications

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Certification ID | 
**images** | [**\Api2Cart\Client\Model\ProductAddImages[]**](ProductAddImages.md) | Certification images | [optional] 
**files** | [**\Api2Cart\Client\Model\ProductAddFiles1[]**](ProductAddFiles1.md) | Certification files | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


