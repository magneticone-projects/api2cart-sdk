# ProductVariantPriceUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Defines the variant where the price has to be updated | [optional] 
**product_id** | **string** | Product id | [optional] 
**group_prices** | [**\Api2Cart\Client\Model\ProductPriceUpdateGroupPrices[]**](ProductPriceUpdateGroupPrices.md) | Defines variants&#39;s group prices | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


