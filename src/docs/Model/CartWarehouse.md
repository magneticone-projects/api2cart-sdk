# CartWarehouse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**avail** | **bool** |  | [optional] 
**address** | [**\Api2Cart\Client\Model\CustomerAddress**](CustomerAddress.md) |  | [optional] 
**carriers_ids** | **string[]** |  | [optional] 
**stores_ids** | **string[]** |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


