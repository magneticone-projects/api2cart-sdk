# ProductVariantUpdateOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**option_name** | **string** |  | [optional] 
**option_value** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


