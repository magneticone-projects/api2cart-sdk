# InlineResponse2002Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carts_count** | **int** |  | [optional] 
**carts** | [**\Api2Cart\Client\Model\InlineResponse2002ResultCarts[]**](InlineResponse2002ResultCarts.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


