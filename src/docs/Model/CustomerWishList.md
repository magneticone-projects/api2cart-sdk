# CustomerWishList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**is_public** | **string** |  | [optional] 
**created_at** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**modified_at** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**products** | [**\Api2Cart\Client\Model\CustomerWishListItem[]**](CustomerWishListItem.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


