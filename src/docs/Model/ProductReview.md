# ProductReview

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**product_id** | **string** |  | [optional] 
**customer_id** | **string** |  | [optional] 
**nick_name** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**summary** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**rating** | **float** |  | [optional] 
**ratings** | [**\Api2Cart\Client\Model\ProductReviewRating[]**](ProductReviewRating.md) |  | [optional] 
**status** | **string** |  | [optional] 
**created_time** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**medias** | [**\Api2Cart\Client\Model\Media[]**](Media.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


