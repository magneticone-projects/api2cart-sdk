# InlineResponse20012

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**return_code** | **int** |  | [optional] 
**return_message** | **string** |  | [optional] 
**result** | [**\Api2Cart\Client\Model\InlineResponse20012Result**](InlineResponse20012Result.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


