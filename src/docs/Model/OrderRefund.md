# OrderRefund

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**shipping** | **float** |  | [optional] 
**fee** | **float** |  | [optional] 
**tax** | **float** |  | [optional] 
**total** | **float** |  | [optional] 
**modified_time** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**comment** | **string** |  | [optional] 
**items** | [**\Api2Cart\Client\Model\OrderStatusRefundItem[]**](OrderStatusRefundItem.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


