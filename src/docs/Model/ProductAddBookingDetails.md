# ProductAddBookingDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location** | **string** |  | 
**type** | **string** |  | 
**session_duration** | **int** |  | [optional] 
**session_gap** | **int** |  | [optional] 
**sessions_count** | **int** |  | 
**time_strict_value** | **float** |  | 
**time_strict_type** | **string** |  | [default to 'days']
**availabilities** | [**\Api2Cart\Client\Model\ProductAddBookingDetailsAvailabilities[]**](ProductAddBookingDetailsAvailabilities.md) |  | 
**overrides** | [**\Api2Cart\Client\Model\ProductAddBookingDetailsOverrides[]**](ProductAddBookingDetailsOverrides.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


