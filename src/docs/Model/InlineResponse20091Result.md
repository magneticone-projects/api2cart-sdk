# InlineResponse20091Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**returns** | [**\Api2Cart\Client\Model\ModelResponseReturnList[]**](ModelResponseReturnList.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


