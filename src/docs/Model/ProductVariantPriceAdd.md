# ProductVariantPriceAdd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Defines the variant to which the price has to be added | [optional] 
**product_id** | **string** | Product id | [optional] 
**group_prices** | [**\Api2Cart\Client\Model\ProductAddGroupPrices[]**](ProductAddGroupPrices.md) | Defines variants&#39;s group prices | 
**store_id** | **string** | Store Id | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


