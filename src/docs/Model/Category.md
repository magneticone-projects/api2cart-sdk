# Category

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**parent_id** | **string** |  | [optional] 
**created_at** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**modified_at** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**name** | **string** |  | [optional] 
**short_description** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**stores_ids** | **string[]** |  | [optional] 
**keywords** | **string** |  | [optional] 
**meta_description** | **string** |  | [optional] 
**meta_title** | **string** |  | [optional] 
**avail** | **bool** |  | [optional] 
**path** | **string** |  | [optional] 
**seo_url** | **string** |  | [optional] 
**sort_order** | **int** |  | [optional] 
**images** | [**\Api2Cart\Client\Model\Image[]**](Image.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


