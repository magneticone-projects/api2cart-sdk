# ProductPriceUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_id** | **string** | Defines the product where the price has to be updated | [optional] 
**group_prices** | [**\Api2Cart\Client\Model\ProductPriceUpdateGroupPrices[]**](ProductPriceUpdateGroupPrices.md) | Defines product&#39;s group prices | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


