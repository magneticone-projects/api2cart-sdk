# ProductVariantUpdateBatchImages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **string** |  | 
**type** | **string** |  | [optional] 
**label** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**position** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


