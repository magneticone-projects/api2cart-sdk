# InlineResponse20031Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**coupons_count** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


