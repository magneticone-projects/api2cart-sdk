# ProductGroupItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**child_item_id** | **string** |  | [optional] 
**product_id** | **string** |  | [optional] 
**default_qty_in_pack** | **string** |  | [optional] 
**is_qty_in_pack_fixed** | **bool** |  | [optional] 
**price** | **float** |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


