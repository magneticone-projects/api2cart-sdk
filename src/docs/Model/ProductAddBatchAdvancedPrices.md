# ProductAddBatchAdvancedPrices

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **float** |  | 
**group_id** | **int** |  | [optional] 
**quantity** | **float** |  | 
**start_time** | **string** |  | [optional] 
**expire_time** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


