# Cart

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**url** | **string** |  | [optional] 
**version** | **string** |  | [optional] 
**db_prefix** | **string** |  | [optional] 
**stores_info** | [**\Api2Cart\Client\Model\CartStoreInfo[]**](CartStoreInfo.md) |  | [optional] 
**warehouses** | [**\Api2Cart\Client\Model\CartWarehouse[]**](CartWarehouse.md) |  | [optional] 
**shipping_zones** | [**\Api2Cart\Client\Model\CartShippingZone[]**](CartShippingZone.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


