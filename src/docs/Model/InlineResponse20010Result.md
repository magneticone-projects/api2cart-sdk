# InlineResponse20010Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assigned** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


