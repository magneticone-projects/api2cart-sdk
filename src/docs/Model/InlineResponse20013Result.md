# InlineResponse20013Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unassigned** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


