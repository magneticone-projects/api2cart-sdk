# BasketItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**parent_id** | **string** |  | [optional] 
**product_id** | **string** |  | [optional] 
**variant_id** | **string** |  | [optional] 
**sku** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**price** | **float** |  | [optional] 
**tax** | **float** |  | [optional] 
**quantity** | **float** |  | [optional] 
**weight_unit** | **string** |  | [optional] 
**weight** | **float** |  | [optional] 
**options** | [**\Api2Cart\Client\Model\BasketItemOption[]**](BasketItemOption.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


