# ProductAddBestOffer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**minimum_offer_price** | **float** |  | [optional] 
**auto_accept_price** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


