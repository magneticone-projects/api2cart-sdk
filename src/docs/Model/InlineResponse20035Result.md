# InlineResponse20035Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all_plugins** | **int** |  | [optional] 
**plugins** | [**\Api2Cart\Client\Model\PluginList[]**](PluginList.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


