# ProductAddSalesTax

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tax_percent** | **float** |  | [optional] 
**tax_state** | **string** |  | [optional] 
**shipping_inc_in_tax** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


