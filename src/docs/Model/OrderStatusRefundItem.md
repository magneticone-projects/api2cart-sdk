# OrderStatusRefundItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_id** | **string** |  | [optional] 
**variant_id** | **string** |  | [optional] 
**order_product_id** | **string** |  | [optional] 
**qty** | **float** |  | [optional] 
**refund** | **float** |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


