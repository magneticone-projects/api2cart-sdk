# BatchJobResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_id** | **int** |  | [optional] 
**job_name** | **string** |  | [optional] 
**items_processed** | **int** |  | [optional] 
**items_succeed** | **int** |  | [optional] 
**items** | [**\Api2Cart\Client\Model\BatchJobResultItem[]**](BatchJobResultItem.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


