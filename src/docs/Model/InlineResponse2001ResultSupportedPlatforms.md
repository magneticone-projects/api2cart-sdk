# InlineResponse2001ResultSupportedPlatforms

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cart_id** | **string** |  | [optional] 
**cart_name** | **string** |  | [optional] 
**cart_versions** | **string** |  | [optional] 
**cart_method** | **string** |  | [optional] 
**params** | [**\Api2Cart\Client\Model\InlineResponse2001ResultParams**](InlineResponse2001ResultParams.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


