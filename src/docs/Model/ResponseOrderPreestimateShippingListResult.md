# ResponseOrderPreestimateShippingListResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**preestimate_shippings_count** | **int** |  | [optional] 
**preestimate_shippings** | [**\Api2Cart\Client\Model\OrderPreestimateShipping[]**](OrderPreestimateShipping.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


