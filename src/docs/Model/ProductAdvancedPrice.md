# ProductAdvancedPrice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**value** | **float** |  | [optional] 
**avail** | **bool** |  | [optional] 
**group_id** | **string** |  | [optional] 
**quantity_from** | **float** |  | [optional] 
**start_time** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**expire_time** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


