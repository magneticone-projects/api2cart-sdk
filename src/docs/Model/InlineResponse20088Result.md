# InlineResponse20088Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_variant_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


