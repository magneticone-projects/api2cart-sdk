# InlineResponse2008ResultSupportedCarts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cart_id** | **string** |  | [optional] 
**cart_name** | **string** |  | [optional] 
**cart_versions** | **string** |  | [optional] 
**params** | **string[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


