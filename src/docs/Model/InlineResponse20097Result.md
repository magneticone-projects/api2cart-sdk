# InlineResponse20097Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhook** | [**\Api2Cart\Client\Model\Webhook[]**](Webhook.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


