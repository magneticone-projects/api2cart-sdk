# CatalogPriceRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**gid** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**short_description** | **string** |  | [optional] 
**avail** | **bool** |  | [optional] 
**actions** | [**\Api2Cart\Client\Model\CatalogPriceRuleAction[]**](CatalogPriceRuleAction.md) |  | [optional] 
**created_time** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**date_start** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**date_end** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**usage_count** | **float** |  | [optional] 
**conditions** | [**\Api2Cart\Client\Model\CouponCondition[]**](CouponCondition.md) |  | [optional] 
**uses_per_order_limit** | **int** |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


