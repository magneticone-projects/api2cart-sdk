# InlineResponse20079Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**option** | [**\Api2Cart\Client\Model\ProductOption[]**](ProductOption.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


