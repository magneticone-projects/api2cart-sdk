# InlineResponse20026Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**store_name** | **string** |  | [optional] 
**store_url** | **string** |  | [optional] 
**db_prefix** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


