# InlineResponse20017Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**live_shipping_services** | [**\Api2Cart\Client\Model\BasketLiveShippingService[]**](BasketLiveShippingService.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


