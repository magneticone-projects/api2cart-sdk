# Basket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**customer** | [**\Api2Cart\Client\Model\BaseCustomer**](BaseCustomer.md) |  | [optional] 
**basket_url** | **string** |  | [optional] 
**created_at** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**modified_at** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**currency** | [**\Api2Cart\Client\Model\Currency**](Currency.md) |  | [optional] 
**basket_products** | [**\Api2Cart\Client\Model\BasketItem[]**](BasketItem.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


