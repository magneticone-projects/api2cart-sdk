# ShipmentItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_id** | **string** |  | [optional] 
**order_product_id** | **string** |  | [optional] 
**model** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**price** | **float** |  | [optional] 
**quantity** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


