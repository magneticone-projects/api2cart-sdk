# CouponCondition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**entity** | **string** |  | [optional] 
**match_items** | **string** |  | [optional] 
**key** | **string** |  | [optional] 
**operator** | **string** |  | [optional] 
**value** | **string** |  | [optional] 
**logic_operator** | **string** |  | [optional] 
**sub_conditions** | [**\Api2Cart\Client\Model\CouponCondition[]**](CouponCondition.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


