# Child

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**parent_id** | **string** |  | [optional] 
**sku** | **string** |  | [optional] 
**upc** | **string** |  | [optional] 
**ean** | **string** |  | [optional] 
**mpn** | **string** |  | [optional] 
**gtin** | **string** |  | [optional] 
**isbn** | **string** |  | [optional] 
**url** | **string** |  | [optional] 
**seo_url** | **string** |  | [optional] 
**sort_order** | **int** |  | [optional] 
**created_time** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**modified_time** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**name** | **string** |  | [optional] 
**short_description** | **string** |  | [optional] 
**full_description** | **string** |  | [optional] 
**images** | [**\Api2Cart\Client\Model\Image[]**](Image.md) |  | [optional] 
**combination** | [**\Api2Cart\Client\Model\ProductChildItemCombination[]**](ProductChildItemCombination.md) |  | [optional] 
**default_price** | **float** |  | [optional] 
**cost_price** | **float** |  | [optional] 
**list_price** | **float** |  | [optional] 
**wholesale_price** | **float** |  | [optional] 
**advanced_price** | [**\Api2Cart\Client\Model\ProductAdvancedPrice[]**](ProductAdvancedPrice.md) |  | [optional] 
**tax_class_id** | **string** |  | [optional] 
**avail_for_sale** | **bool** |  | [optional] 
**allow_backorders** | **bool** |  | [optional] 
**in_stock** | **bool** |  | [optional] 
**manage_stock** | **bool** |  | [optional] 
**inventory_level** | **float** |  | [optional] 
**inventory** | [**\Api2Cart\Client\Model\ProductInventory[]**](ProductInventory.md) |  | [optional] 
**min_quantity** | **float** |  | [optional] 
**default_qty_in_pack** | **float** |  | [optional] 
**is_qty_in_pack_fixed** | **bool** |  | [optional] 
**weight_unit** | **string** |  | [optional] 
**weight** | **float** |  | [optional] 
**dimensions_unit** | **string** |  | [optional] 
**width** | **float** |  | [optional] 
**height** | **float** |  | [optional] 
**length** | **float** |  | [optional] 
**meta_title** | **string** |  | [optional] 
**meta_description** | **string** |  | [optional] 
**meta_keywords** | **string** |  | [optional] 
**discounts** | [**\Api2Cart\Client\Model\Discount[]**](Discount.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


