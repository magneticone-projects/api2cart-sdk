# InlineResponse20087Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**variant** | [**\Api2Cart\Client\Model\Product[]**](Product.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


