# ResponseOrderTransactionListResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transactions_count** | **int** |  | [optional] 
**transactions** | [**\Api2Cart\Client\Model\OrderTransaction[]**](OrderTransaction.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


