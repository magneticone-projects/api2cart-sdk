# InlineResponse20022Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supported_carts** | [**\Api2Cart\Client\Model\InlineResponse20022ResultSupportedCarts[]**](InlineResponse20022ResultSupportedCarts.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


