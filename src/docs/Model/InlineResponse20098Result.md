# InlineResponse20098Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | [**\Api2Cart\Client\Model\InlineResponse20098ResultEvents[]**](InlineResponse20098ResultEvents.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


