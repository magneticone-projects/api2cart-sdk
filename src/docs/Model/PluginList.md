# PluginList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all_plugins** | **int** |  | [optional] 
**plugins** | [**\Api2Cart\Client\Model\Plugin[]**](Plugin.md) |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


