# Order

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**order_id** | **string** |  | [optional] 
**basket_id** | **string** |  | [optional] 
**channel_id** | **string** |  | [optional] 
**customer** | [**\Api2Cart\Client\Model\BaseCustomer**](BaseCustomer.md) |  | [optional] 
**create_at** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**currency** | [**\Api2Cart\Client\Model\Currency**](Currency.md) |  | [optional] 
**shipping_address** | [**\Api2Cart\Client\Model\CustomerAddress**](CustomerAddress.md) |  | [optional] 
**billing_address** | [**\Api2Cart\Client\Model\CustomerAddress**](CustomerAddress.md) |  | [optional] 
**payment_method** | [**\Api2Cart\Client\Model\OrderPaymentMethod**](OrderPaymentMethod.md) |  | [optional] 
**shipping_method** | [**\Api2Cart\Client\Model\OrderShippingMethod**](OrderShippingMethod.md) |  | [optional] 
**shipping_methods** | [**\Api2Cart\Client\Model\OrderShippingMethod[]**](OrderShippingMethod.md) |  | [optional] 
**status** | [**\Api2Cart\Client\Model\OrderStatus**](OrderStatus.md) |  | [optional] 
**totals** | [**\Api2Cart\Client\Model\OrderTotals**](OrderTotals.md) |  | [optional] 
**total** | [**\Api2Cart\Client\Model\OrderTotal**](OrderTotal.md) |  | [optional] 
**discounts** | [**\Api2Cart\Client\Model\OrderTotalsNewDiscount[]**](OrderTotalsNewDiscount.md) |  | [optional] 
**order_products** | [**\Api2Cart\Client\Model\OrderItem[]**](OrderItem.md) |  | [optional] 
**bundles** | [**\Api2Cart\Client\Model\OrderItem[]**](OrderItem.md) |  | [optional] 
**modified_at** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**finished_time** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**comment** | **string** |  | [optional] 
**store_id** | **string** |  | [optional] 
**warehouses_ids** | **string[]** |  | [optional] 
**refunds** | [**\Api2Cart\Client\Model\OrderRefund[]**](OrderRefund.md) |  | [optional] 
**gift_message** | **string** |  | [optional] 
**order_details_url** | **string** |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


