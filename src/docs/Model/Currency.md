# Currency

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**iso3** | **string** |  | [optional] 
**symbol_left** | **string** |  | [optional] 
**symbol_right** | **string** |  | [optional] 
**rate** | **float** |  | [optional] 
**avail** | **bool** |  | [optional] 
**default** | **bool** |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


