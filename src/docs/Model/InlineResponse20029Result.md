# InlineResponse20029Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**catalog_price_rules_count** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


