# InlineResponse200Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all_failed_webhook** | **string** |  | [optional] 
**webhook** | [**\Api2Cart\Client\Model\InlineResponse200ResultWebhook[]**](InlineResponse200ResultWebhook.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


