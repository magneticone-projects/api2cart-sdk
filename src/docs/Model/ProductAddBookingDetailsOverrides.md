# ProductAddBookingDetailsOverrides

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | **string** |  | 
**date** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


