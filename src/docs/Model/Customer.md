# Customer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**first_name** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**created_time** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**modified_time** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**group** | [**\Api2Cart\Client\Model\CustomerGroup[]**](CustomerGroup.md) |  | [optional] 
**login** | **string** |  | [optional] 
**last_login** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**birth_day** | [**\Api2Cart\Client\Model\A2CDateTime**](A2CDateTime.md) |  | [optional] 
**status** | **string** |  | [optional] 
**news_letter_subscription** | **bool** |  | [optional] 
**consents** | [**\Api2Cart\Client\Model\CustomerConsent[]**](CustomerConsent.md) |  | [optional] 
**gender** | **string** |  | [optional] 
**stores_ids** | **string[]** |  | [optional] 
**website** | **string** |  | [optional] 
**fax** | **string** |  | [optional] 
**company** | **string** |  | [optional] 
**ip_address** | **string** |  | [optional] 
**address_book** | [**\Api2Cart\Client\Model\CustomerAddress[]**](CustomerAddress.md) |  | [optional] 
**lang_id** | **string** |  | [optional] 
**orders_count** | **int** |  | [optional] 
**last_order_id** | **string** |  | [optional] 
**additional_fields** | **object** |  | [optional] 
**custom_fields** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


