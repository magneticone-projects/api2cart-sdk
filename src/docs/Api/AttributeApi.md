# Api2Cart\Client\AttributeApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**attributeAdd**](AttributeApi.md#attributeAdd) | **POST** /attribute.add.json | 
[**attributeAssignGroup**](AttributeApi.md#attributeAssignGroup) | **POST** /attribute.assign.group.json | 
[**attributeAssignSet**](AttributeApi.md#attributeAssignSet) | **POST** /attribute.assign.set.json | 
[**attributeAttributesetList**](AttributeApi.md#attributeAttributesetList) | **GET** /attribute.attributeset.list.json | 
[**attributeCount**](AttributeApi.md#attributeCount) | **GET** /attribute.count.json | 
[**attributeDelete**](AttributeApi.md#attributeDelete) | **DELETE** /attribute.delete.json | 
[**attributeGroupList**](AttributeApi.md#attributeGroupList) | **GET** /attribute.group.list.json | 
[**attributeInfo**](AttributeApi.md#attributeInfo) | **GET** /attribute.info.json | 
[**attributeList**](AttributeApi.md#attributeList) | **GET** /attribute.list.json | 
[**attributeTypeList**](AttributeApi.md#attributeTypeList) | **GET** /attribute.type.list.json | 
[**attributeUnassignGroup**](AttributeApi.md#attributeUnassignGroup) | **POST** /attribute.unassign.group.json | 
[**attributeUnassignSet**](AttributeApi.md#attributeUnassignSet) | **POST** /attribute.unassign.set.json | 
[**attributeUpdate**](AttributeApi.md#attributeUpdate) | **POST** /attribute.update.json | 
[**attributeValueAdd**](AttributeApi.md#attributeValueAdd) | **POST** /attribute.value.add.json | 
[**attributeValueDelete**](AttributeApi.md#attributeValueDelete) | **DELETE** /attribute.value.delete.json | 
[**attributeValueUpdate**](AttributeApi.md#attributeValueUpdate) | **PUT** /attribute.value.update.json | 


# **attributeAdd**
> \Api2Cart\Client\Model\InlineResponse2007 attributeAdd($type, $name, $code, $store_id, $lang_id, $visible, $required, $position, $attribute_group_id, $is_global, $is_searchable, $is_filterable, $is_comparable, $is_html_allowed_on_front, $is_filterable_in_search, $is_configurable, $is_visible_in_advanced_search, $is_used_for_promo_rules, $used_in_product_listing, $used_for_sort_by, $apply_to)



Add new attribute

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$type = "type_example"; // string | Defines attribute's type
$name = "name_example"; // string | Defines attributes's name
$code = "code_example"; // string | Entity code
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$visible = false; // bool | Set visibility status
$required = false; // bool | Defines if the option is required
$position = 0; // int | Attribute`s position
$attribute_group_id = "attribute_group_id_example"; // string | Filter by attribute_group_id
$is_global = "Store"; // string | Attribute saving scope
$is_searchable = false; // bool | Use attribute in Quick Search
$is_filterable = "No"; // string | Use In Layered Navigation
$is_comparable = false; // bool | Comparable on Front-end
$is_html_allowed_on_front = false; // bool | Allow HTML Tags on Frontend
$is_filterable_in_search = false; // bool | Use In Search Results Layered Navigation
$is_configurable = false; // bool | Use To Create Configurable Product
$is_visible_in_advanced_search = false; // bool | Use in Advanced Search
$is_used_for_promo_rules = false; // bool | Use for Promo Rule Conditions
$used_in_product_listing = false; // bool | Used in Product Listing
$used_for_sort_by = false; // bool | Used for Sorting in Product Listing
$apply_to = "all_types"; // string | Types of products which can have this attribute

try {
    $result = $apiInstance->attributeAdd($type, $name, $code, $store_id, $lang_id, $visible, $required, $position, $attribute_group_id, $is_global, $is_searchable, $is_filterable, $is_comparable, $is_html_allowed_on_front, $is_filterable_in_search, $is_configurable, $is_visible_in_advanced_search, $is_used_for_promo_rules, $used_in_product_listing, $used_for_sort_by, $apply_to);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **string**| Defines attribute&#39;s type |
 **name** | **string**| Defines attributes&#39;s name |
 **code** | **string**| Entity code | [optional]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **visible** | **bool**| Set visibility status | [optional] [default to false]
 **required** | **bool**| Defines if the option is required | [optional] [default to false]
 **position** | **int**| Attribute&#x60;s position | [optional] [default to 0]
 **attribute_group_id** | **string**| Filter by attribute_group_id | [optional]
 **is_global** | **string**| Attribute saving scope | [optional] [default to Store]
 **is_searchable** | **bool**| Use attribute in Quick Search | [optional] [default to false]
 **is_filterable** | **string**| Use In Layered Navigation | [optional] [default to No]
 **is_comparable** | **bool**| Comparable on Front-end | [optional] [default to false]
 **is_html_allowed_on_front** | **bool**| Allow HTML Tags on Frontend | [optional] [default to false]
 **is_filterable_in_search** | **bool**| Use In Search Results Layered Navigation | [optional] [default to false]
 **is_configurable** | **bool**| Use To Create Configurable Product | [optional] [default to false]
 **is_visible_in_advanced_search** | **bool**| Use in Advanced Search | [optional] [default to false]
 **is_used_for_promo_rules** | **bool**| Use for Promo Rule Conditions | [optional] [default to false]
 **used_in_product_listing** | **bool**| Used in Product Listing | [optional] [default to false]
 **used_for_sort_by** | **bool**| Used for Sorting in Product Listing | [optional] [default to false]
 **apply_to** | **string**| Types of products which can have this attribute | [optional] [default to all_types]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeAssignGroup**
> \Api2Cart\Client\Model\InlineResponse20010 attributeAssignGroup($id, $group_id, $attribute_set_id)



Assign attribute to the group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$group_id = "group_id_example"; // string | Attribute group_id
$attribute_set_id = "attribute_set_id_example"; // string | Attribute set id

try {
    $result = $apiInstance->attributeAssignGroup($id, $group_id, $attribute_set_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeAssignGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **group_id** | **string**| Attribute group_id |
 **attribute_set_id** | **string**| Attribute set id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20010**](../Model/InlineResponse20010.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeAssignSet**
> \Api2Cart\Client\Model\InlineResponse20010 attributeAssignSet($id, $attribute_set_id, $group_id)



Assign attribute to the attribute set

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$attribute_set_id = "attribute_set_id_example"; // string | Attribute set id
$group_id = "group_id_example"; // string | Attribute group_id

try {
    $result = $apiInstance->attributeAssignSet($id, $attribute_set_id, $group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeAssignSet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **attribute_set_id** | **string**| Attribute set id |
 **group_id** | **string**| Attribute group_id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20010**](../Model/InlineResponse20010.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeAttributesetList**
> \Api2Cart\Client\Model\InlineResponse20011 attributeAttributesetList($start, $count, $params, $exclude, $response_fields)



Get attribute_set list

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$params = "id,name"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve

try {
    $result = $apiInstance->attributeAttributesetList($start, $count, $params, $exclude, $response_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeAttributesetList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20011**](../Model/InlineResponse20011.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeCount**
> \Api2Cart\Client\Model\InlineResponse2006 attributeCount($type, $store_id, $lang_id, $visible, $required, $system)



Get attributes count

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$type = "type_example"; // string | Defines attribute's type
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$visible = true; // bool | Filter items by visibility status
$required = true; // bool | Defines if the option is required
$system = true; // bool | True if attribute is system

try {
    $result = $apiInstance->attributeCount($type, $store_id, $lang_id, $visible, $required, $system);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **string**| Defines attribute&#39;s type | [optional]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **visible** | **bool**| Filter items by visibility status | [optional]
 **required** | **bool**| Defines if the option is required | [optional]
 **system** | **bool**| True if attribute is system | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2006**](../Model/InlineResponse2006.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeDelete**
> \Api2Cart\Client\Model\InlineResponse2009 attributeDelete($id, $store_id)



Delete attribute from store

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->attributeDelete($id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeGroupList**
> \Api2Cart\Client\Model\InlineResponse20011 attributeGroupList($start, $count, $lang_id, $params, $exclude, $response_fields, $attribute_set_id)



Get attribute group list

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$lang_id = "lang_id_example"; // string | Language id
$params = "id,name"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$attribute_set_id = "attribute_set_id_example"; // string | Attribute set id

try {
    $result = $apiInstance->attributeGroupList($start, $count, $lang_id, $params, $exclude, $response_fields, $attribute_set_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeGroupList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **lang_id** | **string**| Language id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **attribute_set_id** | **string**| Attribute set id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20011**](../Model/InlineResponse20011.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeInfo**
> \Api2Cart\Client\Model\InlineResponse2005 attributeInfo($id, $store_id, $lang_id, $params, $exclude, $response_fields)



Get information about a specific global attribute by its ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$params = "force_all"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve

try {
    $result = $apiInstance->attributeInfo($id, $store_id, $lang_id, $params, $exclude, $response_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to force_all]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2005**](../Model/InlineResponse2005.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeList**
> \Api2Cart\Client\Model\ModelResponseAttributeList attributeList($start, $count, $type, $attribute_ids, $store_id, $lang_id, $params, $exclude, $response_fields, $visible, $required, $system)



Get a list of global attributes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$type = "type_example"; // string | Defines attribute's type
$attribute_ids = "attribute_ids_example"; // string | Filter attributes by ids
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Retrieves attributes on specified language id
$params = "id,name,code,type"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$visible = true; // bool | Filter items by visibility status
$required = true; // bool | Defines if the option is required
$system = true; // bool | True if attribute is system

try {
    $result = $apiInstance->attributeList($start, $count, $type, $attribute_ids, $store_id, $lang_id, $params, $exclude, $response_fields, $visible, $required, $system);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **type** | **string**| Defines attribute&#39;s type | [optional]
 **attribute_ids** | **string**| Filter attributes by ids | [optional]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Retrieves attributes on specified language id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name,code,type]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **visible** | **bool**| Filter items by visibility status | [optional]
 **required** | **bool**| Defines if the option is required | [optional]
 **system** | **bool**| True if attribute is system | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseAttributeList**](../Model/ModelResponseAttributeList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeTypeList**
> \Api2Cart\Client\Model\InlineResponse20012 attributeTypeList()



Get list of supported attributes types

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->attributeTypeList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeTypeList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Api2Cart\Client\Model\InlineResponse20012**](../Model/InlineResponse20012.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeUnassignGroup**
> \Api2Cart\Client\Model\InlineResponse20013 attributeUnassignGroup($id, $group_id)



Unassign attribute from group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$group_id = "group_id_example"; // string | Customer group_id

try {
    $result = $apiInstance->attributeUnassignGroup($id, $group_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeUnassignGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **group_id** | **string**| Customer group_id |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20013**](../Model/InlineResponse20013.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeUnassignSet**
> \Api2Cart\Client\Model\InlineResponse20013 attributeUnassignSet($id, $attribute_set_id)



Unassign attribute from attribute set

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$attribute_set_id = "attribute_set_id_example"; // string | Attribute set id

try {
    $result = $apiInstance->attributeUnassignSet($id, $attribute_set_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeUnassignSet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **attribute_set_id** | **string**| Attribute set id |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20013**](../Model/InlineResponse20013.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeUpdate**
> \Api2Cart\Client\Model\InlineResponse2008 attributeUpdate($id, $name, $store_id, $lang_id)



Update attribute data

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$name = "name_example"; // string | Defines new attributes's name
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id

try {
    $result = $apiInstance->attributeUpdate($id, $name, $store_id, $lang_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **name** | **string**| Defines new attributes&#39;s name |
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2008**](../Model/InlineResponse2008.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeValueAdd**
> \Api2Cart\Client\Model\InlineResponse2007 attributeValueAdd($attribute_id, $name, $code, $description, $store_id, $lang_id)



Add new value to attribute.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$attribute_id = "attribute_id_example"; // string | Attribute Id
$name = "name_example"; // string | Defines attribute value's name
$code = "code_example"; // string | Entity code
$description = "description_example"; // string | Defines attribute value's description
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id

try {
    $result = $apiInstance->attributeValueAdd($attribute_id, $name, $code, $description, $store_id, $lang_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeValueAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attribute_id** | **string**| Attribute Id |
 **name** | **string**| Defines attribute value&#39;s name |
 **code** | **string**| Entity code | [optional]
 **description** | **string**| Defines attribute value&#39;s description | [optional]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeValueDelete**
> \Api2Cart\Client\Model\InlineResponse20014 attributeValueDelete($id, $attribute_id, $store_id)



Delete attribute value.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$attribute_id = "attribute_id_example"; // string | Attribute Id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->attributeValueDelete($id, $attribute_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeValueDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **attribute_id** | **string**| Attribute Id |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20014**](../Model/InlineResponse20014.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attributeValueUpdate**
> \Api2Cart\Client\Model\InlineResponse2008 attributeValueUpdate($id, $attribute_id, $name, $description, $code, $store_id, $lang_id)



Update attribute value.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AttributeApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Defines attribute value's id
$attribute_id = "attribute_id_example"; // string | Attribute Id
$name = "name_example"; // string | Defines attribute value's name
$description = "description_example"; // string | Defines new attribute value's description
$code = "code_example"; // string | Entity code
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id

try {
    $result = $apiInstance->attributeValueUpdate($id, $attribute_id, $name, $description, $code, $store_id, $lang_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttributeApi->attributeValueUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Defines attribute value&#39;s id |
 **attribute_id** | **string**| Attribute Id |
 **name** | **string**| Defines attribute value&#39;s name | [optional]
 **description** | **string**| Defines new attribute value&#39;s description | [optional]
 **code** | **string**| Entity code | [optional]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2008**](../Model/InlineResponse2008.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

