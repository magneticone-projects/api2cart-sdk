# Api2Cart\Client\TaxApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**taxClassInfo**](TaxApi.md#taxClassInfo) | **GET** /tax.class.info.json | 
[**taxClassList**](TaxApi.md#taxClassList) | **GET** /tax.class.list.json | 


# **taxClassInfo**
> \Api2Cart\Client\Model\ModelResponseTaxClassInfo taxClassInfo($tax_class_id, $store_id, $lang_id, $params, $response_fields, $exclude)



Use this method to get information about a tax class and its rates. It allows you to calculate the tax percentage for a specific customer's address. This information contains relatively static data that rarely changes, so API2Cart may cache certain data to reduce the load on the store and speed up request execution. We also recommend that you cache the response of this method on your side to save requests. If you need to clear the cache for a specific store, use the cart.validate method.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\TaxApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tax_class_id = "tax_class_id_example"; // string | Retrieves taxes specified by class id
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$params = "tax_class_id,name,avail"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all

try {
    $result = $apiInstance->taxClassInfo($tax_class_id, $store_id, $lang_id, $params, $response_fields, $exclude);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TaxApi->taxClassInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tax_class_id** | **string**| Retrieves taxes specified by class id |
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to tax_class_id,name,avail]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseTaxClassInfo**](../Model/ModelResponseTaxClassInfo.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **taxClassList**
> \Api2Cart\Client\Model\ModelResponseTaxClassList taxClassList($created_to, $created_from, $modified_to, $modified_from, $find_value, $find_where, $store_id, $count, $page_cursor, $response_fields)



Get list of tax classes from your store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\TaxApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "find_where_example"; // string | Tax class search that is specified by field
$store_id = "store_id_example"; // string | Store Id
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$response_fields = "{return_code,return_message,pagination,result}"; // string | Set this parameter in order to choose which entity fields you want to retrieve

try {
    $result = $apiInstance->taxClassList($created_to, $created_from, $modified_to, $modified_from, $find_value, $find_where, $store_id, $count, $page_cursor, $response_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TaxApi->taxClassList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **find_value** | **string**| Entity search that is specified by some value | [optional]
 **find_where** | **string**| Tax class search that is specified by field | [optional]
 **store_id** | **string**| Store Id | [optional]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to {return_code,return_message,pagination,result}]

### Return type

[**\Api2Cart\Client\Model\ModelResponseTaxClassList**](../Model/ModelResponseTaxClassList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

