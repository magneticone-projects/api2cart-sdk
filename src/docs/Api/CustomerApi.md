# Api2Cart\Client\CustomerApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerAdd**](CustomerApi.md#customerAdd) | **POST** /customer.add.json | 
[**customerAddressAdd**](CustomerApi.md#customerAddressAdd) | **POST** /customer.address.add.json | 
[**customerAttributeList**](CustomerApi.md#customerAttributeList) | **GET** /customer.attribute.list.json | 
[**customerCount**](CustomerApi.md#customerCount) | **GET** /customer.count.json | 
[**customerDelete**](CustomerApi.md#customerDelete) | **DELETE** /customer.delete.json | 
[**customerFind**](CustomerApi.md#customerFind) | **GET** /customer.find.json | 
[**customerGroupAdd**](CustomerApi.md#customerGroupAdd) | **POST** /customer.group.add.json | 
[**customerGroupList**](CustomerApi.md#customerGroupList) | **GET** /customer.group.list.json | 
[**customerInfo**](CustomerApi.md#customerInfo) | **GET** /customer.info.json | 
[**customerList**](CustomerApi.md#customerList) | **GET** /customer.list.json | 
[**customerUpdate**](CustomerApi.md#customerUpdate) | **PUT** /customer.update.json | 
[**customerWishlistList**](CustomerApi.md#customerWishlistList) | **GET** /customer.wishlist.list.json | 


# **customerAdd**
> \Api2Cart\Client\Model\InlineResponse20047 customerAdd($body)



Add customer into store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\CustomerAdd(); // \Api2Cart\Client\Model\CustomerAdd | 

try {
    $result = $apiInstance->customerAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\CustomerAdd**](../Model/CustomerAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20047**](../Model/InlineResponse20047.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAddressAdd**
> \Api2Cart\Client\Model\InlineResponse2007 customerAddressAdd($body)



Add customer address.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\CustomerAddressAdd(); // \Api2Cart\Client\Model\CustomerAddressAdd | 

try {
    $result = $apiInstance->customerAddressAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerAddressAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\CustomerAddressAdd**](../Model/CustomerAddressAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerAttributeList**
> \Api2Cart\Client\Model\ModelResponseCustomerAttributeList customerAttributeList($customer_id, $count, $page_cursor, $store_id, $lang_id, $params, $exclude, $response_fields)



Get attributes for specific customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Retrieves orders specified by customer id
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$params = "force_all"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve

try {
    $result = $apiInstance->customerAttributeList($customer_id, $count, $page_cursor, $store_id, $lang_id, $params, $exclude, $response_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerAttributeList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Retrieves orders specified by customer id |
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to force_all]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseCustomerAttributeList**](../Model/ModelResponseCustomerAttributeList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerCount**
> \Api2Cart\Client\Model\InlineResponse20045 customerCount($group_id, $created_from, $created_to, $modified_from, $modified_to, $store_id, $customer_list_id, $avail, $find_value, $find_where, $ids, $since_id)



Get number of customers from store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group_id = "group_id_example"; // string | Customer group_id
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$store_id = "store_id_example"; // string | Counts customer specified by store id
$customer_list_id = "customer_list_id_example"; // string | The numeric ID of the customer list in Demandware.
$avail = true; // bool | Defines category's visibility status
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "find_where_example"; // string | Counts customers that are searched specified by field
$ids = "ids_example"; // string | Counts customers specified by ids
$since_id = "since_id_example"; // string | Retrieve entities starting from the specified id.

try {
    $result = $apiInstance->customerCount($group_id, $created_from, $created_to, $modified_from, $modified_to, $store_id, $customer_list_id, $avail, $find_value, $find_where, $ids, $since_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **string**| Customer group_id | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **store_id** | **string**| Counts customer specified by store id | [optional]
 **customer_list_id** | **string**| The numeric ID of the customer list in Demandware. | [optional]
 **avail** | **bool**| Defines category&#39;s visibility status | [optional] [default to true]
 **find_value** | **string**| Entity search that is specified by some value | [optional]
 **find_where** | **string**| Counts customers that are searched specified by field | [optional]
 **ids** | **string**| Counts customers specified by ids | [optional]
 **since_id** | **string**| Retrieve entities starting from the specified id. | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20045**](../Model/InlineResponse20045.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerDelete**
> \Api2Cart\Client\Model\InlineResponse20049 customerDelete($id)



Delete customer from store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Identifies customer specified by the id

try {
    $result = $apiInstance->customerDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Identifies customer specified by the id |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20049**](../Model/InlineResponse20049.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerFind**
> \Api2Cart\Client\Model\InlineResponse20046 customerFind($find_value, $find_where, $find_params, $store_id)



Find customers in store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "email"; // string | Entity search that is specified by the comma-separated unique fields
$find_params = "whole_words"; // string | Entity search that is specified by comma-separated parameters
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->customerFind($find_value, $find_where, $find_params, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **find_value** | **string**| Entity search that is specified by some value |
 **find_where** | **string**| Entity search that is specified by the comma-separated unique fields | [optional] [default to email]
 **find_params** | **string**| Entity search that is specified by comma-separated parameters | [optional] [default to whole_words]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20046**](../Model/InlineResponse20046.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGroupAdd**
> \Api2Cart\Client\Model\InlineResponse20050 customerGroupAdd($name, $store_id, $stores_ids)



Create customer group.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = "name_example"; // string | Customer group name
$store_id = "store_id_example"; // string | Store Id
$stores_ids = "stores_ids_example"; // string | Assign customer group to the stores that is specified by comma-separated stores' id

try {
    $result = $apiInstance->customerGroupAdd($name, $store_id, $stores_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGroupAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**| Customer group name |
 **store_id** | **string**| Store Id | [optional]
 **stores_ids** | **string**| Assign customer group to the stores that is specified by comma-separated stores&#39; id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20050**](../Model/InlineResponse20050.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGroupList**
> \Api2Cart\Client\Model\ModelResponseCustomerGroupList customerGroupList($disable_cache, $page_cursor, $start, $count, $store_id, $lang_id, $group_ids, $params, $exclude, $response_fields)



Get list of customers groups.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$disable_cache = false; // bool | Disable cache for current request
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$group_ids = "group_ids_example"; // string | Groups that will be assigned to a customer
$params = "id,name,additional_fields"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve

try {
    $result = $apiInstance->customerGroupList($disable_cache, $page_cursor, $start, $count, $store_id, $lang_id, $group_ids, $params, $exclude, $response_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGroupList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **disable_cache** | **bool**| Disable cache for current request | [optional] [default to false]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **group_ids** | **string**| Groups that will be assigned to a customer | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name,additional_fields]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseCustomerGroupList**](../Model/ModelResponseCustomerGroupList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInfo**
> \Api2Cart\Client\Model\InlineResponse20044 customerInfo($id, $params, $response_fields, $exclude, $store_id)



Get customers' details from store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Retrieves customer's info specified by customer id
$params = "id,email,first_name,last_name"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$store_id = "store_id_example"; // string | Retrieves customer info specified by store id

try {
    $result = $apiInstance->customerInfo($id, $params, $response_fields, $exclude, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Retrieves customer&#39;s info specified by customer id |
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,email,first_name,last_name]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **store_id** | **string**| Retrieves customer info specified by store id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20044**](../Model/InlineResponse20044.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerList**
> \Api2Cart\Client\Model\ModelResponseCustomerList customerList($page_cursor, $start, $count, $created_from, $created_to, $modified_from, $modified_to, $params, $response_fields, $exclude, $group_id, $store_id, $customer_list_id, $avail, $find_value, $find_where, $ids, $since_id)



Get list of customers from store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$params = "id,email,first_name,last_name"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$group_id = "group_id_example"; // string | Customer group_id
$store_id = "store_id_example"; // string | Retrieves customers specified by store id
$customer_list_id = "customer_list_id_example"; // string | The numeric ID of the customer list in Demandware.
$avail = true; // bool | Defines category's visibility status
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "find_where_example"; // string | Customer search that is specified by field
$ids = "ids_example"; // string | Retrieves customers specified by ids
$since_id = "since_id_example"; // string | Retrieve entities starting from the specified id.

try {
    $result = $apiInstance->customerList($page_cursor, $start, $count, $created_from, $created_to, $modified_from, $modified_to, $params, $response_fields, $exclude, $group_id, $store_id, $customer_list_id, $avail, $find_value, $find_where, $ids, $since_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,email,first_name,last_name]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **group_id** | **string**| Customer group_id | [optional]
 **store_id** | **string**| Retrieves customers specified by store id | [optional]
 **customer_list_id** | **string**| The numeric ID of the customer list in Demandware. | [optional]
 **avail** | **bool**| Defines category&#39;s visibility status | [optional] [default to true]
 **find_value** | **string**| Entity search that is specified by some value | [optional]
 **find_where** | **string**| Customer search that is specified by field | [optional]
 **ids** | **string**| Retrieves customers specified by ids | [optional]
 **since_id** | **string**| Retrieve entities starting from the specified id. | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseCustomerList**](../Model/ModelResponseCustomerList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerUpdate**
> \Api2Cart\Client\Model\InlineResponse20048 customerUpdate($body)



Update information of customer in store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\CustomerUpdate(); // \Api2Cart\Client\Model\CustomerUpdate | 

try {
    $result = $apiInstance->customerUpdate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\CustomerUpdate**](../Model/CustomerUpdate.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20048**](../Model/InlineResponse20048.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerWishlistList**
> \Api2Cart\Client\Model\ModelResponseCustomerWishlistList customerWishlistList($customer_id, $id, $store_id, $start, $count, $page_cursor, $response_fields)



Get a Wish List of customer from the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CustomerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Retrieves orders specified by customer id
$id = "id_example"; // string | Entity id
$store_id = "store_id_example"; // string | Store Id
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$response_fields = "{return_code,return_message,pagination,result}"; // string | Set this parameter in order to choose which entity fields you want to retrieve

try {
    $result = $apiInstance->customerWishlistList($customer_id, $id, $store_id, $start, $count, $page_cursor, $response_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerWishlistList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Retrieves orders specified by customer id |
 **id** | **string**| Entity id | [optional]
 **store_id** | **string**| Store Id | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to {return_code,return_message,pagination,result}]

### Return type

[**\Api2Cart\Client\Model\ModelResponseCustomerWishlistList**](../Model/ModelResponseCustomerWishlistList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

