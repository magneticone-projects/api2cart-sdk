# Api2Cart\Client\ProductApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**productAdd**](ProductApi.md#productAdd) | **POST** /product.add.json | 
[**productAddBatch**](ProductApi.md#productAddBatch) | **POST** /product.add.batch.json | 
[**productAttributeList**](ProductApi.md#productAttributeList) | **GET** /product.attribute.list.json | 
[**productAttributeValueSet**](ProductApi.md#productAttributeValueSet) | **POST** /product.attribute.value.set.json | 
[**productAttributeValueUnset**](ProductApi.md#productAttributeValueUnset) | **POST** /product.attribute.value.unset.json | 
[**productBrandList**](ProductApi.md#productBrandList) | **GET** /product.brand.list.json | 
[**productChildItemFind**](ProductApi.md#productChildItemFind) | **GET** /product.child_item.find.json | 
[**productChildItemInfo**](ProductApi.md#productChildItemInfo) | **GET** /product.child_item.info.json | 
[**productChildItemList**](ProductApi.md#productChildItemList) | **GET** /product.child_item.list.json | 
[**productCount**](ProductApi.md#productCount) | **GET** /product.count.json | 
[**productCurrencyAdd**](ProductApi.md#productCurrencyAdd) | **POST** /product.currency.add.json | 
[**productCurrencyList**](ProductApi.md#productCurrencyList) | **GET** /product.currency.list.json | 
[**productDelete**](ProductApi.md#productDelete) | **DELETE** /product.delete.json | 
[**productDeleteBatch**](ProductApi.md#productDeleteBatch) | **POST** /product.delete.batch.json | 
[**productFields**](ProductApi.md#productFields) | **GET** /product.fields.json | 
[**productFind**](ProductApi.md#productFind) | **GET** /product.find.json | 
[**productImageAdd**](ProductApi.md#productImageAdd) | **POST** /product.image.add.json | 
[**productImageDelete**](ProductApi.md#productImageDelete) | **DELETE** /product.image.delete.json | 
[**productImageUpdate**](ProductApi.md#productImageUpdate) | **PUT** /product.image.update.json | 
[**productInfo**](ProductApi.md#productInfo) | **GET** /product.info.json | 
[**productList**](ProductApi.md#productList) | **GET** /product.list.json | 
[**productManufacturerAdd**](ProductApi.md#productManufacturerAdd) | **POST** /product.manufacturer.add.json | 
[**productOptionAdd**](ProductApi.md#productOptionAdd) | **POST** /product.option.add.json | 
[**productOptionAssign**](ProductApi.md#productOptionAssign) | **POST** /product.option.assign.json | 
[**productOptionDelete**](ProductApi.md#productOptionDelete) | **DELETE** /product.option.delete.json | 
[**productOptionList**](ProductApi.md#productOptionList) | **GET** /product.option.list.json | 
[**productOptionValueAdd**](ProductApi.md#productOptionValueAdd) | **POST** /product.option.value.add.json | 
[**productOptionValueAssign**](ProductApi.md#productOptionValueAssign) | **POST** /product.option.value.assign.json | 
[**productOptionValueDelete**](ProductApi.md#productOptionValueDelete) | **DELETE** /product.option.value.delete.json | 
[**productOptionValueUpdate**](ProductApi.md#productOptionValueUpdate) | **PUT** /product.option.value.update.json | 
[**productPriceAdd**](ProductApi.md#productPriceAdd) | **POST** /product.price.add.json | 
[**productPriceDelete**](ProductApi.md#productPriceDelete) | **DELETE** /product.price.delete.json | 
[**productPriceUpdate**](ProductApi.md#productPriceUpdate) | **PUT** /product.price.update.json | 
[**productReviewList**](ProductApi.md#productReviewList) | **GET** /product.review.list.json | 
[**productStoreAssign**](ProductApi.md#productStoreAssign) | **POST** /product.store.assign.json | 
[**productTaxAdd**](ProductApi.md#productTaxAdd) | **POST** /product.tax.add.json | 
[**productUpdate**](ProductApi.md#productUpdate) | **PUT** /product.update.json | 
[**productUpdateBatch**](ProductApi.md#productUpdateBatch) | **POST** /product.update.batch.json | 
[**productVariantAdd**](ProductApi.md#productVariantAdd) | **POST** /product.variant.add.json | 
[**productVariantAddBatch**](ProductApi.md#productVariantAddBatch) | **POST** /product.variant.add.batch.json | 
[**productVariantCount**](ProductApi.md#productVariantCount) | **GET** /product.variant.count.json | 
[**productVariantDelete**](ProductApi.md#productVariantDelete) | **DELETE** /product.variant.delete.json | 
[**productVariantDeleteBatch**](ProductApi.md#productVariantDeleteBatch) | **POST** /product.variant.delete.batch.json | 
[**productVariantImageAdd**](ProductApi.md#productVariantImageAdd) | **POST** /product.variant.image.add.json | 
[**productVariantImageDelete**](ProductApi.md#productVariantImageDelete) | **DELETE** /product.variant.image.delete.json | 
[**productVariantInfo**](ProductApi.md#productVariantInfo) | **GET** /product.variant.info.json | 
[**productVariantList**](ProductApi.md#productVariantList) | **GET** /product.variant.list.json | 
[**productVariantPriceAdd**](ProductApi.md#productVariantPriceAdd) | **POST** /product.variant.price.add.json | 
[**productVariantPriceDelete**](ProductApi.md#productVariantPriceDelete) | **DELETE** /product.variant.price.delete.json | 
[**productVariantPriceUpdate**](ProductApi.md#productVariantPriceUpdate) | **PUT** /product.variant.price.update.json | 
[**productVariantUpdate**](ProductApi.md#productVariantUpdate) | **PUT** /product.variant.update.json | 
[**productVariantUpdateBatch**](ProductApi.md#productVariantUpdateBatch) | **POST** /product.variant.update.batch.json | 


# **productAdd**
> \Api2Cart\Client\Model\InlineResponse20067 productAdd($body)



Add new product to store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductAdd(); // \Api2Cart\Client\Model\ProductAdd | 

try {
    $result = $apiInstance->productAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductAdd**](../Model/ProductAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20067**](../Model/InlineResponse20067.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productAddBatch**
> \Api2Cart\Client\Model\InlineResponse20042 productAddBatch($body)



Add new products to the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductAddBatch(); // \Api2Cart\Client\Model\ProductAddBatch | 

try {
    $result = $apiInstance->productAddBatch($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productAddBatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductAddBatch**](../Model/ProductAddBatch.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20042**](../Model/InlineResponse20042.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productAttributeList**
> \Api2Cart\Client\Model\ModelResponseProductAttributeList productAttributeList($product_id, $attribute_id, $variant_id, $page_cursor, $start, $count, $attribute_group_id, $set_name, $lang_id, $store_id, $sort_by, $sort_direction, $params, $response_fields, $exclude)



Get list of attributes and values.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Retrieves attributes specified by product id
$attribute_id = "attribute_id_example"; // string | Retrieves info for specified attribute_id
$variant_id = "variant_id_example"; // string | Defines product's variants specified by variant id
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$attribute_group_id = "attribute_group_id_example"; // string | Filter by attribute_group_id
$set_name = "set_name_example"; // string | Retrieves attributes specified by set_name in Magento
$lang_id = "lang_id_example"; // string | Retrieves attributes specified by language id
$store_id = "store_id_example"; // string | Retrieves attributes specified by store id
$sort_by = "attribute_id"; // string | Set field to sort by
$sort_direction = "asc"; // string | Set sorting direction
$params = "attribute_id,name"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all

try {
    $result = $apiInstance->productAttributeList($product_id, $attribute_id, $variant_id, $page_cursor, $start, $count, $attribute_group_id, $set_name, $lang_id, $store_id, $sort_by, $sort_direction, $params, $response_fields, $exclude);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productAttributeList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Retrieves attributes specified by product id |
 **attribute_id** | **string**| Retrieves info for specified attribute_id | [optional]
 **variant_id** | **string**| Defines product&#39;s variants specified by variant id | [optional]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **attribute_group_id** | **string**| Filter by attribute_group_id | [optional]
 **set_name** | **string**| Retrieves attributes specified by set_name in Magento | [optional]
 **lang_id** | **string**| Retrieves attributes specified by language id | [optional]
 **store_id** | **string**| Retrieves attributes specified by store id | [optional]
 **sort_by** | **string**| Set field to sort by | [optional] [default to attribute_id]
 **sort_direction** | **string**| Set sorting direction | [optional] [default to asc]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to attribute_id,name]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseProductAttributeList**](../Model/ModelResponseProductAttributeList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productAttributeValueSet**
> \Api2Cart\Client\Model\InlineResponse20069 productAttributeValueSet($product_id, $attribute_id, $attribute_group_id, $attribute_name, $value, $value_id, $lang_id, $store_id)



Set attribute value to product.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Defines product id where the attribute should be added
$attribute_id = "attribute_id_example"; // string | Filter by attribute_id
$attribute_group_id = "attribute_group_id_example"; // string | Filter by attribute_group_id
$attribute_name = "attribute_name_example"; // string | Define attribute name
$value = "value_example"; // string | Define attribute value
$value_id = 56; // int | Define attribute value id
$lang_id = "lang_id_example"; // string | Language id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productAttributeValueSet($product_id, $attribute_id, $attribute_group_id, $attribute_name, $value, $value_id, $lang_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productAttributeValueSet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Defines product id where the attribute should be added |
 **attribute_id** | **string**| Filter by attribute_id | [optional]
 **attribute_group_id** | **string**| Filter by attribute_group_id | [optional]
 **attribute_name** | **string**| Define attribute name | [optional]
 **value** | **string**| Define attribute value | [optional]
 **value_id** | **int**| Define attribute value id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20069**](../Model/InlineResponse20069.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productAttributeValueUnset**
> \Api2Cart\Client\Model\InlineResponse20070 productAttributeValueUnset($product_id, $attribute_id, $store_id, $include_default, $reindex, $clear_cache)



Removes attribute value for a product.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Product id
$attribute_id = "attribute_id_example"; // string | Attribute Id
$store_id = "store_id_example"; // string | Store Id
$include_default = false; // bool | Boolean, whether or not to unset default value of the attribute, if applicable
$reindex = true; // bool | Is reindex required
$clear_cache = true; // bool | Is cache clear required

try {
    $result = $apiInstance->productAttributeValueUnset($product_id, $attribute_id, $store_id, $include_default, $reindex, $clear_cache);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productAttributeValueUnset: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Product id |
 **attribute_id** | **string**| Attribute Id |
 **store_id** | **string**| Store Id | [optional]
 **include_default** | **bool**| Boolean, whether or not to unset default value of the attribute, if applicable | [optional] [default to false]
 **reindex** | **bool**| Is reindex required | [optional] [default to true]
 **clear_cache** | **bool**| Is cache clear required | [optional] [default to true]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20070**](../Model/InlineResponse20070.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productBrandList**
> \Api2Cart\Client\Model\InlineResponse20071 productBrandList($start, $count, $page_cursor, $params, $brand_ids, $exclude, $store_id, $lang_id, $created_from, $created_to, $modified_from, $modified_to, $response_fields, $find_where, $find_value)



Get list of brands from your store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$params = "id,name,short_description,active,url"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$brand_ids = "brand_ids_example"; // string | Retrieves brands specified by brand ids
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$find_where = "find_where_example"; // string | Entity search that is specified by the comma-separated unique fields
$find_value = "find_value_example"; // string | Entity search that is specified by some value

try {
    $result = $apiInstance->productBrandList($start, $count, $page_cursor, $params, $brand_ids, $exclude, $store_id, $lang_id, $created_from, $created_to, $modified_from, $modified_to, $response_fields, $find_where, $find_value);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productBrandList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name,short_description,active,url]
 **brand_ids** | **string**| Retrieves brands specified by brand ids | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **find_where** | **string**| Entity search that is specified by the comma-separated unique fields | [optional]
 **find_value** | **string**| Entity search that is specified by some value | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20071**](../Model/InlineResponse20071.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productChildItemFind**
> \Api2Cart\Client\Model\InlineResponse20073 productChildItemFind($find_value, $find_where, $find_params, $store_id)



Search product child item (bundled item or configurable product variant) in store catalog.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "find_where_example"; // string | Entity search that is specified by the comma-separated unique fields
$find_params = "whole_words"; // string | Entity search that is specified by comma-separated parameters
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productChildItemFind($find_value, $find_where, $find_params, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productChildItemFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **find_value** | **string**| Entity search that is specified by some value | [optional]
 **find_where** | **string**| Entity search that is specified by the comma-separated unique fields | [optional]
 **find_params** | **string**| Entity search that is specified by comma-separated parameters | [optional] [default to whole_words]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20073**](../Model/InlineResponse20073.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productChildItemInfo**
> \Api2Cart\Client\Model\InlineResponse20072 productChildItemInfo($product_id, $id, $params, $response_fields, $exclude, $store_id, $lang_id, $currency_id, $use_latest_api_version)



Get child for specific product.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Filter by parent product id
$id = "id_example"; // string | Entity id
$params = "force_all"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$currency_id = "currency_id_example"; // string | Currency Id
$use_latest_api_version = false; // bool | Use the latest platform API version

try {
    $result = $apiInstance->productChildItemInfo($product_id, $id, $params, $response_fields, $exclude, $store_id, $lang_id, $currency_id, $use_latest_api_version);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productChildItemInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Filter by parent product id |
 **id** | **string**| Entity id |
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to force_all]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **currency_id** | **string**| Currency Id | [optional]
 **use_latest_api_version** | **bool**| Use the latest platform API version | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20072**](../Model/InlineResponse20072.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productChildItemList**
> \Api2Cart\Client\Model\ModelResponseProductChildItemList productChildItemList($page_cursor, $start, $count, $params, $response_fields, $exclude, $created_from, $created_to, $modified_from, $modified_to, $product_id, $product_ids, $sku, $store_id, $lang_id, $currency_id, $avail_sale, $find_value, $find_where, $report_request_id, $disable_report_cache, $use_latest_api_version, $return_global)



Get a list of a product's child items, such as variants or bundle components. The total_count field in the response indicates the total number of items in the context of the current filter.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page_cursor = "page_cursor_example"; // string | Used to retrieve products child items via cursor-based pagination (it can't be used with any other filtering parameter)
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$params = "force_all"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$product_id = "product_id_example"; // string | Filter by parent product id
$product_ids = "product_ids_example"; // string | Filter by parent product ids
$sku = "sku_example"; // string | Filter by products variant's sku
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$currency_id = "currency_id_example"; // string | Currency Id
$avail_sale = true; // bool | Specifies the set of available/not available products for sale
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "find_where_example"; // string | Child products search that is specified by field
$report_request_id = "report_request_id_example"; // string | Report request id
$disable_report_cache = false; // bool | Disable report cache for current request
$use_latest_api_version = false; // bool | Use the latest platform API version
$return_global = false; // bool | Determines the type of products to be returned. If set to 'true', only global products will be returned; if set to 'false', only local products will be returned.

try {
    $result = $apiInstance->productChildItemList($page_cursor, $start, $count, $params, $response_fields, $exclude, $created_from, $created_to, $modified_from, $modified_to, $product_id, $product_ids, $sku, $store_id, $lang_id, $currency_id, $avail_sale, $find_value, $find_where, $report_request_id, $disable_report_cache, $use_latest_api_version, $return_global);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productChildItemList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_cursor** | **string**| Used to retrieve products child items via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to force_all]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **product_id** | **string**| Filter by parent product id | [optional]
 **product_ids** | **string**| Filter by parent product ids | [optional]
 **sku** | **string**| Filter by products variant&#39;s sku | [optional]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **currency_id** | **string**| Currency Id | [optional]
 **avail_sale** | **bool**| Specifies the set of available/not available products for sale | [optional]
 **find_value** | **string**| Entity search that is specified by some value | [optional]
 **find_where** | **string**| Child products search that is specified by field | [optional]
 **report_request_id** | **string**| Report request id | [optional]
 **disable_report_cache** | **bool**| Disable report cache for current request | [optional] [default to false]
 **use_latest_api_version** | **bool**| Use the latest platform API version | [optional] [default to false]
 **return_global** | **bool**| Determines the type of products to be returned. If set to &#39;true&#39;, only global products will be returned; if set to &#39;false&#39;, only local products will be returned. | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\ModelResponseProductChildItemList**](../Model/ModelResponseProductChildItemList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productCount**
> \Api2Cart\Client\Model\InlineResponse20065 productCount($category_id, $created_from, $created_to, $modified_from, $modified_to, $avail_view, $avail_sale, $store_id, $lang_id, $product_ids, $since_id, $report_request_id, $disable_report_cache, $brand_name, $product_attributes, $status, $type, $find_value, $find_where, $use_latest_api_version, $return_global, $categories_ids)



Count products in store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$category_id = "category_id_example"; // string | Counts products specified by category id
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$avail_view = true; // bool | Specifies the set of visible/invisible products
$avail_sale = true; // bool | Specifies the set of available/not available products for sale
$store_id = "store_id_example"; // string | Counts products specified by store id
$lang_id = "lang_id_example"; // string | Counts products specified by language id
$product_ids = "product_ids_example"; // string | Counts products specified by product ids
$since_id = "since_id_example"; // string | Retrieve entities starting from the specified id.
$report_request_id = "report_request_id_example"; // string | Report request id
$disable_report_cache = false; // bool | Disable report cache for current request
$brand_name = "brand_name_example"; // string | Retrieves brands specified by brand name
$product_attributes = array("product_attributes_example"); // string[] | Defines product attributes
$status = "status_example"; // string | Defines product's status
$type = "type_example"; // string | Defines products's type
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "find_where_example"; // string | Counts products that are searched specified by field
$use_latest_api_version = false; // bool | Use the latest platform API version
$return_global = false; // bool | Determines the type of products to be returned. If set to 'true', only global products will be returned; if set to 'false', only local products will be returned.
$categories_ids = "categories_ids_example"; // string | Defines product add that is specified by comma-separated categories id

try {
    $result = $apiInstance->productCount($category_id, $created_from, $created_to, $modified_from, $modified_to, $avail_view, $avail_sale, $store_id, $lang_id, $product_ids, $since_id, $report_request_id, $disable_report_cache, $brand_name, $product_attributes, $status, $type, $find_value, $find_where, $use_latest_api_version, $return_global, $categories_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_id** | **string**| Counts products specified by category id | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **avail_view** | **bool**| Specifies the set of visible/invisible products | [optional]
 **avail_sale** | **bool**| Specifies the set of available/not available products for sale | [optional]
 **store_id** | **string**| Counts products specified by store id | [optional]
 **lang_id** | **string**| Counts products specified by language id | [optional]
 **product_ids** | **string**| Counts products specified by product ids | [optional]
 **since_id** | **string**| Retrieve entities starting from the specified id. | [optional]
 **report_request_id** | **string**| Report request id | [optional]
 **disable_report_cache** | **bool**| Disable report cache for current request | [optional] [default to false]
 **brand_name** | **string**| Retrieves brands specified by brand name | [optional]
 **product_attributes** | [**string[]**](../Model/string.md)| Defines product attributes | [optional]
 **status** | **string**| Defines product&#39;s status | [optional]
 **type** | **string**| Defines products&#39;s type | [optional]
 **find_value** | **string**| Entity search that is specified by some value | [optional]
 **find_where** | **string**| Counts products that are searched specified by field | [optional]
 **use_latest_api_version** | **bool**| Use the latest platform API version | [optional] [default to false]
 **return_global** | **bool**| Determines the type of products to be returned. If set to &#39;true&#39;, only global products will be returned; if set to &#39;false&#39;, only local products will be returned. | [optional] [default to false]
 **categories_ids** | **string**| Defines product add that is specified by comma-separated categories id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20065**](../Model/InlineResponse20065.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productCurrencyAdd**
> \Api2Cart\Client\Model\InlineResponse20075 productCurrencyAdd($iso3, $rate, $name, $avail, $symbol_left, $symbol_right, $default)



Add currency and/or set default in store

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$iso3 = "iso3_example"; // string | Specifies standardized currency code
$rate = 8.14; // float | Defines the numerical identifier against to the major currency
$name = "name_example"; // string | Defines currency's name
$avail = true; // bool | Specifies whether the currency is available
$symbol_left = "symbol_left_example"; // string | Defines the symbol that is located before the currency
$symbol_right = "symbol_right_example"; // string | Defines the symbol that is located after the currency
$default = false; // bool | Specifies currency's default meaning

try {
    $result = $apiInstance->productCurrencyAdd($iso3, $rate, $name, $avail, $symbol_left, $symbol_right, $default);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productCurrencyAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iso3** | **string**| Specifies standardized currency code |
 **rate** | **float**| Defines the numerical identifier against to the major currency |
 **name** | **string**| Defines currency&#39;s name | [optional]
 **avail** | **bool**| Specifies whether the currency is available | [optional] [default to true]
 **symbol_left** | **string**| Defines the symbol that is located before the currency | [optional]
 **symbol_right** | **string**| Defines the symbol that is located after the currency | [optional]
 **default** | **bool**| Specifies currency&#39;s default meaning | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20075**](../Model/InlineResponse20075.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productCurrencyList**
> \Api2Cart\Client\Model\InlineResponse20074 productCurrencyList($start, $count, $params, $page_cursor, $exclude, $response_fields, $default, $avail)



Get list of currencies

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$params = "name,iso3,default,avail"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$default = true; // bool | Specifies the set of default/not default currencies
$avail = true; // bool | Specifies the set of available/not available currencies

try {
    $result = $apiInstance->productCurrencyList($start, $count, $params, $page_cursor, $exclude, $response_fields, $default, $avail);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productCurrencyList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to name,iso3,default,avail]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **default** | **bool**| Specifies the set of default/not default currencies | [optional]
 **avail** | **bool**| Specifies the set of available/not available currencies | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20074**](../Model/InlineResponse20074.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productDelete**
> \Api2Cart\Client\Model\InlineResponse20068 productDelete($id, $store_id)



Product delete

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Product id that will be removed
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productDelete($id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Product id that will be removed |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20068**](../Model/InlineResponse20068.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productDeleteBatch**
> \Api2Cart\Client\Model\InlineResponse20042 productDeleteBatch($body)



Remove product from the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductDeleteBatch(); // \Api2Cart\Client\Model\ProductDeleteBatch | 

try {
    $result = $apiInstance->productDeleteBatch($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productDeleteBatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductDeleteBatch**](../Model/ProductDeleteBatch.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20042**](../Model/InlineResponse20042.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productFields**
> \Api2Cart\Client\Model\InlineResponse20030 productFields()



Retrieve all available fields for product item in store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->productFields();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productFields: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Api2Cart\Client\Model\InlineResponse20030**](../Model/InlineResponse20030.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productFind**
> \Api2Cart\Client\Model\InlineResponse20066 productFind($find_value, $find_where, $find_params, $find_what, $lang_id, $store_id)



Search product in store catalog. \"Apple\" is specified here by default.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "name"; // string | Entity search that is specified by the comma-separated unique fields
$find_params = "whole_words"; // string | Entity search that is specified by comma-separated parameters
$find_what = "product"; // string | Parameter's value specifies the entity that has to be found
$lang_id = "lang_id_example"; // string | Search products specified by language id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productFind($find_value, $find_where, $find_params, $find_what, $lang_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **find_value** | **string**| Entity search that is specified by some value |
 **find_where** | **string**| Entity search that is specified by the comma-separated unique fields | [optional] [default to name]
 **find_params** | **string**| Entity search that is specified by comma-separated parameters | [optional] [default to whole_words]
 **find_what** | **string**| Parameter&#39;s value specifies the entity that has to be found | [optional] [default to product]
 **lang_id** | **string**| Search products specified by language id | [optional]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20066**](../Model/InlineResponse20066.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productImageAdd**
> \Api2Cart\Client\Model\InlineResponse20076 productImageAdd($body)



Add image to product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductImageAdd(); // \Api2Cart\Client\Model\ProductImageAdd | 

try {
    $result = $apiInstance->productImageAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productImageAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductImageAdd**](../Model/ProductImageAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20076**](../Model/InlineResponse20076.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productImageDelete**
> \Api2Cart\Client\Model\InlineResponse2009 productImageDelete($product_id, $id, $store_id)



Delete image

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Defines product id where the image should be deleted
$id = "id_example"; // string | Entity id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productImageDelete($product_id, $id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productImageDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Defines product id where the image should be deleted |
 **id** | **string**| Entity id |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productImageUpdate**
> \Api2Cart\Client\Model\InlineResponse20077 productImageUpdate($product_id, $id, $variant_ids, $image_name, $type, $label, $position, $store_id, $lang_id, $hidden)



Update details of image

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Defines product id where the image should be updated
$id = "id_example"; // string | Defines image update specified by image id
$variant_ids = "variant_ids_example"; // string | Defines product's variants ids
$image_name = "image_name_example"; // string | Defines image's name
$type = "additional"; // string | Defines image's types that are specified by comma-separated list
$label = "label_example"; // string | Defines alternative text that has to be attached to the picture
$position = 56; // int | Defines image’s position in the list
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$hidden = true; // bool | Define is hide image

try {
    $result = $apiInstance->productImageUpdate($product_id, $id, $variant_ids, $image_name, $type, $label, $position, $store_id, $lang_id, $hidden);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productImageUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Defines product id where the image should be updated |
 **id** | **string**| Defines image update specified by image id |
 **variant_ids** | **string**| Defines product&#39;s variants ids | [optional]
 **image_name** | **string**| Defines image&#39;s name | [optional]
 **type** | **string**| Defines image&#39;s types that are specified by comma-separated list | [optional] [default to additional]
 **label** | **string**| Defines alternative text that has to be attached to the picture | [optional]
 **position** | **int**| Defines image’s position in the list | [optional]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **hidden** | **bool**| Define is hide image | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20077**](../Model/InlineResponse20077.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productInfo**
> \Api2Cart\Client\Model\InlineResponse20064 productInfo($id, $params, $response_fields, $exclude, $store_id, $lang_id, $currency_id, $report_request_id, $disable_report_cache, $use_latest_api_version)



Get information about a specific product by its ID. In the case of a multistore configuration, use the store_id filter to get a response in the context of a specific store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Retrieves product's info specified by product id
$params = "id,name,description,price,categories_ids"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$store_id = "store_id_example"; // string | Retrieves product info specified by store id
$lang_id = "lang_id_example"; // string | Retrieves product info specified by language id
$currency_id = "currency_id_example"; // string | Currency Id
$report_request_id = "report_request_id_example"; // string | Report request id
$disable_report_cache = false; // bool | Disable report cache for current request
$use_latest_api_version = false; // bool | Use the latest platform API version

try {
    $result = $apiInstance->productInfo($id, $params, $response_fields, $exclude, $store_id, $lang_id, $currency_id, $report_request_id, $disable_report_cache, $use_latest_api_version);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Retrieves product&#39;s info specified by product id |
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name,description,price,categories_ids]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **store_id** | **string**| Retrieves product info specified by store id | [optional]
 **lang_id** | **string**| Retrieves product info specified by language id | [optional]
 **currency_id** | **string**| Currency Id | [optional]
 **report_request_id** | **string**| Report request id | [optional]
 **disable_report_cache** | **bool**| Disable report cache for current request | [optional] [default to false]
 **use_latest_api_version** | **bool**| Use the latest platform API version | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20064**](../Model/InlineResponse20064.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productList**
> \Api2Cart\Client\Model\ModelResponseProductList productList($page_cursor, $start, $count, $params, $response_fields, $exclude, $category_id, $created_from, $created_to, $modified_from, $modified_to, $avail_view, $avail_sale, $store_id, $lang_id, $currency_id, $product_ids, $since_id, $report_request_id, $disable_report_cache, $sort_by, $sort_direction, $sku, $disable_cache, $brand_name, $product_attributes, $status, $type, $find_value, $find_where, $use_latest_api_version, $return_global, $categories_ids)



Get list of products from your store. Returns 10 products by default.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page_cursor = "page_cursor_example"; // string | Used to retrieve products via cursor-based pagination (it can't be used with any other filtering parameter)
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$params = "id,name,description,price,categories_ids"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$category_id = "category_id_example"; // string | Retrieves products specified by category id
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$avail_view = true; // bool | Specifies the set of visible/invisible products
$avail_sale = true; // bool | Specifies the set of available/not available products for sale
$store_id = "store_id_example"; // string | Retrieves products specified by store id
$lang_id = "lang_id_example"; // string | Retrieves products specified by language id
$currency_id = "currency_id_example"; // string | Currency Id
$product_ids = "product_ids_example"; // string | Retrieves products specified by product ids
$since_id = "since_id_example"; // string | Retrieve entities starting from the specified id.
$report_request_id = "report_request_id_example"; // string | Report request id
$disable_report_cache = false; // bool | Disable report cache for current request
$sort_by = "id"; // string | Set field to sort by
$sort_direction = "asc"; // string | Set sorting direction
$sku = "sku_example"; // string | Filter by product's sku
$disable_cache = false; // bool | Disable cache for current request
$brand_name = "brand_name_example"; // string | Retrieves brands specified by brand name
$product_attributes = array("product_attributes_example"); // string[] | Defines product attributes
$status = "status_example"; // string | Defines product's status
$type = "type_example"; // string | Defines products's type
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "find_where_example"; // string | Product search that is specified by field
$use_latest_api_version = false; // bool | Use the latest platform API version
$return_global = false; // bool | Determines the type of products to be returned. If set to 'true', only global products will be returned; if set to 'false', only local products will be returned.
$categories_ids = "categories_ids_example"; // string | Retrieves products specified by categories ids

try {
    $result = $apiInstance->productList($page_cursor, $start, $count, $params, $response_fields, $exclude, $category_id, $created_from, $created_to, $modified_from, $modified_to, $avail_view, $avail_sale, $store_id, $lang_id, $currency_id, $product_ids, $since_id, $report_request_id, $disable_report_cache, $sort_by, $sort_direction, $sku, $disable_cache, $brand_name, $product_attributes, $status, $type, $find_value, $find_where, $use_latest_api_version, $return_global, $categories_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_cursor** | **string**| Used to retrieve products via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name,description,price,categories_ids]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **category_id** | **string**| Retrieves products specified by category id | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **avail_view** | **bool**| Specifies the set of visible/invisible products | [optional]
 **avail_sale** | **bool**| Specifies the set of available/not available products for sale | [optional]
 **store_id** | **string**| Retrieves products specified by store id | [optional]
 **lang_id** | **string**| Retrieves products specified by language id | [optional]
 **currency_id** | **string**| Currency Id | [optional]
 **product_ids** | **string**| Retrieves products specified by product ids | [optional]
 **since_id** | **string**| Retrieve entities starting from the specified id. | [optional]
 **report_request_id** | **string**| Report request id | [optional]
 **disable_report_cache** | **bool**| Disable report cache for current request | [optional] [default to false]
 **sort_by** | **string**| Set field to sort by | [optional] [default to id]
 **sort_direction** | **string**| Set sorting direction | [optional] [default to asc]
 **sku** | **string**| Filter by product&#39;s sku | [optional]
 **disable_cache** | **bool**| Disable cache for current request | [optional] [default to false]
 **brand_name** | **string**| Retrieves brands specified by brand name | [optional]
 **product_attributes** | [**string[]**](../Model/string.md)| Defines product attributes | [optional]
 **status** | **string**| Defines product&#39;s status | [optional]
 **type** | **string**| Defines products&#39;s type | [optional]
 **find_value** | **string**| Entity search that is specified by some value | [optional]
 **find_where** | **string**| Product search that is specified by field | [optional]
 **use_latest_api_version** | **bool**| Use the latest platform API version | [optional] [default to false]
 **return_global** | **bool**| Determines the type of products to be returned. If set to &#39;true&#39;, only global products will be returned; if set to &#39;false&#39;, only local products will be returned. | [optional] [default to false]
 **categories_ids** | **string**| Retrieves products specified by categories ids | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseProductList**](../Model/ModelResponseProductList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productManufacturerAdd**
> \Api2Cart\Client\Model\InlineResponse20078 productManufacturerAdd($product_id, $manufacturer, $store_id)



Add manufacturer to store and assign to product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Defines products specified by product id
$manufacturer = "manufacturer_example"; // string | Defines product’s manufacturer's name
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productManufacturerAdd($product_id, $manufacturer, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productManufacturerAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Defines products specified by product id |
 **manufacturer** | **string**| Defines product’s manufacturer&#39;s name |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20078**](../Model/InlineResponse20078.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productOptionAdd**
> \Api2Cart\Client\Model\InlineResponse20081 productOptionAdd($body)



Add product option from store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductOptionAdd(); // \Api2Cart\Client\Model\ProductOptionAdd | 

try {
    $result = $apiInstance->productOptionAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productOptionAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductOptionAdd**](../Model/ProductOptionAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20081**](../Model/InlineResponse20081.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productOptionAssign**
> \Api2Cart\Client\Model\InlineResponse20080 productOptionAssign($product_id, $option_id, $required, $sort_order, $option_values, $clear_cache)



Assign option from product.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Defines product id where the option should be assigned
$option_id = "option_id_example"; // string | Defines option id which has to be assigned
$required = false; // bool | Defines if the option is required
$sort_order = 0; // int | Sort number in the list
$option_values = "option_values_example"; // string | Defines option values that has to be assigned
$clear_cache = true; // bool | Is cache clear required

try {
    $result = $apiInstance->productOptionAssign($product_id, $option_id, $required, $sort_order, $option_values, $clear_cache);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productOptionAssign: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Defines product id where the option should be assigned |
 **option_id** | **string**| Defines option id which has to be assigned |
 **required** | **bool**| Defines if the option is required | [optional] [default to false]
 **sort_order** | **int**| Sort number in the list | [optional] [default to 0]
 **option_values** | **string**| Defines option values that has to be assigned | [optional]
 **clear_cache** | **bool**| Is cache clear required | [optional] [default to true]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20080**](../Model/InlineResponse20080.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productOptionDelete**
> \Api2Cart\Client\Model\InlineResponse2009 productOptionDelete($option_id, $product_id, $store_id)



Product option delete.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$option_id = "option_id_example"; // string | Defines option id that should be deleted
$product_id = "product_id_example"; // string | Defines product id where the option should be deleted
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productOptionDelete($option_id, $product_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productOptionDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **option_id** | **string**| Defines option id that should be deleted |
 **product_id** | **string**| Defines product id where the option should be deleted |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productOptionList**
> \Api2Cart\Client\Model\InlineResponse20079 productOptionList($start, $count, $params, $exclude, $response_fields, $product_id, $lang_id, $store_id)



Get list of options.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$params = "id,name,description"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$product_id = "product_id_example"; // string | Retrieves products' options specified by product id
$lang_id = "lang_id_example"; // string | Language id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productOptionList($start, $count, $params, $exclude, $response_fields, $product_id, $lang_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productOptionList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name,description]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **product_id** | **string**| Retrieves products&#39; options specified by product id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20079**](../Model/InlineResponse20079.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productOptionValueAdd**
> \Api2Cart\Client\Model\InlineResponse20083 productOptionValueAdd($product_id, $option_id, $option_value, $sort_order, $display_value, $is_default, $clear_cache)



Add product option item from option.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Defines product id where the option value should be added
$option_id = "option_id_example"; // string | Defines option id where the value has to be added
$option_value = "option_value_example"; // string | Defines option value that has to be added
$sort_order = 0; // int | Sort number in the list
$display_value = "display_value_example"; // string | Defines the value that will be displayed for the option value
$is_default = true; // bool | Defines as a default
$clear_cache = true; // bool | Is cache clear required

try {
    $result = $apiInstance->productOptionValueAdd($product_id, $option_id, $option_value, $sort_order, $display_value, $is_default, $clear_cache);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productOptionValueAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Defines product id where the option value should be added |
 **option_id** | **string**| Defines option id where the value has to be added |
 **option_value** | **string**| Defines option value that has to be added | [optional]
 **sort_order** | **int**| Sort number in the list | [optional] [default to 0]
 **display_value** | **string**| Defines the value that will be displayed for the option value | [optional]
 **is_default** | **bool**| Defines as a default | [optional]
 **clear_cache** | **bool**| Is cache clear required | [optional] [default to true]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20083**](../Model/InlineResponse20083.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productOptionValueAssign**
> \Api2Cart\Client\Model\InlineResponse20082 productOptionValueAssign($product_option_id, $option_value_id, $clear_cache)



Assign product option item from product.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_option_id = 56; // int | Defines product's option id where the value has to be assigned
$option_value_id = "option_value_id_example"; // string | Defines value id that has to be assigned
$clear_cache = true; // bool | Is cache clear required

try {
    $result = $apiInstance->productOptionValueAssign($product_option_id, $option_value_id, $clear_cache);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productOptionValueAssign: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_option_id** | **int**| Defines product&#39;s option id where the value has to be assigned |
 **option_value_id** | **string**| Defines value id that has to be assigned |
 **clear_cache** | **bool**| Is cache clear required | [optional] [default to true]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20082**](../Model/InlineResponse20082.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productOptionValueDelete**
> \Api2Cart\Client\Model\InlineResponse2009 productOptionValueDelete($option_id, $option_value_id, $product_id, $store_id)



Product option value delete.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$option_id = "option_id_example"; // string | Defines option id where the value should be deleted
$option_value_id = "option_value_id_example"; // string | Defines option value id that should be deleted
$product_id = "product_id_example"; // string | Defines product id where the option value should be deleted
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productOptionValueDelete($option_id, $option_value_id, $product_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productOptionValueDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **option_id** | **string**| Defines option id where the value should be deleted |
 **option_value_id** | **string**| Defines option value id that should be deleted |
 **product_id** | **string**| Defines product id where the option value should be deleted |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productOptionValueUpdate**
> \Api2Cart\Client\Model\InlineResponse2004 productOptionValueUpdate($product_id, $option_id, $option_value_id, $option_value, $price, $quantity, $display_value, $clear_cache)



Update product option item from option.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Defines product id where the option value should be updated
$option_id = "option_id_example"; // string | Defines option id where the value has to be updated
$option_value_id = "option_value_id_example"; // string | Defines value id that has to be assigned
$option_value = "option_value_example"; // string | Defines option value that has to be added
$price = 8.14; // float | Defines new product option price
$quantity = 8.14; // float | Defines new products' options quantity
$display_value = "display_value_example"; // string | Defines the value that will be displayed for the option value
$clear_cache = true; // bool | Is cache clear required

try {
    $result = $apiInstance->productOptionValueUpdate($product_id, $option_id, $option_value_id, $option_value, $price, $quantity, $display_value, $clear_cache);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productOptionValueUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Defines product id where the option value should be updated |
 **option_id** | **string**| Defines option id where the value has to be updated |
 **option_value_id** | **string**| Defines value id that has to be assigned |
 **option_value** | **string**| Defines option value that has to be added | [optional]
 **price** | **float**| Defines new product option price | [optional]
 **quantity** | **float**| Defines new products&#39; options quantity | [optional]
 **display_value** | **string**| Defines the value that will be displayed for the option value | [optional]
 **clear_cache** | **bool**| Is cache clear required | [optional] [default to true]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productPriceAdd**
> \Api2Cart\Client\Model\InlineResponse20021 productPriceAdd($body)



Add some prices to the product.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductPriceAdd(); // \Api2Cart\Client\Model\ProductPriceAdd | 

try {
    $result = $apiInstance->productPriceAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productPriceAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductPriceAdd**](../Model/ProductPriceAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20021**](../Model/InlineResponse20021.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productPriceDelete**
> \Api2Cart\Client\Model\InlineResponse2009 productPriceDelete($product_id, $group_prices, $store_id)



Delete some prices of the product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Defines the product where the price has to be deleted
$group_prices = "group_prices_example"; // string | Defines product's group prices
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productPriceDelete($product_id, $group_prices, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productPriceDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Defines the product where the price has to be deleted |
 **group_prices** | **string**| Defines product&#39;s group prices | [optional]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productPriceUpdate**
> \Api2Cart\Client\Model\InlineResponse2004 productPriceUpdate($body)



Update some prices of the product.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductPriceUpdate(); // \Api2Cart\Client\Model\ProductPriceUpdate | 

try {
    $result = $apiInstance->productPriceUpdate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productPriceUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductPriceUpdate**](../Model/ProductPriceUpdate.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productReviewList**
> \Api2Cart\Client\Model\InlineResponse20084 productReviewList($product_id, $start, $page_cursor, $count, $ids, $store_id, $status, $params, $exclude, $response_fields)



Get reviews of a specific product.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Product id
$start = 0; // int | This parameter sets the number from which you want to get entities
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$ids = "ids_example"; // string | Retrieves reviews specified by ids
$store_id = "store_id_example"; // string | Store Id
$status = "status_example"; // string | Defines status
$params = "id,customer_id,email,message,status,product_id,nick_name,summary,rating,ratings,status,created_time"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve

try {
    $result = $apiInstance->productReviewList($product_id, $start, $page_cursor, $count, $ids, $store_id, $status, $params, $exclude, $response_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productReviewList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Product id |
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **ids** | **string**| Retrieves reviews specified by ids | [optional]
 **store_id** | **string**| Store Id | [optional]
 **status** | **string**| Defines status | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,customer_id,email,message,status,product_id,nick_name,summary,rating,ratings,status,created_time]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20084**](../Model/InlineResponse20084.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productStoreAssign**
> \Api2Cart\Client\Model\InlineResponse2004 productStoreAssign($product_id, $store_id)



Assign product to store

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Defines id of the product which should be assigned to a store
$store_id = "store_id_example"; // string | Defines id of the store product should be assigned to

try {
    $result = $apiInstance->productStoreAssign($product_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productStoreAssign: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Defines id of the product which should be assigned to a store |
 **store_id** | **string**| Defines id of the store product should be assigned to |

### Return type

[**\Api2Cart\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productTaxAdd**
> \Api2Cart\Client\Model\InlineResponse20085 productTaxAdd($body)



Add tax class and tax rate to store and assign to product.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductTaxAdd(); // \Api2Cart\Client\Model\ProductTaxAdd | 

try {
    $result = $apiInstance->productTaxAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productTaxAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductTaxAdd**](../Model/ProductTaxAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20085**](../Model/InlineResponse20085.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productUpdate**
> \Api2Cart\Client\Model\InlineResponse2004 productUpdate($body)



This method can be used to update certain product data. The list of supported parameters depends on the specific platform. Please transmit only those parameters that are supported by the particular platform. Please note that to update the product quantity, it is recommended to use relative parameters (increase_quantity or reduce_quantity) to avoid unexpected overwrites on heavily loaded stores.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductUpdate(); // \Api2Cart\Client\Model\ProductUpdate | 

try {
    $result = $apiInstance->productUpdate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductUpdate**](../Model/ProductUpdate.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productUpdateBatch**
> \Api2Cart\Client\Model\InlineResponse20042 productUpdateBatch($body)



Update products on the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductUpdateBatch(); // \Api2Cart\Client\Model\ProductUpdateBatch | 

try {
    $result = $apiInstance->productUpdateBatch($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productUpdateBatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductUpdateBatch**](../Model/ProductUpdateBatch.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20042**](../Model/InlineResponse20042.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantAdd**
> \Api2Cart\Client\Model\InlineResponse20088 productVariantAdd($body)



Add variant to product.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductVariantAdd(); // \Api2Cart\Client\Model\ProductVariantAdd | 

try {
    $result = $apiInstance->productVariantAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductVariantAdd**](../Model/ProductVariantAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20088**](../Model/InlineResponse20088.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantAddBatch**
> \Api2Cart\Client\Model\InlineResponse20042 productVariantAddBatch($body)



Add new product variants to the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductVariantAddBatch(); // \Api2Cart\Client\Model\ProductVariantAddBatch | 

try {
    $result = $apiInstance->productVariantAddBatch($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantAddBatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductVariantAddBatch**](../Model/ProductVariantAddBatch.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20042**](../Model/InlineResponse20042.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantCount**
> \Api2Cart\Client\Model\InlineResponse20086 productVariantCount($product_id, $created_from, $created_to, $modified_from, $modified_to, $category_id, $store_id)



Get count variants.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Retrieves products' variants specified by product id
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$category_id = "category_id_example"; // string | Counts products’ variants specified by category id
$store_id = "store_id_example"; // string | Retrieves variants specified by store id

try {
    $result = $apiInstance->productVariantCount($product_id, $created_from, $created_to, $modified_from, $modified_to, $category_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Retrieves products&#39; variants specified by product id |
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **category_id** | **string**| Counts products’ variants specified by category id | [optional]
 **store_id** | **string**| Retrieves variants specified by store id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20086**](../Model/InlineResponse20086.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantDelete**
> \Api2Cart\Client\Model\InlineResponse20014 productVariantDelete($id, $product_id, $store_id)



Delete variant.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Defines variant removal, specified by variant id
$product_id = "product_id_example"; // string | Defines product's id where the variant has to be deleted
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productVariantDelete($id, $product_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Defines variant removal, specified by variant id |
 **product_id** | **string**| Defines product&#39;s id where the variant has to be deleted |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20014**](../Model/InlineResponse20014.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantDeleteBatch**
> \Api2Cart\Client\Model\InlineResponse20042 productVariantDeleteBatch($body)



Remove product variants from the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductVariantDeleteBatch(); // \Api2Cart\Client\Model\ProductVariantDeleteBatch | 

try {
    $result = $apiInstance->productVariantDeleteBatch($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantDeleteBatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductVariantDeleteBatch**](../Model/ProductVariantDeleteBatch.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20042**](../Model/InlineResponse20042.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantImageAdd**
> \Api2Cart\Client\Model\InlineResponse20076 productVariantImageAdd($body)



Add image to product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductVariantImageAdd(); // \Api2Cart\Client\Model\ProductVariantImageAdd | 

try {
    $result = $apiInstance->productVariantImageAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantImageAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductVariantImageAdd**](../Model/ProductVariantImageAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20076**](../Model/InlineResponse20076.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantImageDelete**
> \Api2Cart\Client\Model\InlineResponse2009 productVariantImageDelete($product_id, $product_variant_id, $id, $store_id)



Delete  image to product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Defines product id where the variant image should be deleted
$product_variant_id = "product_variant_id_example"; // string | Defines product's variants specified by variant id
$id = "id_example"; // string | Entity id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productVariantImageDelete($product_id, $product_variant_id, $id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantImageDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Defines product id where the variant image should be deleted |
 **product_variant_id** | **string**| Defines product&#39;s variants specified by variant id |
 **id** | **string**| Entity id |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantInfo**
> \Api2Cart\Client\Model\InlineResponse20064 productVariantInfo($id, $params, $exclude, $store_id)



Get variant info. This method is deprecated, and its development is stopped. Please use \"product.child_item.info\" instead.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Retrieves variant's info specified by variant id
$params = "id,name,description,price"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$store_id = "store_id_example"; // string | Retrieves variant info specified by store id

try {
    $result = $apiInstance->productVariantInfo($id, $params, $exclude, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Retrieves variant&#39;s info specified by variant id |
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name,description,price]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **store_id** | **string**| Retrieves variant info specified by store id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20064**](../Model/InlineResponse20064.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantList**
> \Api2Cart\Client\Model\InlineResponse20087 productVariantList($start, $count, $params, $exclude, $created_from, $created_to, $modified_from, $modified_to, $category_id, $product_id, $store_id)



Get a list of variants. This method is deprecated, and its development is stopped. Please use \"product.child_item.list\" instead.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$params = "id,name,description,price"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$category_id = "category_id_example"; // string | Retrieves products’ variants specified by category id
$product_id = "product_id_example"; // string | Retrieves products' variants specified by product id
$store_id = "store_id_example"; // string | Retrieves variants specified by store id

try {
    $result = $apiInstance->productVariantList($start, $count, $params, $exclude, $created_from, $created_to, $modified_from, $modified_to, $category_id, $product_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name,description,price]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **category_id** | **string**| Retrieves products’ variants specified by category id | [optional]
 **product_id** | **string**| Retrieves products&#39; variants specified by product id | [optional]
 **store_id** | **string**| Retrieves variants specified by store id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20087**](../Model/InlineResponse20087.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantPriceAdd**
> \Api2Cart\Client\Model\InlineResponse20021 productVariantPriceAdd($body)



Add some prices to the product variant.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductVariantPriceAdd(); // \Api2Cart\Client\Model\ProductVariantPriceAdd | 

try {
    $result = $apiInstance->productVariantPriceAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantPriceAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductVariantPriceAdd**](../Model/ProductVariantPriceAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20021**](../Model/InlineResponse20021.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantPriceDelete**
> \Api2Cart\Client\Model\InlineResponse2009 productVariantPriceDelete($id, $product_id, $group_prices, $store_id)



Delete some prices of the product variant.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Defines the variant where the price has to be deleted
$product_id = "product_id_example"; // string | Product id
$group_prices = "group_prices_example"; // string | Defines variants's group prices
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->productVariantPriceDelete($id, $product_id, $group_prices, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantPriceDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Defines the variant where the price has to be deleted |
 **product_id** | **string**| Product id |
 **group_prices** | **string**| Defines variants&#39;s group prices |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantPriceUpdate**
> \Api2Cart\Client\Model\InlineResponse2004 productVariantPriceUpdate($body)



Update some prices of the product variant.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductVariantPriceUpdate(); // \Api2Cart\Client\Model\ProductVariantPriceUpdate | 

try {
    $result = $apiInstance->productVariantPriceUpdate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantPriceUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductVariantPriceUpdate**](../Model/ProductVariantPriceUpdate.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantUpdate**
> \Api2Cart\Client\Model\InlineResponse2004 productVariantUpdate($body)



Update variant.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductVariantUpdate(); // \Api2Cart\Client\Model\ProductVariantUpdate | 

try {
    $result = $apiInstance->productVariantUpdate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductVariantUpdate**](../Model/ProductVariantUpdate.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **productVariantUpdateBatch**
> \Api2Cart\Client\Model\InlineResponse20042 productVariantUpdateBatch($body)



Update products variants on the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ProductApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\ProductVariantUpdateBatch(); // \Api2Cart\Client\Model\ProductVariantUpdateBatch | 

try {
    $result = $apiInstance->productVariantUpdateBatch($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductApi->productVariantUpdateBatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\ProductVariantUpdateBatch**](../Model/ProductVariantUpdateBatch.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20042**](../Model/InlineResponse20042.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

