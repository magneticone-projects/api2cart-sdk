# Api2Cart\Client\AccountApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountCartAdd**](AccountApi.md#accountCartAdd) | **POST** /account.cart.add.json | 
[**accountCartList**](AccountApi.md#accountCartList) | **GET** /account.cart.list.json | 
[**accountConfigUpdate**](AccountApi.md#accountConfigUpdate) | **PUT** /account.config.update.json | 
[**accountFailedWebhooks**](AccountApi.md#accountFailedWebhooks) | **GET** /account.failed_webhooks.json | 
[**accountSupportedPlatforms**](AccountApi.md#accountSupportedPlatforms) | **GET** /account.supported_platforms.json | 


# **accountCartAdd**
> \Api2Cart\Client\Model\InlineResponse2003 accountCartAdd($body)



Use this method to automate the process of connecting stores to API2Cart. The list of parameters will vary depending on the platform. To get a list of parameters that are specific to a particular shopping platform, you need to execute the account.supported_platforms.json method.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\AccountCartAdd(); // \Api2Cart\Client\Model\AccountCartAdd | 

try {
    $result = $apiInstance->accountCartAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountCartAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\AccountCartAdd**](../Model/AccountCartAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountCartList**
> \Api2Cart\Client\Model\InlineResponse2002 accountCartList($params, $exclude, $request_from_date, $request_to_date, $store_url, $store_key)



This method lets you get a list of online stores connected to your API2Cart account. You can get the number of API requests to each store if you specify a period using parameters (request_from_date, request_to_date). The total_calls field is displayed only if there are parameters (request_from_date, request_to_date).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$params = "force_all"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$request_from_date = "request_from_date_example"; // string | Retrieve entities from their creation date
$request_to_date = "request_to_date_example"; // string | Retrieve entities to their creation date
$store_url = "store_url_example"; // string | A web address of a store
$store_key = "store_key_example"; // string | Find store by store key

try {
    $result = $apiInstance->accountCartList($params, $exclude, $request_from_date, $request_to_date, $store_url, $store_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountCartList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to force_all]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **request_from_date** | **string**| Retrieve entities from their creation date | [optional]
 **request_to_date** | **string**| Retrieve entities to their creation date | [optional]
 **store_url** | **string**| A web address of a store | [optional]
 **store_key** | **string**| Find store by store key | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountConfigUpdate**
> \Api2Cart\Client\Model\InlineResponse2004 accountConfigUpdate($replace_parameters, $new_store_url, $new_store_key, $bridge_url, $store_root, $db_tables_prefix, $user_agent, $_3dcart_private_key, $_3dcart_access_token, $_3dcartapi_api_key, $amazon_sp_client_id, $amazon_sp_client_secret, $amazon_sp_refresh_token, $amazon_sp_aws_region, $amazon_sp_api_environment, $amazon_seller_id, $aspdotnetstorefront_api_user, $aspdotnetstorefront_api_pass, $bigcommerceapi_admin_account, $bigcommerceapi_api_path, $bigcommerceapi_api_key, $bigcommerceapi_client_id, $bigcommerceapi_access_token, $bigcommerceapi_context, $bol_api_key, $bol_api_secret, $bol_retailer_id, $demandware_client_id, $demandware_api_password, $demandware_user_name, $demandware_user_password, $ebay_client_id, $ebay_client_secret, $ebay_runame, $ebay_access_token, $ebay_refresh_token, $ebay_environment, $ebay_site_id, $ecwid_acess_token, $ecwid_store_id, $lazada_app_id, $lazada_app_secret, $lazada_refresh_token, $lazada_region, $etsy_keystring, $etsy_shared_secret, $etsy_access_token, $etsy_token_secret, $etsy_client_id, $etsy_refresh_token, $facebook_app_id, $facebook_app_secret, $facebook_access_token, $facebook_business_id, $neto_api_key, $neto_api_username, $shopline_access_token, $shopline_app_key, $shopline_app_secret, $shopify_access_token, $shopify_api_key, $shopify_api_password, $shopify_shared_secret, $shoplazza_access_token, $shoplazza_shared_secret, $miva_access_token, $miva_signature, $shopware_access_key, $shopware_api_key, $shopware_api_secret, $volusion_login, $volusion_password, $walmart_client_id, $walmart_client_secret, $walmart_environment, $walmart_channel_type, $walmart_region, $square_client_id, $square_client_secret, $square_refresh_token, $squarespace_api_key, $squarespace_client_id, $squarespace_client_secret, $squarespace_access_token, $squarespace_refresh_token, $hybris_client_id, $hybris_client_secret, $hybris_username, $hybris_password, $hybris_websites, $lightspeed_api_key, $lightspeed_api_secret, $commercehq_api_key, $commercehq_api_password, $wc_consumer_key, $wc_consumer_secret, $magento_consumer_key, $magento_consumer_secret, $magento_access_token, $magento_token_secret, $prestashop_webservice_key, $wix_app_id, $wix_app_secret_key, $wix_refresh_token, $mercado_libre_app_id, $mercado_libre_app_secret_key, $mercado_libre_refresh_token, $zid_client_id, $zid_client_secret, $zid_access_token, $zid_authorization, $zid_refresh_token, $flipkart_client_id, $flipkart_client_secret, $allegro_client_id, $allegro_client_secret, $allegro_access_token, $allegro_refresh_token, $allegro_environment, $zoho_client_id, $zoho_client_secret, $zoho_refresh_token, $zoho_region, $tiendanube_user_id, $tiendanube_access_token, $tiendanube_client_secret, $otto_client_id, $otto_client_secret, $otto_app_id, $otto_refresh_token, $otto_environment, $otto_access_token, $tiktokshop_app_key, $tiktokshop_app_secret, $tiktokshop_refresh_token, $tiktokshop_access_token, $salla_client_id, $salla_client_secret, $salla_refresh_token, $salla_access_token)



Use this method to automate the change of credentials used to connect online stores. The list of supported parameters differs depending on the platform.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$replace_parameters = true; // bool | Identifies if there is a necessity to replace parameters
$new_store_url = "new_store_url_example"; // string | The web address of the store you want to update to connect to API2Cart
$new_store_key = "new_store_key_example"; // string | Update store key
$bridge_url = "bridge_url_example"; // string | This parameter allows to set up store with custom bridge url (also you must use store_root parameter if a bridge folder is not in the root folder of the store)
$store_root = "store_root_example"; // string | Absolute path to the store root directory (used with \"bridge_url\" parameter)
$db_tables_prefix = "db_tables_prefix_example"; // string | DB tables prefix
$user_agent = "user_agent_example"; // string | This parameter allows you to set your custom user agent, which will be used in requests to the store. Please use it cautiously, as the store's firewall may block specific values.
$_3dcart_private_key = "_3dcart_private_key_example"; // string | 3DCart Private Key
$_3dcart_access_token = "_3dcart_access_token_example"; // string | 3DCart Token
$_3dcartapi_api_key = "_3dcartapi_api_key_example"; // string | 3DCart API Key
$amazon_sp_client_id = "amazon_sp_client_id_example"; // string | Amazon SP API app client id
$amazon_sp_client_secret = "amazon_sp_client_secret_example"; // string | Amazon SP API app client secret
$amazon_sp_refresh_token = "amazon_sp_refresh_token_example"; // string | Amazon SP API OAuth refresh token
$amazon_sp_aws_region = "amazon_sp_aws_region_example"; // string | Amazon AWS Region
$amazon_sp_api_environment = "production"; // string | Amazon SP API environment
$amazon_seller_id = "amazon_seller_id_example"; // string | Amazon Seller ID (Merchant token)
$aspdotnetstorefront_api_user = "aspdotnetstorefront_api_user_example"; // string | It's a AspDotNetStorefront account for which API is available
$aspdotnetstorefront_api_pass = "aspdotnetstorefront_api_pass_example"; // string | AspDotNetStorefront API Password
$bigcommerceapi_admin_account = "bigcommerceapi_admin_account_example"; // string | It's a BigCommerce account for which API is enabled
$bigcommerceapi_api_path = "bigcommerceapi_api_path_example"; // string | BigCommerce API URL
$bigcommerceapi_api_key = "bigcommerceapi_api_key_example"; // string | Bigcommerce API Key
$bigcommerceapi_client_id = "bigcommerceapi_client_id_example"; // string | Client ID of the requesting app
$bigcommerceapi_access_token = "bigcommerceapi_access_token_example"; // string | Access token authorizing the app to access resources on behalf of a user
$bigcommerceapi_context = "bigcommerceapi_context_example"; // string | API Path section unique to the store
$bol_api_key = "bol_api_key_example"; // string | Bol API Key
$bol_api_secret = "bol_api_secret_example"; // string | Bol API Secret
$bol_retailer_id = 56; // int | Bol Retailer ID
$demandware_client_id = "demandware_client_id_example"; // string | Demandware client id
$demandware_api_password = "demandware_api_password_example"; // string | Demandware api password
$demandware_user_name = "demandware_user_name_example"; // string | Demandware user name
$demandware_user_password = "demandware_user_password_example"; // string | Demandware user password
$ebay_client_id = "ebay_client_id_example"; // string | Application ID (AppID).
$ebay_client_secret = "ebay_client_secret_example"; // string | Shared Secret from eBay application
$ebay_runame = "ebay_runame_example"; // string | The RuName value that eBay assigns to your application.
$ebay_access_token = "ebay_access_token_example"; // string | Used to authenticate API requests.
$ebay_refresh_token = "ebay_refresh_token_example"; // string | Used to renew the access token.
$ebay_environment = "ebay_environment_example"; // string | eBay environment
$ebay_site_id = 0; // int | eBay global ID
$ecwid_acess_token = "ecwid_acess_token_example"; // string | Access token authorizing the app to access resources on behalf of a user
$ecwid_store_id = "ecwid_store_id_example"; // string | Store Id
$lazada_app_id = "lazada_app_id_example"; // string | Lazada App ID
$lazada_app_secret = "lazada_app_secret_example"; // string | Lazada App Secret
$lazada_refresh_token = "lazada_refresh_token_example"; // string | Lazada Refresh Token
$lazada_region = "lazada_region_example"; // string | Lazada API endpoint Region
$etsy_keystring = "etsy_keystring_example"; // string | Etsy keystring
$etsy_shared_secret = "etsy_shared_secret_example"; // string | Etsy shared secret
$etsy_access_token = "etsy_access_token_example"; // string | Access token authorizing the app to access resources on behalf of a user
$etsy_token_secret = "etsy_token_secret_example"; // string | Secret token authorizing the app to access resources on behalf of a user
$etsy_client_id = "etsy_client_id_example"; // string | Etsy Client Id
$etsy_refresh_token = "etsy_refresh_token_example"; // string | Etsy Refresh token
$facebook_app_id = "facebook_app_id_example"; // string | Facebook App ID
$facebook_app_secret = "facebook_app_secret_example"; // string | Facebook App Secret
$facebook_access_token = "facebook_access_token_example"; // string | Facebook Access Token
$facebook_business_id = "facebook_business_id_example"; // string | Facebook Business ID
$neto_api_key = "neto_api_key_example"; // string | Neto API Key
$neto_api_username = "neto_api_username_example"; // string | Neto User Name
$shopline_access_token = "shopline_access_token_example"; // string | Shopline APP Key
$shopline_app_key = "shopline_app_key_example"; // string | Shopline APP Key
$shopline_app_secret = "shopline_app_secret_example"; // string | Shopline App Secret
$shopify_access_token = "shopify_access_token_example"; // string | Access token authorizing the app to access resources on behalf of a user
$shopify_api_key = "shopify_api_key_example"; // string | Shopify API Key
$shopify_api_password = "shopify_api_password_example"; // string | Shopify API Password
$shopify_shared_secret = "shopify_shared_secret_example"; // string | Shared secret
$shoplazza_access_token = "shoplazza_access_token_example"; // string | Access token authorizing the app to access resources on behalf of a user
$shoplazza_shared_secret = "shoplazza_shared_secret_example"; // string | Shared secret
$miva_access_token = "miva_access_token_example"; // string | Miva access token
$miva_signature = "miva_signature_example"; // string | Miva signature
$shopware_access_key = "shopware_access_key_example"; // string | Shopware access key
$shopware_api_key = "shopware_api_key_example"; // string | Shopware api key
$shopware_api_secret = "shopware_api_secret_example"; // string | Shopware client secret access key
$volusion_login = "volusion_login_example"; // string | It's a Volusion account for which API is enabled
$volusion_password = "volusion_password_example"; // string | Volusion API Password
$walmart_client_id = "walmart_client_id_example"; // string | Walmart client ID. For the region 'ca' use Consumer ID
$walmart_client_secret = "walmart_client_secret_example"; // string | Walmart client secret. For the region 'ca' use Private Key
$walmart_environment = "production"; // string | Walmart environment
$walmart_channel_type = "walmart_channel_type_example"; // string | Walmart WM_CONSUMER.CHANNEL.TYPE header
$walmart_region = "us"; // string | Walmart region
$square_client_id = "square_client_id_example"; // string | Square (Weebly) Client ID
$square_client_secret = "square_client_secret_example"; // string | Square (Weebly) Client Secret
$square_refresh_token = "square_refresh_token_example"; // string | Square (Weebly) Refresh Token
$squarespace_api_key = "squarespace_api_key_example"; // string | Squarespace API Key
$squarespace_client_id = "squarespace_client_id_example"; // string | Squarespace Connector Client ID
$squarespace_client_secret = "squarespace_client_secret_example"; // string | Squarespace Connector Client Secret
$squarespace_access_token = "squarespace_access_token_example"; // string | Squarespace access token
$squarespace_refresh_token = "squarespace_refresh_token_example"; // string | Squarespace refresh token
$hybris_client_id = "hybris_client_id_example"; // string | Omni Commerce Connector Client ID
$hybris_client_secret = "hybris_client_secret_example"; // string | Omni Commerce Connector Client Secret
$hybris_username = "hybris_username_example"; // string | User Name
$hybris_password = "hybris_password_example"; // string | User password
$hybris_websites = array("hybris_websites_example"); // string[] | Websites to stores mapping data
$lightspeed_api_key = "lightspeed_api_key_example"; // string | LightSpeed api key
$lightspeed_api_secret = "lightspeed_api_secret_example"; // string | LightSpeed api secret
$commercehq_api_key = "commercehq_api_key_example"; // string | CommerceHQ api key
$commercehq_api_password = "commercehq_api_password_example"; // string | CommerceHQ api password
$wc_consumer_key = "wc_consumer_key_example"; // string | Woocommerce consumer key
$wc_consumer_secret = "wc_consumer_secret_example"; // string | Woocommerce consumer secret
$magento_consumer_key = "magento_consumer_key_example"; // string | Magento Consumer Key
$magento_consumer_secret = "magento_consumer_secret_example"; // string | Magento Consumer Secret
$magento_access_token = "magento_access_token_example"; // string | Magento Access Token
$magento_token_secret = "magento_token_secret_example"; // string | Magento Token Secret
$prestashop_webservice_key = "prestashop_webservice_key_example"; // string | Prestashop webservice key
$wix_app_id = "wix_app_id_example"; // string | Wix App ID
$wix_app_secret_key = "wix_app_secret_key_example"; // string | Wix App Secret Key
$wix_refresh_token = "wix_refresh_token_example"; // string | Wix refresh token
$mercado_libre_app_id = "mercado_libre_app_id_example"; // string | Mercado Libre App ID
$mercado_libre_app_secret_key = "mercado_libre_app_secret_key_example"; // string | Mercado Libre App Secret Key
$mercado_libre_refresh_token = "mercado_libre_refresh_token_example"; // string | Mercado Libre Refresh Token
$zid_client_id = 56; // int | Zid Client ID
$zid_client_secret = "zid_client_secret_example"; // string | Zid Client Secret
$zid_access_token = "zid_access_token_example"; // string | Zid Access Token
$zid_authorization = "zid_authorization_example"; // string | Zid Authorization
$zid_refresh_token = "zid_refresh_token_example"; // string | Zid refresh token
$flipkart_client_id = "flipkart_client_id_example"; // string | Flipkart Client ID
$flipkart_client_secret = "flipkart_client_secret_example"; // string | Flipkart Client Secret
$allegro_client_id = "allegro_client_id_example"; // string | Allegro Client ID
$allegro_client_secret = "allegro_client_secret_example"; // string | Allegro Client Secret
$allegro_access_token = "allegro_access_token_example"; // string | Allegro Access Token
$allegro_refresh_token = "allegro_refresh_token_example"; // string | Allegro Refresh Token
$allegro_environment = "production"; // string | Allegro Environment
$zoho_client_id = "zoho_client_id_example"; // string | Zoho Client ID
$zoho_client_secret = "zoho_client_secret_example"; // string | Zoho Client Secret
$zoho_refresh_token = "zoho_refresh_token_example"; // string | Zoho Refresh Token
$zoho_region = "zoho_region_example"; // string | Zoho API endpoint Region
$tiendanube_user_id = 56; // int | Tiendanube User ID
$tiendanube_access_token = "tiendanube_access_token_example"; // string | Tiendanube Access Token
$tiendanube_client_secret = "tiendanube_client_secret_example"; // string | Tiendanube Client Secret
$otto_client_id = "otto_client_id_example"; // string | Otto Client ID
$otto_client_secret = "otto_client_secret_example"; // string | Otto Client Secret
$otto_app_id = "otto_app_id_example"; // string | Otto App ID
$otto_refresh_token = "otto_refresh_token_example"; // string | Otto Refresh Token
$otto_environment = "otto_environment_example"; // string | Otto Environment
$otto_access_token = "otto_access_token_example"; // string | Otto Access Token
$tiktokshop_app_key = "tiktokshop_app_key_example"; // string | TikTok Shop App Key
$tiktokshop_app_secret = "tiktokshop_app_secret_example"; // string | TikTok Shop App Secret
$tiktokshop_refresh_token = "tiktokshop_refresh_token_example"; // string | TikTok Shop Refresh Token
$tiktokshop_access_token = "tiktokshop_access_token_example"; // string | TikTok Shop Access Token
$salla_client_id = "salla_client_id_example"; // string | Salla Client ID
$salla_client_secret = "salla_client_secret_example"; // string | Salla Client Secret
$salla_refresh_token = "salla_refresh_token_example"; // string | Salla Refresh Token
$salla_access_token = "salla_access_token_example"; // string | Salla Access Token

try {
    $result = $apiInstance->accountConfigUpdate($replace_parameters, $new_store_url, $new_store_key, $bridge_url, $store_root, $db_tables_prefix, $user_agent, $_3dcart_private_key, $_3dcart_access_token, $_3dcartapi_api_key, $amazon_sp_client_id, $amazon_sp_client_secret, $amazon_sp_refresh_token, $amazon_sp_aws_region, $amazon_sp_api_environment, $amazon_seller_id, $aspdotnetstorefront_api_user, $aspdotnetstorefront_api_pass, $bigcommerceapi_admin_account, $bigcommerceapi_api_path, $bigcommerceapi_api_key, $bigcommerceapi_client_id, $bigcommerceapi_access_token, $bigcommerceapi_context, $bol_api_key, $bol_api_secret, $bol_retailer_id, $demandware_client_id, $demandware_api_password, $demandware_user_name, $demandware_user_password, $ebay_client_id, $ebay_client_secret, $ebay_runame, $ebay_access_token, $ebay_refresh_token, $ebay_environment, $ebay_site_id, $ecwid_acess_token, $ecwid_store_id, $lazada_app_id, $lazada_app_secret, $lazada_refresh_token, $lazada_region, $etsy_keystring, $etsy_shared_secret, $etsy_access_token, $etsy_token_secret, $etsy_client_id, $etsy_refresh_token, $facebook_app_id, $facebook_app_secret, $facebook_access_token, $facebook_business_id, $neto_api_key, $neto_api_username, $shopline_access_token, $shopline_app_key, $shopline_app_secret, $shopify_access_token, $shopify_api_key, $shopify_api_password, $shopify_shared_secret, $shoplazza_access_token, $shoplazza_shared_secret, $miva_access_token, $miva_signature, $shopware_access_key, $shopware_api_key, $shopware_api_secret, $volusion_login, $volusion_password, $walmart_client_id, $walmart_client_secret, $walmart_environment, $walmart_channel_type, $walmart_region, $square_client_id, $square_client_secret, $square_refresh_token, $squarespace_api_key, $squarespace_client_id, $squarespace_client_secret, $squarespace_access_token, $squarespace_refresh_token, $hybris_client_id, $hybris_client_secret, $hybris_username, $hybris_password, $hybris_websites, $lightspeed_api_key, $lightspeed_api_secret, $commercehq_api_key, $commercehq_api_password, $wc_consumer_key, $wc_consumer_secret, $magento_consumer_key, $magento_consumer_secret, $magento_access_token, $magento_token_secret, $prestashop_webservice_key, $wix_app_id, $wix_app_secret_key, $wix_refresh_token, $mercado_libre_app_id, $mercado_libre_app_secret_key, $mercado_libre_refresh_token, $zid_client_id, $zid_client_secret, $zid_access_token, $zid_authorization, $zid_refresh_token, $flipkart_client_id, $flipkart_client_secret, $allegro_client_id, $allegro_client_secret, $allegro_access_token, $allegro_refresh_token, $allegro_environment, $zoho_client_id, $zoho_client_secret, $zoho_refresh_token, $zoho_region, $tiendanube_user_id, $tiendanube_access_token, $tiendanube_client_secret, $otto_client_id, $otto_client_secret, $otto_app_id, $otto_refresh_token, $otto_environment, $otto_access_token, $tiktokshop_app_key, $tiktokshop_app_secret, $tiktokshop_refresh_token, $tiktokshop_access_token, $salla_client_id, $salla_client_secret, $salla_refresh_token, $salla_access_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountConfigUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **replace_parameters** | **bool**| Identifies if there is a necessity to replace parameters | [optional]
 **new_store_url** | **string**| The web address of the store you want to update to connect to API2Cart | [optional]
 **new_store_key** | **string**| Update store key | [optional]
 **bridge_url** | **string**| This parameter allows to set up store with custom bridge url (also you must use store_root parameter if a bridge folder is not in the root folder of the store) | [optional]
 **store_root** | **string**| Absolute path to the store root directory (used with \&quot;bridge_url\&quot; parameter) | [optional]
 **db_tables_prefix** | **string**| DB tables prefix | [optional]
 **user_agent** | **string**| This parameter allows you to set your custom user agent, which will be used in requests to the store. Please use it cautiously, as the store&#39;s firewall may block specific values. | [optional]
 **_3dcart_private_key** | **string**| 3DCart Private Key | [optional]
 **_3dcart_access_token** | **string**| 3DCart Token | [optional]
 **_3dcartapi_api_key** | **string**| 3DCart API Key | [optional]
 **amazon_sp_client_id** | **string**| Amazon SP API app client id | [optional]
 **amazon_sp_client_secret** | **string**| Amazon SP API app client secret | [optional]
 **amazon_sp_refresh_token** | **string**| Amazon SP API OAuth refresh token | [optional]
 **amazon_sp_aws_region** | **string**| Amazon AWS Region | [optional]
 **amazon_sp_api_environment** | **string**| Amazon SP API environment | [optional] [default to production]
 **amazon_seller_id** | **string**| Amazon Seller ID (Merchant token) | [optional]
 **aspdotnetstorefront_api_user** | **string**| It&#39;s a AspDotNetStorefront account for which API is available | [optional]
 **aspdotnetstorefront_api_pass** | **string**| AspDotNetStorefront API Password | [optional]
 **bigcommerceapi_admin_account** | **string**| It&#39;s a BigCommerce account for which API is enabled | [optional]
 **bigcommerceapi_api_path** | **string**| BigCommerce API URL | [optional]
 **bigcommerceapi_api_key** | **string**| Bigcommerce API Key | [optional]
 **bigcommerceapi_client_id** | **string**| Client ID of the requesting app | [optional]
 **bigcommerceapi_access_token** | **string**| Access token authorizing the app to access resources on behalf of a user | [optional]
 **bigcommerceapi_context** | **string**| API Path section unique to the store | [optional]
 **bol_api_key** | **string**| Bol API Key | [optional]
 **bol_api_secret** | **string**| Bol API Secret | [optional]
 **bol_retailer_id** | **int**| Bol Retailer ID | [optional]
 **demandware_client_id** | **string**| Demandware client id | [optional]
 **demandware_api_password** | **string**| Demandware api password | [optional]
 **demandware_user_name** | **string**| Demandware user name | [optional]
 **demandware_user_password** | **string**| Demandware user password | [optional]
 **ebay_client_id** | **string**| Application ID (AppID). | [optional]
 **ebay_client_secret** | **string**| Shared Secret from eBay application | [optional]
 **ebay_runame** | **string**| The RuName value that eBay assigns to your application. | [optional]
 **ebay_access_token** | **string**| Used to authenticate API requests. | [optional]
 **ebay_refresh_token** | **string**| Used to renew the access token. | [optional]
 **ebay_environment** | **string**| eBay environment | [optional]
 **ebay_site_id** | **int**| eBay global ID | [optional] [default to 0]
 **ecwid_acess_token** | **string**| Access token authorizing the app to access resources on behalf of a user | [optional]
 **ecwid_store_id** | **string**| Store Id | [optional]
 **lazada_app_id** | **string**| Lazada App ID | [optional]
 **lazada_app_secret** | **string**| Lazada App Secret | [optional]
 **lazada_refresh_token** | **string**| Lazada Refresh Token | [optional]
 **lazada_region** | **string**| Lazada API endpoint Region | [optional]
 **etsy_keystring** | **string**| Etsy keystring | [optional]
 **etsy_shared_secret** | **string**| Etsy shared secret | [optional]
 **etsy_access_token** | **string**| Access token authorizing the app to access resources on behalf of a user | [optional]
 **etsy_token_secret** | **string**| Secret token authorizing the app to access resources on behalf of a user | [optional]
 **etsy_client_id** | **string**| Etsy Client Id | [optional]
 **etsy_refresh_token** | **string**| Etsy Refresh token | [optional]
 **facebook_app_id** | **string**| Facebook App ID | [optional]
 **facebook_app_secret** | **string**| Facebook App Secret | [optional]
 **facebook_access_token** | **string**| Facebook Access Token | [optional]
 **facebook_business_id** | **string**| Facebook Business ID | [optional]
 **neto_api_key** | **string**| Neto API Key | [optional]
 **neto_api_username** | **string**| Neto User Name | [optional]
 **shopline_access_token** | **string**| Shopline APP Key | [optional]
 **shopline_app_key** | **string**| Shopline APP Key | [optional]
 **shopline_app_secret** | **string**| Shopline App Secret | [optional]
 **shopify_access_token** | **string**| Access token authorizing the app to access resources on behalf of a user | [optional]
 **shopify_api_key** | **string**| Shopify API Key | [optional]
 **shopify_api_password** | **string**| Shopify API Password | [optional]
 **shopify_shared_secret** | **string**| Shared secret | [optional]
 **shoplazza_access_token** | **string**| Access token authorizing the app to access resources on behalf of a user | [optional]
 **shoplazza_shared_secret** | **string**| Shared secret | [optional]
 **miva_access_token** | **string**| Miva access token | [optional]
 **miva_signature** | **string**| Miva signature | [optional]
 **shopware_access_key** | **string**| Shopware access key | [optional]
 **shopware_api_key** | **string**| Shopware api key | [optional]
 **shopware_api_secret** | **string**| Shopware client secret access key | [optional]
 **volusion_login** | **string**| It&#39;s a Volusion account for which API is enabled | [optional]
 **volusion_password** | **string**| Volusion API Password | [optional]
 **walmart_client_id** | **string**| Walmart client ID. For the region &#39;ca&#39; use Consumer ID | [optional]
 **walmart_client_secret** | **string**| Walmart client secret. For the region &#39;ca&#39; use Private Key | [optional]
 **walmart_environment** | **string**| Walmart environment | [optional] [default to production]
 **walmart_channel_type** | **string**| Walmart WM_CONSUMER.CHANNEL.TYPE header | [optional]
 **walmart_region** | **string**| Walmart region | [optional] [default to us]
 **square_client_id** | **string**| Square (Weebly) Client ID | [optional]
 **square_client_secret** | **string**| Square (Weebly) Client Secret | [optional]
 **square_refresh_token** | **string**| Square (Weebly) Refresh Token | [optional]
 **squarespace_api_key** | **string**| Squarespace API Key | [optional]
 **squarespace_client_id** | **string**| Squarespace Connector Client ID | [optional]
 **squarespace_client_secret** | **string**| Squarespace Connector Client Secret | [optional]
 **squarespace_access_token** | **string**| Squarespace access token | [optional]
 **squarespace_refresh_token** | **string**| Squarespace refresh token | [optional]
 **hybris_client_id** | **string**| Omni Commerce Connector Client ID | [optional]
 **hybris_client_secret** | **string**| Omni Commerce Connector Client Secret | [optional]
 **hybris_username** | **string**| User Name | [optional]
 **hybris_password** | **string**| User password | [optional]
 **hybris_websites** | [**string[]**](../Model/string.md)| Websites to stores mapping data | [optional]
 **lightspeed_api_key** | **string**| LightSpeed api key | [optional]
 **lightspeed_api_secret** | **string**| LightSpeed api secret | [optional]
 **commercehq_api_key** | **string**| CommerceHQ api key | [optional]
 **commercehq_api_password** | **string**| CommerceHQ api password | [optional]
 **wc_consumer_key** | **string**| Woocommerce consumer key | [optional]
 **wc_consumer_secret** | **string**| Woocommerce consumer secret | [optional]
 **magento_consumer_key** | **string**| Magento Consumer Key | [optional]
 **magento_consumer_secret** | **string**| Magento Consumer Secret | [optional]
 **magento_access_token** | **string**| Magento Access Token | [optional]
 **magento_token_secret** | **string**| Magento Token Secret | [optional]
 **prestashop_webservice_key** | **string**| Prestashop webservice key | [optional]
 **wix_app_id** | **string**| Wix App ID | [optional]
 **wix_app_secret_key** | **string**| Wix App Secret Key | [optional]
 **wix_refresh_token** | **string**| Wix refresh token | [optional]
 **mercado_libre_app_id** | **string**| Mercado Libre App ID | [optional]
 **mercado_libre_app_secret_key** | **string**| Mercado Libre App Secret Key | [optional]
 **mercado_libre_refresh_token** | **string**| Mercado Libre Refresh Token | [optional]
 **zid_client_id** | **int**| Zid Client ID | [optional]
 **zid_client_secret** | **string**| Zid Client Secret | [optional]
 **zid_access_token** | **string**| Zid Access Token | [optional]
 **zid_authorization** | **string**| Zid Authorization | [optional]
 **zid_refresh_token** | **string**| Zid refresh token | [optional]
 **flipkart_client_id** | **string**| Flipkart Client ID | [optional]
 **flipkart_client_secret** | **string**| Flipkart Client Secret | [optional]
 **allegro_client_id** | **string**| Allegro Client ID | [optional]
 **allegro_client_secret** | **string**| Allegro Client Secret | [optional]
 **allegro_access_token** | **string**| Allegro Access Token | [optional]
 **allegro_refresh_token** | **string**| Allegro Refresh Token | [optional]
 **allegro_environment** | **string**| Allegro Environment | [optional] [default to production]
 **zoho_client_id** | **string**| Zoho Client ID | [optional]
 **zoho_client_secret** | **string**| Zoho Client Secret | [optional]
 **zoho_refresh_token** | **string**| Zoho Refresh Token | [optional]
 **zoho_region** | **string**| Zoho API endpoint Region | [optional]
 **tiendanube_user_id** | **int**| Tiendanube User ID | [optional]
 **tiendanube_access_token** | **string**| Tiendanube Access Token | [optional]
 **tiendanube_client_secret** | **string**| Tiendanube Client Secret | [optional]
 **otto_client_id** | **string**| Otto Client ID | [optional]
 **otto_client_secret** | **string**| Otto Client Secret | [optional]
 **otto_app_id** | **string**| Otto App ID | [optional]
 **otto_refresh_token** | **string**| Otto Refresh Token | [optional]
 **otto_environment** | **string**| Otto Environment | [optional]
 **otto_access_token** | **string**| Otto Access Token | [optional]
 **tiktokshop_app_key** | **string**| TikTok Shop App Key | [optional]
 **tiktokshop_app_secret** | **string**| TikTok Shop App Secret | [optional]
 **tiktokshop_refresh_token** | **string**| TikTok Shop Refresh Token | [optional]
 **tiktokshop_access_token** | **string**| TikTok Shop Access Token | [optional]
 **salla_client_id** | **string**| Salla Client ID | [optional]
 **salla_client_secret** | **string**| Salla Client Secret | [optional]
 **salla_refresh_token** | **string**| Salla Refresh Token | [optional]
 **salla_access_token** | **string**| Salla Access Token | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountFailedWebhooks**
> \Api2Cart\Client\Model\InlineResponse200 accountFailedWebhooks($count, $start, $ids)



If the callback of your service for some reason could not accept webhooks from API2Cart, then with the help of this method you can get a list of missed webhooks to perform synchronization again using entity_id. Please note that we keep such records for 24 hours.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$start = 0; // int | This parameter sets the number from which you want to get entities
$ids = "ids_example"; // string | List of сomma-separated webhook ids

try {
    $result = $apiInstance->accountFailedWebhooks($count, $start, $ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountFailedWebhooks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **ids** | **string**| List of сomma-separated webhook ids | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountSupportedPlatforms**
> \Api2Cart\Client\Model\InlineResponse2001 accountSupportedPlatforms()



Use this method to retrieve a list of supported platforms and the sets of parameters required for connecting to each of them. Note: some platforms may have multiple connection methods so that the response will contain multiple sets of parameters.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\AccountApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->accountSupportedPlatforms();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountSupportedPlatforms: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Api2Cart\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

