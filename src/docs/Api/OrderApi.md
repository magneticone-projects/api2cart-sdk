# Api2Cart\Client\OrderApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orderAbandonedList**](OrderApi.md#orderAbandonedList) | **GET** /order.abandoned.list.json | 
[**orderAdd**](OrderApi.md#orderAdd) | **POST** /order.add.json | 
[**orderCount**](OrderApi.md#orderCount) | **GET** /order.count.json | 
[**orderFinancialStatusList**](OrderApi.md#orderFinancialStatusList) | **GET** /order.financial_status.list.json | 
[**orderFind**](OrderApi.md#orderFind) | **GET** /order.find.json | 
[**orderFulfillmentStatusList**](OrderApi.md#orderFulfillmentStatusList) | **GET** /order.fulfillment_status.list.json | 
[**orderInfo**](OrderApi.md#orderInfo) | **GET** /order.info.json | 
[**orderList**](OrderApi.md#orderList) | **GET** /order.list.json | 
[**orderPreestimateShippingList**](OrderApi.md#orderPreestimateShippingList) | **POST** /order.preestimate_shipping.list.json | 
[**orderRefundAdd**](OrderApi.md#orderRefundAdd) | **POST** /order.refund.add.json | 
[**orderReturnAdd**](OrderApi.md#orderReturnAdd) | **POST** /order.return.add.json | 
[**orderReturnDelete**](OrderApi.md#orderReturnDelete) | **DELETE** /order.return.delete.json | 
[**orderReturnUpdate**](OrderApi.md#orderReturnUpdate) | **PUT** /order.return.update.json | 
[**orderShipmentAdd**](OrderApi.md#orderShipmentAdd) | **POST** /order.shipment.add.json | 
[**orderShipmentDelete**](OrderApi.md#orderShipmentDelete) | **DELETE** /order.shipment.delete.json | 
[**orderShipmentInfo**](OrderApi.md#orderShipmentInfo) | **GET** /order.shipment.info.json | 
[**orderShipmentList**](OrderApi.md#orderShipmentList) | **GET** /order.shipment.list.json | 
[**orderShipmentTrackingAdd**](OrderApi.md#orderShipmentTrackingAdd) | **POST** /order.shipment.tracking.add.json | 
[**orderShipmentUpdate**](OrderApi.md#orderShipmentUpdate) | **PUT** /order.shipment.update.json | 
[**orderStatusList**](OrderApi.md#orderStatusList) | **GET** /order.status.list.json | 
[**orderTransactionList**](OrderApi.md#orderTransactionList) | **GET** /order.transaction.list.json | 
[**orderUpdate**](OrderApi.md#orderUpdate) | **PUT** /order.update.json | 


# **orderAbandonedList**
> \Api2Cart\Client\Model\ModelResponseOrderAbandonedList orderAbandonedList($customer_id, $customer_email, $created_to, $created_from, $modified_to, $modified_from, $skip_empty_email, $store_id, $page_cursor, $count, $start, $params, $response_fields, $exclude)



Get list of orders that were left by customers before completing the order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Retrieves orders specified by customer id
$customer_email = "customer_email_example"; // string | Retrieves orders specified by customer email
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$skip_empty_email = false; // bool | Filter empty emails
$store_id = "store_id_example"; // string | Store Id
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$start = 0; // int | This parameter sets the number from which you want to get entities
$params = "customer,totals,items"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all

try {
    $result = $apiInstance->orderAbandonedList($customer_id, $customer_email, $created_to, $created_from, $modified_to, $modified_from, $skip_empty_email, $store_id, $page_cursor, $count, $start, $params, $response_fields, $exclude);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderAbandonedList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Retrieves orders specified by customer id | [optional]
 **customer_email** | **string**| Retrieves orders specified by customer email | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **skip_empty_email** | **bool**| Filter empty emails | [optional] [default to false]
 **store_id** | **string**| Store Id | [optional]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to customer,totals,items]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseOrderAbandonedList**](../Model/ModelResponseOrderAbandonedList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderAdd**
> \Api2Cart\Client\Model\InlineResponse20055 orderAdd($body)



Add a new order to the cart.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\OrderAdd(); // \Api2Cart\Client\Model\OrderAdd | 

try {
    $result = $apiInstance->orderAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\OrderAdd**](../Model/OrderAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20055**](../Model/InlineResponse20055.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderCount**
> \Api2Cart\Client\Model\InlineResponse20053 orderCount($customer_id, $customer_email, $order_status, $order_status_ids, $created_to, $created_from, $modified_to, $modified_from, $store_id, $ids, $order_ids, $ebay_order_status, $financial_status, $financial_status_ids, $fulfillment_channel, $fulfillment_status, $shipping_method, $delivery_method, $tags, $ship_node_type)



Count orders in store

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Counts orders quantity specified by customer id
$customer_email = "customer_email_example"; // string | Counts orders quantity specified by customer email
$order_status = "order_status_example"; // string | Counts orders quantity specified by order status
$order_status_ids = array("order_status_ids_example"); // string[] | Retrieves orders specified by order statuses
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$store_id = "store_id_example"; // string | Counts orders quantity specified by store id
$ids = "ids_example"; // string | Counts orders specified by ids
$order_ids = "order_ids_example"; // string | Counts orders specified by order ids
$ebay_order_status = "ebay_order_status_example"; // string | Counts orders quantity specified by order status
$financial_status = "financial_status_example"; // string | Counts orders quantity specified by financial status
$financial_status_ids = array("financial_status_ids_example"); // string[] | Retrieves orders count specified by financial status ids
$fulfillment_channel = "fulfillment_channel_example"; // string | Retrieves order with a fulfillment channel
$fulfillment_status = "fulfillment_status_example"; // string | Create order with fulfillment status
$shipping_method = "shipping_method_example"; // string | Retrieve entities according to shipping method
$delivery_method = "delivery_method_example"; // string | Retrieves order with delivery method
$tags = "tags_example"; // string | Order tags
$ship_node_type = "ship_node_type_example"; // string | Retrieves order with ship node type

try {
    $result = $apiInstance->orderCount($customer_id, $customer_email, $order_status, $order_status_ids, $created_to, $created_from, $modified_to, $modified_from, $store_id, $ids, $order_ids, $ebay_order_status, $financial_status, $financial_status_ids, $fulfillment_channel, $fulfillment_status, $shipping_method, $delivery_method, $tags, $ship_node_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Counts orders quantity specified by customer id | [optional]
 **customer_email** | **string**| Counts orders quantity specified by customer email | [optional]
 **order_status** | **string**| Counts orders quantity specified by order status | [optional]
 **order_status_ids** | [**string[]**](../Model/string.md)| Retrieves orders specified by order statuses | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **store_id** | **string**| Counts orders quantity specified by store id | [optional]
 **ids** | **string**| Counts orders specified by ids | [optional]
 **order_ids** | **string**| Counts orders specified by order ids | [optional]
 **ebay_order_status** | **string**| Counts orders quantity specified by order status | [optional]
 **financial_status** | **string**| Counts orders quantity specified by financial status | [optional]
 **financial_status_ids** | [**string[]**](../Model/string.md)| Retrieves orders count specified by financial status ids | [optional]
 **fulfillment_channel** | **string**| Retrieves order with a fulfillment channel | [optional]
 **fulfillment_status** | **string**| Create order with fulfillment status | [optional]
 **shipping_method** | **string**| Retrieve entities according to shipping method | [optional]
 **delivery_method** | **string**| Retrieves order with delivery method | [optional]
 **tags** | **string**| Order tags | [optional]
 **ship_node_type** | **string**| Retrieves order with ship node type | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20053**](../Model/InlineResponse20053.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderFinancialStatusList**
> \Api2Cart\Client\Model\InlineResponse20056 orderFinancialStatusList()



Retrieve list of financial statuses

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->orderFinancialStatusList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderFinancialStatusList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Api2Cart\Client\Model\InlineResponse20056**](../Model/InlineResponse20056.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderFind**
> \Api2Cart\Client\Model\InlineResponse20054 orderFind($customer_id, $customer_email, $order_status, $start, $count, $params, $exclude, $created_to, $created_from, $modified_to, $modified_from, $financial_status)



This method is deprecated and won't be supported in the future. Please use \"order.list\" instead.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Retrieves orders specified by customer id
$customer_email = "customer_email_example"; // string | Retrieves orders specified by customer email
$order_status = "order_status_example"; // string | Retrieves orders specified by order status
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$params = "order_id,customer,totals,address,items,bundles,status"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$financial_status = "financial_status_example"; // string | Retrieves orders specified by financial status

try {
    $result = $apiInstance->orderFind($customer_id, $customer_email, $order_status, $start, $count, $params, $exclude, $created_to, $created_from, $modified_to, $modified_from, $financial_status);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Retrieves orders specified by customer id | [optional]
 **customer_email** | **string**| Retrieves orders specified by customer email | [optional]
 **order_status** | **string**| Retrieves orders specified by order status | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to order_id,customer,totals,address,items,bundles,status]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **financial_status** | **string**| Retrieves orders specified by financial status | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20054**](../Model/InlineResponse20054.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderFulfillmentStatusList**
> \Api2Cart\Client\Model\InlineResponse20057 orderFulfillmentStatusList($action)



Retrieve list of fulfillment statuses

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$action = "action_example"; // string | Available statuses for the specified action.

try {
    $result = $apiInstance->orderFulfillmentStatusList($action);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderFulfillmentStatusList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **action** | **string**| Available statuses for the specified action. | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20057**](../Model/InlineResponse20057.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderInfo**
> \Api2Cart\Client\Model\InlineResponse20052 orderInfo($order_id, $id, $params, $response_fields, $exclude, $store_id, $enable_cache, $use_latest_api_version)



Info about a specific order by ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_id = "order_id_example"; // string | Retrieves order’s info specified by order id
$id = "id_example"; // string | Retrieves order info specified by id
$params = "order_id,customer,totals,address,items,bundles,status"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$store_id = "store_id_example"; // string | Defines store id where the order should be found
$enable_cache = false; // bool | If the value is 'true' and order exist in our cache, we will return order.info response from cache
$use_latest_api_version = false; // bool | Use the latest platform API version

try {
    $result = $apiInstance->orderInfo($order_id, $id, $params, $response_fields, $exclude, $store_id, $enable_cache, $use_latest_api_version);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **string**| Retrieves order’s info specified by order id | [optional]
 **id** | **string**| Retrieves order info specified by id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to order_id,customer,totals,address,items,bundles,status]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **store_id** | **string**| Defines store id where the order should be found | [optional]
 **enable_cache** | **bool**| If the value is &#39;true&#39; and order exist in our cache, we will return order.info response from cache | [optional] [default to false]
 **use_latest_api_version** | **bool**| Use the latest platform API version | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20052**](../Model/InlineResponse20052.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderList**
> \Api2Cart\Client\Model\ModelResponseOrderList orderList($customer_id, $customer_email, $phone, $order_status, $order_status_ids, $start, $count, $page_cursor, $sort_by, $sort_direction, $params, $response_fields, $exclude, $created_to, $created_from, $modified_to, $modified_from, $store_id, $ids, $order_ids, $ebay_order_status, $basket_id, $financial_status, $financial_status_ids, $fulfillment_status, $fulfillment_channel, $shipping_method, $skip_order_ids, $since_id, $is_deleted, $shipping_country_iso3, $enable_cache, $delivery_method, $tags, $ship_node_type, $currency_id, $return_status, $use_latest_api_version)



Get list of orders from store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Retrieves orders specified by customer id
$customer_email = "customer_email_example"; // string | Retrieves orders specified by customer email
$phone = "phone_example"; // string | Filter orders by customer's phone number
$order_status = "order_status_example"; // string | Retrieves orders specified by order status
$order_status_ids = array("order_status_ids_example"); // string[] | Retrieves orders specified by order statuses
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$page_cursor = "page_cursor_example"; // string | Used to retrieve orders via cursor-based pagination (it can't be used with any other filtering parameter)
$sort_by = "order_id"; // string | Set field to sort by
$sort_direction = "asc"; // string | Set sorting direction
$params = "order_id,customer,totals,address,items,bundles,status"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$store_id = "store_id_example"; // string | Store Id
$ids = "ids_example"; // string | Retrieves orders specified by ids
$order_ids = "order_ids_example"; // string | Retrieves orders specified by order ids
$ebay_order_status = "ebay_order_status_example"; // string | Retrieves orders specified by order status
$basket_id = "basket_id_example"; // string | Retrieves order’s info specified by basket id.
$financial_status = "financial_status_example"; // string | Retrieves orders specified by financial status
$financial_status_ids = array("financial_status_ids_example"); // string[] | Retrieves orders specified by financial status ids
$fulfillment_status = "fulfillment_status_example"; // string | Create order with fulfillment status
$fulfillment_channel = "fulfillment_channel_example"; // string | Retrieves order with a fulfillment channel
$shipping_method = "shipping_method_example"; // string | Retrieve entities according to shipping method
$skip_order_ids = "skip_order_ids_example"; // string | Skipped orders by ids
$since_id = "since_id_example"; // string | Retrieve entities starting from the specified id.
$is_deleted = true; // bool | Filter deleted orders
$shipping_country_iso3 = "shipping_country_iso3_example"; // string | Retrieve entities according to shipping country
$enable_cache = false; // bool | If the value is 'true', we will cache orders for a 15 minutes in order to increase speed and reduce requests throttling for some methods and shoping platforms (for example order.shipment.add)
$delivery_method = "delivery_method_example"; // string | Retrieves order with delivery method
$tags = "tags_example"; // string | Order tags
$ship_node_type = "ship_node_type_example"; // string | Retrieves order with ship node type
$currency_id = "currency_id_example"; // string | Currency Id
$return_status = "return_status_example"; // string | Retrieves orders specified by return status
$use_latest_api_version = false; // bool | Use the latest platform API version

try {
    $result = $apiInstance->orderList($customer_id, $customer_email, $phone, $order_status, $order_status_ids, $start, $count, $page_cursor, $sort_by, $sort_direction, $params, $response_fields, $exclude, $created_to, $created_from, $modified_to, $modified_from, $store_id, $ids, $order_ids, $ebay_order_status, $basket_id, $financial_status, $financial_status_ids, $fulfillment_status, $fulfillment_channel, $shipping_method, $skip_order_ids, $since_id, $is_deleted, $shipping_country_iso3, $enable_cache, $delivery_method, $tags, $ship_node_type, $currency_id, $return_status, $use_latest_api_version);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Retrieves orders specified by customer id | [optional]
 **customer_email** | **string**| Retrieves orders specified by customer email | [optional]
 **phone** | **string**| Filter orders by customer&#39;s phone number | [optional]
 **order_status** | **string**| Retrieves orders specified by order status | [optional]
 **order_status_ids** | [**string[]**](../Model/string.md)| Retrieves orders specified by order statuses | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **page_cursor** | **string**| Used to retrieve orders via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **sort_by** | **string**| Set field to sort by | [optional] [default to order_id]
 **sort_direction** | **string**| Set sorting direction | [optional] [default to asc]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to order_id,customer,totals,address,items,bundles,status]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **store_id** | **string**| Store Id | [optional]
 **ids** | **string**| Retrieves orders specified by ids | [optional]
 **order_ids** | **string**| Retrieves orders specified by order ids | [optional]
 **ebay_order_status** | **string**| Retrieves orders specified by order status | [optional]
 **basket_id** | **string**| Retrieves order’s info specified by basket id. | [optional]
 **financial_status** | **string**| Retrieves orders specified by financial status | [optional]
 **financial_status_ids** | [**string[]**](../Model/string.md)| Retrieves orders specified by financial status ids | [optional]
 **fulfillment_status** | **string**| Create order with fulfillment status | [optional]
 **fulfillment_channel** | **string**| Retrieves order with a fulfillment channel | [optional]
 **shipping_method** | **string**| Retrieve entities according to shipping method | [optional]
 **skip_order_ids** | **string**| Skipped orders by ids | [optional]
 **since_id** | **string**| Retrieve entities starting from the specified id. | [optional]
 **is_deleted** | **bool**| Filter deleted orders | [optional]
 **shipping_country_iso3** | **string**| Retrieve entities according to shipping country | [optional]
 **enable_cache** | **bool**| If the value is &#39;true&#39;, we will cache orders for a 15 minutes in order to increase speed and reduce requests throttling for some methods and shoping platforms (for example order.shipment.add) | [optional] [default to false]
 **delivery_method** | **string**| Retrieves order with delivery method | [optional]
 **tags** | **string**| Order tags | [optional]
 **ship_node_type** | **string**| Retrieves order with ship node type | [optional]
 **currency_id** | **string**| Currency Id | [optional]
 **return_status** | **string**| Retrieves orders specified by return status | [optional]
 **use_latest_api_version** | **bool**| Use the latest platform API version | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\ModelResponseOrderList**](../Model/ModelResponseOrderList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderPreestimateShippingList**
> \Api2Cart\Client\Model\ModelResponseOrderPreestimateShippingList orderPreestimateShippingList($body)



Retrieve list of order preestimated shipping methods

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\OrderPreestimateShippingList(); // \Api2Cart\Client\Model\OrderPreestimateShippingList | 

try {
    $result = $apiInstance->orderPreestimateShippingList($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderPreestimateShippingList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\OrderPreestimateShippingList**](../Model/OrderPreestimateShippingList.md)|  |

### Return type

[**\Api2Cart\Client\Model\ModelResponseOrderPreestimateShippingList**](../Model/ModelResponseOrderPreestimateShippingList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderRefundAdd**
> \Api2Cart\Client\Model\InlineResponse20058 orderRefundAdd($body)



Add a refund to the order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\OrderRefundAdd(); // \Api2Cart\Client\Model\OrderRefundAdd | 

try {
    $result = $apiInstance->orderRefundAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderRefundAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\OrderRefundAdd**](../Model/OrderRefundAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20058**](../Model/InlineResponse20058.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderReturnAdd**
> \Api2Cart\Client\Model\InlineResponse20059 orderReturnAdd($body)



Create new return request.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\OrderReturnAdd(); // \Api2Cart\Client\Model\OrderReturnAdd | 

try {
    $result = $apiInstance->orderReturnAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderReturnAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\OrderReturnAdd**](../Model/OrderReturnAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20059**](../Model/InlineResponse20059.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderReturnDelete**
> \Api2Cart\Client\Model\InlineResponse20014 orderReturnDelete($return_id, $order_id, $store_id)



Delete return.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$return_id = "return_id_example"; // string | Return ID
$order_id = "order_id_example"; // string | Defines the order id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->orderReturnDelete($return_id, $order_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderReturnDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **return_id** | **string**| Return ID |
 **order_id** | **string**| Defines the order id |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20014**](../Model/InlineResponse20014.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderReturnUpdate**
> \Api2Cart\Client\Model\InlineResponse2004 orderReturnUpdate($body)



Update order's shipment information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\OrderReturnUpdate(); // \Api2Cart\Client\Model\OrderReturnUpdate | 

try {
    $result = $apiInstance->orderReturnUpdate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderReturnUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\OrderReturnUpdate**](../Model/OrderReturnUpdate.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderShipmentAdd**
> \Api2Cart\Client\Model\InlineResponse20060 orderShipmentAdd($body)



Add a shipment to the order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\OrderShipmentAdd(); // \Api2Cart\Client\Model\OrderShipmentAdd | 

try {
    $result = $apiInstance->orderShipmentAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderShipmentAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\OrderShipmentAdd**](../Model/OrderShipmentAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20060**](../Model/InlineResponse20060.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderShipmentDelete**
> \Api2Cart\Client\Model\InlineResponse20061 orderShipmentDelete($shipment_id, $order_id, $store_id)



Delete order's shipment.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$shipment_id = "shipment_id_example"; // string | Shipment id indicates the number of delivery
$order_id = "order_id_example"; // string | Defines the order for which the shipment will be deleted
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->orderShipmentDelete($shipment_id, $order_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderShipmentDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipment_id** | **string**| Shipment id indicates the number of delivery |
 **order_id** | **string**| Defines the order for which the shipment will be deleted |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20061**](../Model/InlineResponse20061.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderShipmentInfo**
> \Api2Cart\Client\Model\ModelResponseOrderShipmentList orderShipmentInfo($id, $order_id, $start, $params, $response_fields, $exclude, $store_id)



Get information of shipment.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$order_id = "order_id_example"; // string | Defines the order id
$start = 0; // int | This parameter sets the number from which you want to get entities
$params = "id,order_id,items,tracking_numbers"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->orderShipmentInfo($id, $order_id, $start, $params, $response_fields, $exclude, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderShipmentInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **order_id** | **string**| Defines the order id |
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,order_id,items,tracking_numbers]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseOrderShipmentList**](../Model/ModelResponseOrderShipmentList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderShipmentList**
> \Api2Cart\Client\Model\ModelResponseOrderShipmentList orderShipmentList($order_id, $page_cursor, $start, $count, $params, $response_fields, $exclude, $created_from, $created_to, $modified_from, $modified_to, $store_id)



Get list of shipments by orders.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_id = "order_id_example"; // string | Retrieves shipments specified by order id
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$params = "id,order_id,items,tracking_numbers"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->orderShipmentList($order_id, $page_cursor, $start, $count, $params, $response_fields, $exclude, $created_from, $created_to, $modified_from, $modified_to, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderShipmentList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **string**| Retrieves shipments specified by order id |
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,order_id,items,tracking_numbers]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseOrderShipmentList**](../Model/ModelResponseOrderShipmentList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderShipmentTrackingAdd**
> \Api2Cart\Client\Model\InlineResponse20062 orderShipmentTrackingAdd($body)



Add order shipment's tracking info.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\OrderShipmentTrackingAdd(); // \Api2Cart\Client\Model\OrderShipmentTrackingAdd | 

try {
    $result = $apiInstance->orderShipmentTrackingAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderShipmentTrackingAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\OrderShipmentTrackingAdd**](../Model/OrderShipmentTrackingAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20062**](../Model/InlineResponse20062.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderShipmentUpdate**
> \Api2Cart\Client\Model\InlineResponse2004 orderShipmentUpdate($body)



Update order's shipment information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\OrderShipmentUpdate(); // \Api2Cart\Client\Model\OrderShipmentUpdate | 

try {
    $result = $apiInstance->orderShipmentUpdate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderShipmentUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\OrderShipmentUpdate**](../Model/OrderShipmentUpdate.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderStatusList**
> \Api2Cart\Client\Model\InlineResponse20063 orderStatusList($store_id, $action, $response_fields)



Retrieve list of statuses

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$store_id = "store_id_example"; // string | Store Id
$action = "action_example"; // string | Available statuses for the specified action.
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve

try {
    $result = $apiInstance->orderStatusList($store_id, $action, $response_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderStatusList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_id** | **string**| Store Id | [optional]
 **action** | **string**| Available statuses for the specified action. | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20063**](../Model/InlineResponse20063.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderTransactionList**
> \Api2Cart\Client\Model\ModelResponseOrderTransactionList orderTransactionList($order_ids, $count, $store_id, $params, $response_fields, $exclude, $page_cursor)



Retrieve list of order transaction

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_ids = "order_ids_example"; // string | Retrieves order transactions specified by order ids
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$store_id = "store_id_example"; // string | Store Id
$params = "id,order_id,amount,description"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)

try {
    $result = $apiInstance->orderTransactionList($order_ids, $count, $store_id, $params, $response_fields, $exclude, $page_cursor);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderTransactionList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_ids** | **string**| Retrieves order transactions specified by order ids |
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **store_id** | **string**| Store Id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,order_id,amount,description]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseOrderTransactionList**](../Model/ModelResponseOrderTransactionList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orderUpdate**
> \Api2Cart\Client\Model\InlineResponse2004 orderUpdate($order_id, $store_id, $order_status, $cancellation_reason, $comment, $admin_comment, $admin_private_comment, $date_modified, $date_finished, $financial_status, $fulfillment_status, $order_payment_method, $send_notifications, $origin)



Update existing order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\OrderApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_id = "order_id_example"; // string | Defines the orders specified by order id
$store_id = "store_id_example"; // string | Defines store id where the order should be found
$order_status = "order_status_example"; // string | Defines new order's status
$cancellation_reason = "cancellation_reason_example"; // string | Defines the cancellation reason when the order will be canceled
$comment = "comment_example"; // string | Specifies order comment
$admin_comment = "admin_comment_example"; // string | Specifies admin's order comment
$admin_private_comment = "admin_private_comment_example"; // string | Specifies private admin's order comment
$date_modified = "date_modified_example"; // string | Specifies order's  modification date
$date_finished = "date_finished_example"; // string | Specifies order's  finished date
$financial_status = "financial_status_example"; // string | Update order financial status to specified
$fulfillment_status = "fulfillment_status_example"; // string | Create order with fulfillment status
$order_payment_method = "order_payment_method_example"; // string | Defines order payment method.<br/>Setting order_payment_method on Shopify will also change financial_status field value to 'paid'
$send_notifications = false; // bool | Send notifications to customer after order was created
$origin = "origin_example"; // string | The source of the order

try {
    $result = $apiInstance->orderUpdate($order_id, $store_id, $order_status, $cancellation_reason, $comment, $admin_comment, $admin_private_comment, $date_modified, $date_finished, $financial_status, $fulfillment_status, $order_payment_method, $send_notifications, $origin);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderApi->orderUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **string**| Defines the orders specified by order id |
 **store_id** | **string**| Defines store id where the order should be found | [optional]
 **order_status** | **string**| Defines new order&#39;s status | [optional]
 **cancellation_reason** | **string**| Defines the cancellation reason when the order will be canceled | [optional]
 **comment** | **string**| Specifies order comment | [optional]
 **admin_comment** | **string**| Specifies admin&#39;s order comment | [optional]
 **admin_private_comment** | **string**| Specifies private admin&#39;s order comment | [optional]
 **date_modified** | **string**| Specifies order&#39;s  modification date | [optional]
 **date_finished** | **string**| Specifies order&#39;s  finished date | [optional]
 **financial_status** | **string**| Update order financial status to specified | [optional]
 **fulfillment_status** | **string**| Create order with fulfillment status | [optional]
 **order_payment_method** | **string**| Defines order payment method.&lt;br/&gt;Setting order_payment_method on Shopify will also change financial_status field value to &#39;paid&#39; | [optional]
 **send_notifications** | **bool**| Send notifications to customer after order was created | [optional] [default to false]
 **origin** | **string**| The source of the order | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

