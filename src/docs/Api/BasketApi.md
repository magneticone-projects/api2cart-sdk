# Api2Cart\Client\BasketApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**basketInfo**](BasketApi.md#basketInfo) | **GET** /basket.info.json | 
[**basketItemAdd**](BasketApi.md#basketItemAdd) | **POST** /basket.item.add.json | 
[**basketLiveShippingServiceCreate**](BasketApi.md#basketLiveShippingServiceCreate) | **POST** /basket.live_shipping_service.create.json | 
[**basketLiveShippingServiceDelete**](BasketApi.md#basketLiveShippingServiceDelete) | **DELETE** /basket.live_shipping_service.delete.json | 
[**basketLiveShippingServiceList**](BasketApi.md#basketLiveShippingServiceList) | **GET** /basket.live_shipping_service.list.json | 


# **basketInfo**
> \Api2Cart\Client\Model\InlineResponse20015 basketInfo($id, $store_id, $params, $exclude, $response_fields)



Retrieve basket information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\BasketApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$store_id = "store_id_example"; // string | Store Id
$params = "force_all"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve

try {
    $result = $apiInstance->basketInfo($id, $store_id, $params, $exclude, $response_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BasketApi->basketInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **store_id** | **string**| Store Id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to force_all]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20015**](../Model/InlineResponse20015.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **basketItemAdd**
> \Api2Cart\Client\Model\InlineResponse20016 basketItemAdd($customer_id, $product_id, $variant_id, $quantity, $store_id)



Add item to basket

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\BasketApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$customer_id = "customer_id_example"; // string | Retrieves orders specified by customer id
$product_id = "product_id_example"; // string | Defines id of the product which should be added to the basket
$variant_id = "variant_id_example"; // string | Defines product's variants specified by variant id
$quantity = 0; // float | Defines new items quantity
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->basketItemAdd($customer_id, $product_id, $variant_id, $quantity, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BasketApi->basketItemAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_id** | **string**| Retrieves orders specified by customer id |
 **product_id** | **string**| Defines id of the product which should be added to the basket |
 **variant_id** | **string**| Defines product&#39;s variants specified by variant id | [optional]
 **quantity** | **float**| Defines new items quantity | [optional] [default to 0]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20016**](../Model/InlineResponse20016.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **basketLiveShippingServiceCreate**
> \Api2Cart\Client\Model\InlineResponse20018 basketLiveShippingServiceCreate($name, $callback, $store_id)



Create live shipping rate service.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\BasketApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = "name_example"; // string | Shipping Service Name
$callback = "callback_example"; // string | Callback url that returns shipping rates. It should be able to accept POST requests with json data.
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->basketLiveShippingServiceCreate($name, $callback, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BasketApi->basketLiveShippingServiceCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**| Shipping Service Name |
 **callback** | **string**| Callback url that returns shipping rates. It should be able to accept POST requests with json data. |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20018**](../Model/InlineResponse20018.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **basketLiveShippingServiceDelete**
> \Api2Cart\Client\Model\InlineResponse20019 basketLiveShippingServiceDelete($id)



Delete live shipping rate service.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\BasketApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Entity id

try {
    $result = $apiInstance->basketLiveShippingServiceDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BasketApi->basketLiveShippingServiceDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Entity id |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20019**](../Model/InlineResponse20019.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **basketLiveShippingServiceList**
> \Api2Cart\Client\Model\InlineResponse20017 basketLiveShippingServiceList($store_id, $start, $count)



Retrieve a list of live shipping rate services.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\BasketApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$store_id = "store_id_example"; // string | Store Id
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250

try {
    $result = $apiInstance->basketLiveShippingServiceList($store_id, $start, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BasketApi->basketLiveShippingServiceList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_id** | **string**| Store Id | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20017**](../Model/InlineResponse20017.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

