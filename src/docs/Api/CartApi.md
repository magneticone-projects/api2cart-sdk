# Api2Cart\Client\CartApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cartBridge**](CartApi.md#cartBridge) | **GET** /cart.bridge.json | 
[**cartCatalogPriceRulesCount**](CartApi.md#cartCatalogPriceRulesCount) | **GET** /cart.catalog_price_rules.count.json | 
[**cartCatalogPriceRulesList**](CartApi.md#cartCatalogPriceRulesList) | **GET** /cart.catalog_price_rules.list.json | 
[**cartClearCache**](CartApi.md#cartClearCache) | **POST** /cart.clear_cache.json | 
[**cartConfig**](CartApi.md#cartConfig) | **GET** /cart.config.json | 
[**cartConfigUpdate**](CartApi.md#cartConfigUpdate) | **PUT** /cart.config.update.json | 
[**cartCouponAdd**](CartApi.md#cartCouponAdd) | **POST** /cart.coupon.add.json | 
[**cartCouponConditionAdd**](CartApi.md#cartCouponConditionAdd) | **POST** /cart.coupon.condition.add.json | 
[**cartCouponCount**](CartApi.md#cartCouponCount) | **GET** /cart.coupon.count.json | 
[**cartCouponDelete**](CartApi.md#cartCouponDelete) | **DELETE** /cart.coupon.delete.json | 
[**cartCouponList**](CartApi.md#cartCouponList) | **GET** /cart.coupon.list.json | 
[**cartCreate**](CartApi.md#cartCreate) | **POST** /cart.create.json | 
[**cartDelete**](CartApi.md#cartDelete) | **DELETE** /cart.delete.json | 
[**cartDisconnect**](CartApi.md#cartDisconnect) | **GET** /cart.disconnect.json | 
[**cartGiftcardAdd**](CartApi.md#cartGiftcardAdd) | **POST** /cart.giftcard.add.json | 
[**cartGiftcardCount**](CartApi.md#cartGiftcardCount) | **GET** /cart.giftcard.count.json | 
[**cartGiftcardDelete**](CartApi.md#cartGiftcardDelete) | **DELETE** /cart.giftcard.delete.json | 
[**cartGiftcardList**](CartApi.md#cartGiftcardList) | **GET** /cart.giftcard.list.json | 
[**cartInfo**](CartApi.md#cartInfo) | **GET** /cart.info.json | 
[**cartList**](CartApi.md#cartList) | **GET** /cart.list.json | 
[**cartMetaDataList**](CartApi.md#cartMetaDataList) | **GET** /cart.meta_data.list.json | 
[**cartMetaDataSet**](CartApi.md#cartMetaDataSet) | **POST** /cart.meta_data.set.json | 
[**cartMetaDataUnset**](CartApi.md#cartMetaDataUnset) | **DELETE** /cart.meta_data.unset.json | 
[**cartMethods**](CartApi.md#cartMethods) | **GET** /cart.methods.json | 
[**cartPluginList**](CartApi.md#cartPluginList) | **GET** /cart.plugin.list.json | 
[**cartScriptAdd**](CartApi.md#cartScriptAdd) | **POST** /cart.script.add.json | 
[**cartScriptDelete**](CartApi.md#cartScriptDelete) | **DELETE** /cart.script.delete.json | 
[**cartScriptList**](CartApi.md#cartScriptList) | **GET** /cart.script.list.json | 
[**cartShippingZonesList**](CartApi.md#cartShippingZonesList) | **GET** /cart.shipping_zones.list.json | 
[**cartValidate**](CartApi.md#cartValidate) | **GET** /cart.validate.json | 


# **cartBridge**
> \Api2Cart\Client\Model\InlineResponse20023 cartBridge()



Get bridge key and store key

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->cartBridge();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartBridge: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Api2Cart\Client\Model\InlineResponse20023**](../Model/InlineResponse20023.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartCatalogPriceRulesCount**
> \Api2Cart\Client\Model\InlineResponse20029 cartCatalogPriceRulesCount()



Get count of cart catalog price rules discounts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->cartCatalogPriceRulesCount();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartCatalogPriceRulesCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Api2Cart\Client\Model\InlineResponse20029**](../Model/InlineResponse20029.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartCatalogPriceRulesList**
> \Api2Cart\Client\Model\ModelResponseCartCatalogPriceRulesList cartCatalogPriceRulesList($page_cursor, $start, $count, $ids, $params, $response_fields, $exclude)



Get cart catalog price rules discounts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$ids = "ids_example"; // string | Retrieves  catalog_price_rules by ids
$params = "id,name,description"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all

try {
    $result = $apiInstance->cartCatalogPriceRulesList($page_cursor, $start, $count, $ids, $params, $response_fields, $exclude);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartCatalogPriceRulesList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **ids** | **string**| Retrieves  catalog_price_rules by ids | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name,description]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseCartCatalogPriceRulesList**](../Model/ModelResponseCartCatalogPriceRulesList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartClearCache**
> \Api2Cart\Client\Model\InlineResponse20027 cartClearCache($cache_type)



Clear cache on store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$cache_type = "cache_type_example"; // string | Defines which cache should be cleared.

try {
    $result = $apiInstance->cartClearCache($cache_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartClearCache: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cache_type** | **string**| Defines which cache should be cleared. |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20027**](../Model/InlineResponse20027.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartConfig**
> \Api2Cart\Client\Model\InlineResponse20026 cartConfig($params, $exclude)



Get list of cart configs

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$params = "store_name,store_url,db_prefix"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all

try {
    $result = $apiInstance->cartConfig($params, $exclude);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartConfig: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to store_name,store_url,db_prefix]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20026**](../Model/InlineResponse20026.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartConfigUpdate**
> \Api2Cart\Client\Model\InlineResponse20030 cartConfigUpdate($body)



Use this API method to update custom data in client database.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\CartConfigUpdate(); // \Api2Cart\Client\Model\CartConfigUpdate | 

try {
    $result = $apiInstance->cartConfigUpdate($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartConfigUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\CartConfigUpdate**](../Model/CartConfigUpdate.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20030**](../Model/InlineResponse20030.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartCouponAdd**
> \Api2Cart\Client\Model\InlineResponse20032 cartCouponAdd($body)



Use this method to create a coupon with specified conditions.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\CartCouponAdd(); // \Api2Cart\Client\Model\CartCouponAdd | 

try {
    $result = $apiInstance->cartCouponAdd($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartCouponAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\CartCouponAdd**](../Model/CartCouponAdd.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20032**](../Model/InlineResponse20032.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartCouponConditionAdd**
> \Api2Cart\Client\Model\InlineResponse20019 cartCouponConditionAdd($coupon_id, $entity, $key, $operator, $value, $store_id, $target, $include_tax, $include_shipping)



Use this method to add additional conditions for coupon application.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$coupon_id = "coupon_id_example"; // string | Coupon Id
$entity = "entity_example"; // string | Defines condition entity type
$key = "key_example"; // string | Defines condition entity attribute key
$operator = "operator_example"; // string | Defines condition operator
$value = "value_example"; // string | Defines condition value, can be comma separated according to the operator.
$store_id = "store_id_example"; // string | Store Id
$target = "coupon_prerequisite"; // string | Defines condition operator
$include_tax = false; // bool | Indicates whether to apply a discount for taxes.
$include_shipping = false; // bool | Indicates whether to apply a discount for shipping.

try {
    $result = $apiInstance->cartCouponConditionAdd($coupon_id, $entity, $key, $operator, $value, $store_id, $target, $include_tax, $include_shipping);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartCouponConditionAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **coupon_id** | **string**| Coupon Id |
 **entity** | **string**| Defines condition entity type |
 **key** | **string**| Defines condition entity attribute key |
 **operator** | **string**| Defines condition operator |
 **value** | **string**| Defines condition value, can be comma separated according to the operator. |
 **store_id** | **string**| Store Id | [optional]
 **target** | **string**| Defines condition operator | [optional] [default to coupon_prerequisite]
 **include_tax** | **bool**| Indicates whether to apply a discount for taxes. | [optional] [default to false]
 **include_shipping** | **bool**| Indicates whether to apply a discount for shipping. | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20019**](../Model/InlineResponse20019.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartCouponCount**
> \Api2Cart\Client\Model\InlineResponse20031 cartCouponCount($store_id, $date_start_from, $date_start_to, $date_end_from, $date_end_to, $avail)



This method allows you to get the number of coupons. On some platforms, you can filter the coupons by the date they were active.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$store_id = "store_id_example"; // string | Store Id
$date_start_from = "date_start_from_example"; // string | Filter entity by date_start (greater or equal)
$date_start_to = "date_start_to_example"; // string | Filter entity by date_start (less or equal)
$date_end_from = "date_end_from_example"; // string | Filter entity by date_end (greater or equal)
$date_end_to = "date_end_to_example"; // string | Filter entity by date_end (less or equal)
$avail = true; // bool | Defines category's visibility status

try {
    $result = $apiInstance->cartCouponCount($store_id, $date_start_from, $date_start_to, $date_end_from, $date_end_to, $avail);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartCouponCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_id** | **string**| Store Id | [optional]
 **date_start_from** | **string**| Filter entity by date_start (greater or equal) | [optional]
 **date_start_to** | **string**| Filter entity by date_start (less or equal) | [optional]
 **date_end_from** | **string**| Filter entity by date_end (greater or equal) | [optional]
 **date_end_to** | **string**| Filter entity by date_end (less or equal) | [optional]
 **avail** | **bool**| Defines category&#39;s visibility status | [optional] [default to true]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20031**](../Model/InlineResponse20031.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartCouponDelete**
> \Api2Cart\Client\Model\InlineResponse2009 cartCouponDelete($id, $store_id)



Delete coupon

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->cartCouponDelete($id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartCouponDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartCouponList**
> \Api2Cart\Client\Model\ModelResponseCartCouponList cartCouponList($page_cursor, $start, $count, $coupons_ids, $store_id, $date_start_from, $date_start_to, $date_end_from, $date_end_to, $avail, $lang_id, $params, $response_fields, $exclude)



Get cart coupon discounts.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$coupons_ids = "coupons_ids_example"; // string | Filter coupons by ids
$store_id = "store_id_example"; // string | Filter coupons by store id
$date_start_from = "date_start_from_example"; // string | Filter entity by date_start (greater or equal)
$date_start_to = "date_start_to_example"; // string | Filter entity by date_start (less or equal)
$date_end_from = "date_end_from_example"; // string | Filter entity by date_end (greater or equal)
$date_end_to = "date_end_to_example"; // string | Filter entity by date_end (less or equal)
$avail = true; // bool | Filter coupons by avail status
$lang_id = "lang_id_example"; // string | Language id
$params = "id,code,name,description"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all

try {
    $result = $apiInstance->cartCouponList($page_cursor, $start, $count, $coupons_ids, $store_id, $date_start_from, $date_start_to, $date_end_from, $date_end_to, $avail, $lang_id, $params, $response_fields, $exclude);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartCouponList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **coupons_ids** | **string**| Filter coupons by ids | [optional]
 **store_id** | **string**| Filter coupons by store id | [optional]
 **date_start_from** | **string**| Filter entity by date_start (greater or equal) | [optional]
 **date_start_to** | **string**| Filter entity by date_start (less or equal) | [optional]
 **date_end_from** | **string**| Filter entity by date_end (greater or equal) | [optional]
 **date_end_to** | **string**| Filter entity by date_end (less or equal) | [optional]
 **avail** | **bool**| Filter coupons by avail status | [optional]
 **lang_id** | **string**| Language id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,code,name,description]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseCartCouponList**](../Model/ModelResponseCartCouponList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartCreate**
> \Api2Cart\Client\Model\InlineResponse2003 cartCreate($cart_id, $store_id, $store_url, $bridge_url, $store_root, $store_key, $shared_secret, $validate_version, $verify, $db_tables_prefix, $user_agent, $ftp_host, $ftp_user, $ftp_password, $ftp_port, $ftp_store_dir, $api_key_3dcart, $admin_account, $api_path, $api_key, $client_id, $access_token, $context, $access_token2, $api_key_shopify, $api_password, $access_token_shopify, $api_key2, $api_username, $encrypted_password, $login, $api_user_adnsf, $api_pass, $private_key, $app_token, $etsy_keystring, $etsy_shared_secret, $token_secret, $etsy_client_id, $etsy_refresh_token, $ebay_client_id, $ebay_client_secret, $ebay_runame, $ebay_access_token, $ebay_refresh_token, $ebay_environment, $ebay_site_id, $dw_client_id, $dw_api_pass, $demandware_user_name, $demandware_user_password, $seller_id, $environment, $hybris_client_id, $hybris_client_secret, $hybris_username, $hybris_password, $hybris_websites, $walmart_client_id, $walmart_client_secret, $walmart_environment, $walmart_channel_type, $walmart_region, $lightspeed_api_key, $lightspeed_api_secret, $shoplazza_access_token, $shoplazza_shared_secret, $shopware_access_key, $shopware_api_key, $shopware_api_secret, $commercehq_api_key, $commercehq_api_password, $_3dcart_private_key, $_3dcart_access_token, $wc_consumer_key, $wc_consumer_secret, $magento_consumer_key, $magento_consumer_secret, $magento_access_token, $magento_token_secret, $prestashop_webservice_key, $wix_app_id, $wix_app_secret_key, $wix_refresh_token, $mercado_libre_app_id, $mercado_libre_app_secret_key, $mercado_libre_refresh_token, $zid_client_id, $zid_client_secret, $zid_access_token, $zid_authorization, $zid_refresh_token, $flipkart_client_id, $flipkart_client_secret, $allegro_client_id, $allegro_client_secret, $allegro_access_token, $allegro_refresh_token, $allegro_environment)



Add store to the account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$cart_id = "cart_id_example"; // string | Store’s identifier which you can get from cart_list method
$store_id = "store_id_example"; // string | Store Id
$store_url = "store_url_example"; // string | A web address of a store that you would like to connect to API2Cart
$bridge_url = "bridge_url_example"; // string | This parameter allows to set up store with custom bridge url (also you must use store_root parameter if a bridge folder is not in the root folder of the store)
$store_root = "store_root_example"; // string | Absolute path to the store root directory (used with \"bridge_url\" parameter)
$store_key = "store_key_example"; // string | Set this parameter if bridge is already uploaded to store
$shared_secret = "shared_secret_example"; // string | Shared secret
$validate_version = false; // bool | Specify if api2cart should validate cart version
$verify = true; // bool | Enables or disables cart's verification
$db_tables_prefix = "db_tables_prefix_example"; // string | DB tables prefix
$user_agent = "user_agent_example"; // string | This parameter allows you to set your custom user agent, which will be used in requests to the store. Please use it cautiously, as the store's firewall may block specific values.
$ftp_host = "ftp_host_example"; // string | FTP connection host
$ftp_user = "ftp_user_example"; // string | FTP User
$ftp_password = "ftp_password_example"; // string | FTP Password
$ftp_port = 56; // int | FTP Port
$ftp_store_dir = "ftp_store_dir_example"; // string | FTP Store dir
$api_key_3dcart = "api_key_3dcart_example"; // string | 3DCart API Key
$admin_account = "admin_account_example"; // string | It's a BigCommerce account for which API is enabled
$api_path = "api_path_example"; // string | BigCommerce API URL
$api_key = "api_key_example"; // string | Bigcommerce API Key
$client_id = "client_id_example"; // string | Client ID of the requesting app
$access_token = "access_token_example"; // string | Access token authorizing the app to access resources on behalf of a user
$context = "context_example"; // string | API Path section unique to the store
$access_token2 = "access_token_example"; // string | Access token authorizing the app to access resources on behalf of a user
$api_key_shopify = "api_key_shopify_example"; // string | Shopify API Key
$api_password = "api_password_example"; // string | Shopify API Password
$access_token_shopify = "access_token_shopify_example"; // string | Access token authorizing the app to access resources on behalf of a user
$api_key2 = "api_key_example"; // string | Neto API Key
$api_username = "api_username_example"; // string | Neto User Name
$encrypted_password = "encrypted_password_example"; // string | Volusion API Password
$login = "login_example"; // string | It's a Volusion account for which API is enabled
$api_user_adnsf = "api_user_adnsf_example"; // string | It's a AspDotNetStorefront account for which API is available
$api_pass = "api_pass_example"; // string | AspDotNetStorefront API Password
$private_key = "private_key_example"; // string | 3DCart Application Private Key
$app_token = "app_token_example"; // string | 3DCart Token from Application
$etsy_keystring = "etsy_keystring_example"; // string | Etsy keystring
$etsy_shared_secret = "etsy_shared_secret_example"; // string | Etsy shared secret
$token_secret = "token_secret_example"; // string | Secret token authorizing the app to access resources on behalf of a user
$etsy_client_id = "etsy_client_id_example"; // string | Etsy Client Id
$etsy_refresh_token = "etsy_refresh_token_example"; // string | Etsy Refresh token
$ebay_client_id = "ebay_client_id_example"; // string | Application ID (AppID).
$ebay_client_secret = "ebay_client_secret_example"; // string | Shared Secret from eBay application
$ebay_runame = "ebay_runame_example"; // string | The RuName value that eBay assigns to your application.
$ebay_access_token = "ebay_access_token_example"; // string | Used to authenticate API requests.
$ebay_refresh_token = "ebay_refresh_token_example"; // string | Used to renew the access token.
$ebay_environment = "production"; // string | eBay environment
$ebay_site_id = 0; // int | eBay global ID
$dw_client_id = "dw_client_id_example"; // string | Demandware client id
$dw_api_pass = "dw_api_pass_example"; // string | Demandware api password
$demandware_user_name = "demandware_user_name_example"; // string | Demandware user name
$demandware_user_password = "demandware_user_password_example"; // string | Demandware user password
$seller_id = "seller_id_example"; // string | Seller Id
$environment = "production"; // string | 
$hybris_client_id = "hybris_client_id_example"; // string | Omni Commerce Connector Client ID
$hybris_client_secret = "hybris_client_secret_example"; // string | Omni Commerce Connector Client Secret
$hybris_username = "hybris_username_example"; // string | User Name
$hybris_password = "hybris_password_example"; // string | User password
$hybris_websites = array("hybris_websites_example"); // string[] | Websites to stores mapping data
$walmart_client_id = "walmart_client_id_example"; // string | Walmart client ID. For the region 'ca' use Consumer ID
$walmart_client_secret = "walmart_client_secret_example"; // string | Walmart client secret. For the region 'ca' use Private Key
$walmart_environment = "production"; // string | Walmart environment
$walmart_channel_type = "walmart_channel_type_example"; // string | Walmart WM_CONSUMER.CHANNEL.TYPE header
$walmart_region = "us"; // string | Walmart region
$lightspeed_api_key = "lightspeed_api_key_example"; // string | LightSpeed api key
$lightspeed_api_secret = "lightspeed_api_secret_example"; // string | LightSpeed api secret
$shoplazza_access_token = "shoplazza_access_token_example"; // string | Access token authorizing the app to access resources on behalf of a user
$shoplazza_shared_secret = "shoplazza_shared_secret_example"; // string | Shared secret
$shopware_access_key = "shopware_access_key_example"; // string | Shopware access key
$shopware_api_key = "shopware_api_key_example"; // string | Shopware api key
$shopware_api_secret = "shopware_api_secret_example"; // string | Shopware client secret access key
$commercehq_api_key = "commercehq_api_key_example"; // string | CommerceHQ api key
$commercehq_api_password = "commercehq_api_password_example"; // string | CommerceHQ api password
$_3dcart_private_key = "_3dcart_private_key_example"; // string | 3DCart Private Key
$_3dcart_access_token = "_3dcart_access_token_example"; // string | 3DCart Token
$wc_consumer_key = "wc_consumer_key_example"; // string | Woocommerce consumer key
$wc_consumer_secret = "wc_consumer_secret_example"; // string | Woocommerce consumer secret
$magento_consumer_key = "magento_consumer_key_example"; // string | Magento Consumer Key
$magento_consumer_secret = "magento_consumer_secret_example"; // string | Magento Consumer Secret
$magento_access_token = "magento_access_token_example"; // string | Magento Access Token
$magento_token_secret = "magento_token_secret_example"; // string | Magento Token Secret
$prestashop_webservice_key = "prestashop_webservice_key_example"; // string | Prestashop webservice key
$wix_app_id = "wix_app_id_example"; // string | Wix App ID
$wix_app_secret_key = "wix_app_secret_key_example"; // string | Wix App Secret Key
$wix_refresh_token = "wix_refresh_token_example"; // string | Wix refresh token
$mercado_libre_app_id = "mercado_libre_app_id_example"; // string | Mercado Libre App ID
$mercado_libre_app_secret_key = "mercado_libre_app_secret_key_example"; // string | Mercado Libre App Secret Key
$mercado_libre_refresh_token = "mercado_libre_refresh_token_example"; // string | Mercado Libre Refresh Token
$zid_client_id = 56; // int | Zid Client ID
$zid_client_secret = "zid_client_secret_example"; // string | Zid Client Secret
$zid_access_token = "zid_access_token_example"; // string | Zid Access Token
$zid_authorization = "zid_authorization_example"; // string | Zid Authorization
$zid_refresh_token = "zid_refresh_token_example"; // string | Zid refresh token
$flipkart_client_id = "flipkart_client_id_example"; // string | Flipkart Client ID
$flipkart_client_secret = "flipkart_client_secret_example"; // string | Flipkart Client Secret
$allegro_client_id = "allegro_client_id_example"; // string | Allegro Client ID
$allegro_client_secret = "allegro_client_secret_example"; // string | Allegro Client Secret
$allegro_access_token = "allegro_access_token_example"; // string | Allegro Access Token
$allegro_refresh_token = "allegro_refresh_token_example"; // string | Allegro Refresh Token
$allegro_environment = "production"; // string | Allegro Environment

try {
    $result = $apiInstance->cartCreate($cart_id, $store_id, $store_url, $bridge_url, $store_root, $store_key, $shared_secret, $validate_version, $verify, $db_tables_prefix, $user_agent, $ftp_host, $ftp_user, $ftp_password, $ftp_port, $ftp_store_dir, $api_key_3dcart, $admin_account, $api_path, $api_key, $client_id, $access_token, $context, $access_token2, $api_key_shopify, $api_password, $access_token_shopify, $api_key2, $api_username, $encrypted_password, $login, $api_user_adnsf, $api_pass, $private_key, $app_token, $etsy_keystring, $etsy_shared_secret, $token_secret, $etsy_client_id, $etsy_refresh_token, $ebay_client_id, $ebay_client_secret, $ebay_runame, $ebay_access_token, $ebay_refresh_token, $ebay_environment, $ebay_site_id, $dw_client_id, $dw_api_pass, $demandware_user_name, $demandware_user_password, $seller_id, $environment, $hybris_client_id, $hybris_client_secret, $hybris_username, $hybris_password, $hybris_websites, $walmart_client_id, $walmart_client_secret, $walmart_environment, $walmart_channel_type, $walmart_region, $lightspeed_api_key, $lightspeed_api_secret, $shoplazza_access_token, $shoplazza_shared_secret, $shopware_access_key, $shopware_api_key, $shopware_api_secret, $commercehq_api_key, $commercehq_api_password, $_3dcart_private_key, $_3dcart_access_token, $wc_consumer_key, $wc_consumer_secret, $magento_consumer_key, $magento_consumer_secret, $magento_access_token, $magento_token_secret, $prestashop_webservice_key, $wix_app_id, $wix_app_secret_key, $wix_refresh_token, $mercado_libre_app_id, $mercado_libre_app_secret_key, $mercado_libre_refresh_token, $zid_client_id, $zid_client_secret, $zid_access_token, $zid_authorization, $zid_refresh_token, $flipkart_client_id, $flipkart_client_secret, $allegro_client_id, $allegro_client_secret, $allegro_access_token, $allegro_refresh_token, $allegro_environment);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cart_id** | **string**| Store’s identifier which you can get from cart_list method |
 **store_id** | **string**| Store Id |
 **store_url** | **string**| A web address of a store that you would like to connect to API2Cart | [optional]
 **bridge_url** | **string**| This parameter allows to set up store with custom bridge url (also you must use store_root parameter if a bridge folder is not in the root folder of the store) | [optional]
 **store_root** | **string**| Absolute path to the store root directory (used with \&quot;bridge_url\&quot; parameter) | [optional]
 **store_key** | **string**| Set this parameter if bridge is already uploaded to store | [optional]
 **shared_secret** | **string**| Shared secret | [optional]
 **validate_version** | **bool**| Specify if api2cart should validate cart version | [optional] [default to false]
 **verify** | **bool**| Enables or disables cart&#39;s verification | [optional] [default to true]
 **db_tables_prefix** | **string**| DB tables prefix | [optional]
 **user_agent** | **string**| This parameter allows you to set your custom user agent, which will be used in requests to the store. Please use it cautiously, as the store&#39;s firewall may block specific values. | [optional]
 **ftp_host** | **string**| FTP connection host | [optional]
 **ftp_user** | **string**| FTP User | [optional]
 **ftp_password** | **string**| FTP Password | [optional]
 **ftp_port** | **int**| FTP Port | [optional]
 **ftp_store_dir** | **string**| FTP Store dir | [optional]
 **api_key_3dcart** | **string**| 3DCart API Key | [optional]
 **admin_account** | **string**| It&#39;s a BigCommerce account for which API is enabled | [optional]
 **api_path** | **string**| BigCommerce API URL | [optional]
 **api_key** | **string**| Bigcommerce API Key | [optional]
 **client_id** | **string**| Client ID of the requesting app | [optional]
 **access_token** | **string**| Access token authorizing the app to access resources on behalf of a user | [optional]
 **context** | **string**| API Path section unique to the store | [optional]
 **access_token2** | **string**| Access token authorizing the app to access resources on behalf of a user | [optional]
 **api_key_shopify** | **string**| Shopify API Key | [optional]
 **api_password** | **string**| Shopify API Password | [optional]
 **access_token_shopify** | **string**| Access token authorizing the app to access resources on behalf of a user | [optional]
 **api_key2** | **string**| Neto API Key | [optional]
 **api_username** | **string**| Neto User Name | [optional]
 **encrypted_password** | **string**| Volusion API Password | [optional]
 **login** | **string**| It&#39;s a Volusion account for which API is enabled | [optional]
 **api_user_adnsf** | **string**| It&#39;s a AspDotNetStorefront account for which API is available | [optional]
 **api_pass** | **string**| AspDotNetStorefront API Password | [optional]
 **private_key** | **string**| 3DCart Application Private Key | [optional]
 **app_token** | **string**| 3DCart Token from Application | [optional]
 **etsy_keystring** | **string**| Etsy keystring | [optional]
 **etsy_shared_secret** | **string**| Etsy shared secret | [optional]
 **token_secret** | **string**| Secret token authorizing the app to access resources on behalf of a user | [optional]
 **etsy_client_id** | **string**| Etsy Client Id | [optional]
 **etsy_refresh_token** | **string**| Etsy Refresh token | [optional]
 **ebay_client_id** | **string**| Application ID (AppID). | [optional]
 **ebay_client_secret** | **string**| Shared Secret from eBay application | [optional]
 **ebay_runame** | **string**| The RuName value that eBay assigns to your application. | [optional]
 **ebay_access_token** | **string**| Used to authenticate API requests. | [optional]
 **ebay_refresh_token** | **string**| Used to renew the access token. | [optional]
 **ebay_environment** | **string**| eBay environment | [optional] [default to production]
 **ebay_site_id** | **int**| eBay global ID | [optional] [default to 0]
 **dw_client_id** | **string**| Demandware client id | [optional]
 **dw_api_pass** | **string**| Demandware api password | [optional]
 **demandware_user_name** | **string**| Demandware user name | [optional]
 **demandware_user_password** | **string**| Demandware user password | [optional]
 **seller_id** | **string**| Seller Id | [optional]
 **environment** | **string**|  | [optional] [default to production]
 **hybris_client_id** | **string**| Omni Commerce Connector Client ID | [optional]
 **hybris_client_secret** | **string**| Omni Commerce Connector Client Secret | [optional]
 **hybris_username** | **string**| User Name | [optional]
 **hybris_password** | **string**| User password | [optional]
 **hybris_websites** | [**string[]**](../Model/string.md)| Websites to stores mapping data | [optional]
 **walmart_client_id** | **string**| Walmart client ID. For the region &#39;ca&#39; use Consumer ID | [optional]
 **walmart_client_secret** | **string**| Walmart client secret. For the region &#39;ca&#39; use Private Key | [optional]
 **walmart_environment** | **string**| Walmart environment | [optional] [default to production]
 **walmart_channel_type** | **string**| Walmart WM_CONSUMER.CHANNEL.TYPE header | [optional]
 **walmart_region** | **string**| Walmart region | [optional] [default to us]
 **lightspeed_api_key** | **string**| LightSpeed api key | [optional]
 **lightspeed_api_secret** | **string**| LightSpeed api secret | [optional]
 **shoplazza_access_token** | **string**| Access token authorizing the app to access resources on behalf of a user | [optional]
 **shoplazza_shared_secret** | **string**| Shared secret | [optional]
 **shopware_access_key** | **string**| Shopware access key | [optional]
 **shopware_api_key** | **string**| Shopware api key | [optional]
 **shopware_api_secret** | **string**| Shopware client secret access key | [optional]
 **commercehq_api_key** | **string**| CommerceHQ api key | [optional]
 **commercehq_api_password** | **string**| CommerceHQ api password | [optional]
 **_3dcart_private_key** | **string**| 3DCart Private Key | [optional]
 **_3dcart_access_token** | **string**| 3DCart Token | [optional]
 **wc_consumer_key** | **string**| Woocommerce consumer key | [optional]
 **wc_consumer_secret** | **string**| Woocommerce consumer secret | [optional]
 **magento_consumer_key** | **string**| Magento Consumer Key | [optional]
 **magento_consumer_secret** | **string**| Magento Consumer Secret | [optional]
 **magento_access_token** | **string**| Magento Access Token | [optional]
 **magento_token_secret** | **string**| Magento Token Secret | [optional]
 **prestashop_webservice_key** | **string**| Prestashop webservice key | [optional]
 **wix_app_id** | **string**| Wix App ID | [optional]
 **wix_app_secret_key** | **string**| Wix App Secret Key | [optional]
 **wix_refresh_token** | **string**| Wix refresh token | [optional]
 **mercado_libre_app_id** | **string**| Mercado Libre App ID | [optional]
 **mercado_libre_app_secret_key** | **string**| Mercado Libre App Secret Key | [optional]
 **mercado_libre_refresh_token** | **string**| Mercado Libre Refresh Token | [optional]
 **zid_client_id** | **int**| Zid Client ID | [optional]
 **zid_client_secret** | **string**| Zid Client Secret | [optional]
 **zid_access_token** | **string**| Zid Access Token | [optional]
 **zid_authorization** | **string**| Zid Authorization | [optional]
 **zid_refresh_token** | **string**| Zid refresh token | [optional]
 **flipkart_client_id** | **string**| Flipkart Client ID | [optional]
 **flipkart_client_secret** | **string**| Flipkart Client Secret | [optional]
 **allegro_client_id** | **string**| Allegro Client ID | [optional]
 **allegro_client_secret** | **string**| Allegro Client Secret | [optional]
 **allegro_access_token** | **string**| Allegro Access Token | [optional]
 **allegro_refresh_token** | **string**| Allegro Refresh Token | [optional]
 **allegro_environment** | **string**| Allegro Environment | [optional] [default to production]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartDelete**
> \Api2Cart\Client\Model\InlineResponse20028 cartDelete($delete_bridge)



Remove store from API2Cart

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delete_bridge = true; // bool | Identifies if there is a necessity to delete bridge

try {
    $result = $apiInstance->cartDelete($delete_bridge);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_bridge** | **bool**| Identifies if there is a necessity to delete bridge | [optional] [default to true]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20028**](../Model/InlineResponse20028.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartDisconnect**
> \Api2Cart\Client\Model\InlineResponse20024 cartDisconnect($delete_bridge)



Disconnect with the store and clear store session data.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delete_bridge = false; // bool | Identifies if there is a necessity to delete bridge

try {
    $result = $apiInstance->cartDisconnect($delete_bridge);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartDisconnect: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_bridge** | **bool**| Identifies if there is a necessity to delete bridge | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20024**](../Model/InlineResponse20024.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartGiftcardAdd**
> \Api2Cart\Client\Model\InlineResponse20034 cartGiftcardAdd($amount, $code, $owner_email, $recipient_email, $recipient_name, $owner_name)



Use this method to create a gift card for a specified amount.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$amount = 8.14; // float | Defines the gift card amount value.
$code = "code_example"; // string | Gift card code
$owner_email = "owner_email_example"; // string | Gift card owner email
$recipient_email = "recipient_email_example"; // string | Gift card recipient email
$recipient_name = "recipient_name_example"; // string | Gift card recipient name
$owner_name = "owner_name_example"; // string | Gift card owner name

try {
    $result = $apiInstance->cartGiftcardAdd($amount, $code, $owner_email, $recipient_email, $recipient_name, $owner_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartGiftcardAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **amount** | **float**| Defines the gift card amount value. |
 **code** | **string**| Gift card code | [optional]
 **owner_email** | **string**| Gift card owner email | [optional]
 **recipient_email** | **string**| Gift card recipient email | [optional]
 **recipient_name** | **string**| Gift card recipient name | [optional]
 **owner_name** | **string**| Gift card owner name | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20034**](../Model/InlineResponse20034.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartGiftcardCount**
> \Api2Cart\Client\Model\InlineResponse20033 cartGiftcardCount($store_id)



Get gift cards count.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->cartGiftcardCount($store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartGiftcardCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20033**](../Model/InlineResponse20033.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartGiftcardDelete**
> \Api2Cart\Client\Model\InlineResponse2009 cartGiftcardDelete($id)



Delete giftcard

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id

try {
    $result = $apiInstance->cartGiftcardDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartGiftcardDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartGiftcardList**
> \Api2Cart\Client\Model\ModelResponseCartGiftCardList cartGiftcardList($page_cursor, $start, $count, $store_id, $params, $response_fields, $exclude)



Get gift cards list.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$store_id = "store_id_example"; // string | Store Id
$params = "id,code,name"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all

try {
    $result = $apiInstance->cartGiftcardList($page_cursor, $start, $count, $store_id, $params, $response_fields, $exclude);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartGiftcardList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **store_id** | **string**| Store Id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,code,name]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseCartGiftCardList**](../Model/ModelResponseCartGiftCardList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartInfo**
> \Api2Cart\Client\Model\InlineResponse20020 cartInfo($params, $response_fields, $exclude, $store_id)



This method allows you to get various information about the store, including a list of stores (in the case of a multistore configuration), a list of supported languages, currencies, carriers, warehouses, and many other information. This information contains data that is relatively stable and rarely changes, so API2Cart can cache certain data to reduce the load on the store and speed up the execution of the request. We also recommend that you cache the response of this method on your side to save requests. If you need to clear the cache for a specific store, then use the cart.validate method.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$params = "store_name,store_url,db_prefix"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->cartInfo($params, $response_fields, $exclude, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to store_name,store_url,db_prefix]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20020**](../Model/InlineResponse20020.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartList**
> \Api2Cart\Client\Model\InlineResponse20022 cartList()



Get list of supported carts

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->cartList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Api2Cart\Client\Model\InlineResponse20022**](../Model/InlineResponse20022.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartMetaDataList**
> \Api2Cart\Client\Model\ModelResponseCartMetaDataList cartMetaDataList($entity_id, $entity, $store_id, $lang_id, $key, $count, $page_cursor, $params, $response_fields, $exclude)



Using this method, you can get a list of metadata for various entities (products, options, customers, orders). Usually this is data created by third-party plugins.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$entity_id = "entity_id_example"; // string | Entity Id
$entity = "product"; // string | Entity
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$key = "key_example"; // string | Key
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$params = "key,value"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all

try {
    $result = $apiInstance->cartMetaDataList($entity_id, $entity, $store_id, $lang_id, $key, $count, $page_cursor, $params, $response_fields, $exclude);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartMetaDataList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_id** | **string**| Entity Id |
 **entity** | **string**| Entity | [optional] [default to product]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **key** | **string**| Key | [optional]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to key,value]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseCartMetaDataList**](../Model/ModelResponseCartMetaDataList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartMetaDataSet**
> \Api2Cart\Client\Model\InlineResponse2007 cartMetaDataSet($entity_id, $key, $value, $namespace, $entity, $store_id, $lang_id)



Set meta data for a specific entity

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$entity_id = "entity_id_example"; // string | Entity Id
$key = "key_example"; // string | Key
$value = "value_example"; // string | Value
$namespace = "namespace_example"; // string | Metafield namespace
$entity = "product"; // string | Entity
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id

try {
    $result = $apiInstance->cartMetaDataSet($entity_id, $key, $value, $namespace, $entity, $store_id, $lang_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartMetaDataSet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_id** | **string**| Entity Id |
 **key** | **string**| Key |
 **value** | **string**| Value |
 **namespace** | **string**| Metafield namespace |
 **entity** | **string**| Entity | [optional] [default to product]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartMetaDataUnset**
> \Api2Cart\Client\Model\InlineResponse20019 cartMetaDataUnset($entity_id, $key, $id, $entity, $store_id)



Unset meta data for a specific entity

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$entity_id = "entity_id_example"; // string | Entity Id
$key = "key_example"; // string | Key
$id = "id_example"; // string | Entity id
$entity = "product"; // string | Entity
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->cartMetaDataUnset($entity_id, $key, $id, $entity, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartMetaDataUnset: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_id** | **string**| Entity Id |
 **key** | **string**| Key |
 **id** | **string**| Entity id |
 **entity** | **string**| Entity | [optional] [default to product]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20019**](../Model/InlineResponse20019.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartMethods**
> \Api2Cart\Client\Model\InlineResponse20025 cartMethods()



Returns a list of supported API methods.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->cartMethods();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartMethods: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Api2Cart\Client\Model\InlineResponse20025**](../Model/InlineResponse20025.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartPluginList**
> \Api2Cart\Client\Model\InlineResponse20035 cartPluginList($store_key, $store_id, $start, $count)



Get a list of third-party plugins installed on the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$store_key = "store_key_example"; // string | Set this parameter if bridge is already uploaded to store
$store_id = "store_id_example"; // string | Store Id
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250

try {
    $result = $apiInstance->cartPluginList($store_key, $store_id, $start, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartPluginList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_key** | **string**| Set this parameter if bridge is already uploaded to store | [optional]
 **store_id** | **string**| Store Id | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20035**](../Model/InlineResponse20035.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartScriptAdd**
> \Api2Cart\Client\Model\InlineResponse20036 cartScriptAdd($name, $description, $html, $src, $load_method, $scope, $events, $store_id)



Add new script to the storefront

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = "name_example"; // string | The user-friendly script name
$description = "description_example"; // string | The user-friendly description
$html = "html_example"; // string | An html string containing exactly one `script` tag.
$src = "src_example"; // string | The URL of the remote script
$load_method = "load_method_example"; // string | The load method to use for the script
$scope = "storefront"; // string | The page or pages on the online store where the script should be included
$events = "events_example"; // string | Event for run scripts
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->cartScriptAdd($name, $description, $html, $src, $load_method, $scope, $events, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartScriptAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**| The user-friendly script name | [optional]
 **description** | **string**| The user-friendly description | [optional]
 **html** | **string**| An html string containing exactly one &#x60;script&#x60; tag. | [optional]
 **src** | **string**| The URL of the remote script | [optional]
 **load_method** | **string**| The load method to use for the script | [optional]
 **scope** | **string**| The page or pages on the online store where the script should be included | [optional] [default to storefront]
 **events** | **string**| Event for run scripts | [optional]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20036**](../Model/InlineResponse20036.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartScriptDelete**
> \Api2Cart\Client\Model\InlineResponse2009 cartScriptDelete($id, $store_id)



Remove script from the storefront

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->cartScriptDelete($id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartScriptDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartScriptList**
> \Api2Cart\Client\Model\ModelResponseCartScriptList cartScriptList($page_cursor, $start, $count, $created_from, $created_to, $modified_from, $modified_to, $script_ids, $store_id, $params, $response_fields, $exclude)



Get scripts installed to the storefront

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$script_ids = "script_ids_example"; // string | Retrieves only scripts with specific ids
$store_id = "store_id_example"; // string | Store Id
$params = "id,name,description"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all

try {
    $result = $apiInstance->cartScriptList($page_cursor, $start, $count, $created_from, $created_to, $modified_from, $modified_to, $script_ids, $store_id, $params, $response_fields, $exclude);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartScriptList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **script_ids** | **string**| Retrieves only scripts with specific ids | [optional]
 **store_id** | **string**| Store Id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name,description]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]

### Return type

[**\Api2Cart\Client\Model\ModelResponseCartScriptList**](../Model/ModelResponseCartScriptList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartShippingZonesList**
> \Api2Cart\Client\Model\InlineResponse20037 cartShippingZonesList($store_id, $start, $count, $params, $response_fields, $exclude)



Get list of shipping zones

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$store_id = "store_id_example"; // string | Store Id
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$params = "id,name,enabled"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all

try {
    $result = $apiInstance->cartShippingZonesList($store_id, $start, $count, $params, $response_fields, $exclude);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartShippingZonesList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_id** | **string**| Store Id | [optional]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,name,enabled]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20037**](../Model/InlineResponse20037.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cartValidate**
> \Api2Cart\Client\Model\InlineResponse20021 cartValidate($validate_version)



This method clears the cache in API2Cart for a particular store and checks whether the connection to the store is available. Use this method if there have been any changes in the settings on the storе, for example, if a new plugin has been installed or removed.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CartApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$validate_version = false; // bool | Specify if api2cart should validate cart version

try {
    $result = $apiInstance->cartValidate($validate_version);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->cartValidate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **validate_version** | **bool**| Specify if api2cart should validate cart version | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20021**](../Model/InlineResponse20021.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

