# Api2Cart\Client\MarketplaceApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**marketplaceProductFind**](MarketplaceApi.md#marketplaceProductFind) | **GET** /marketplace.product.find.json | 


# **marketplaceProductFind**
> \Api2Cart\Client\Model\InlineResponse20051 marketplaceProductFind($store_id, $count, $page_cursor, $categories_ids, $keyword, $asin, $ean, $gtin, $upc, $mpn, $isbn, $params, $exclude, $response_fields)



Search product in global catalog.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\MarketplaceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$store_id = "store_id_example"; // string | Store Id
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$categories_ids = "categories_ids_example"; // string | Defines product add that is specified by comma-separated categories id
$keyword = "keyword_example"; // string | Defines search keyword
$asin = "asin_example"; // string | Amazon Standard Identification Number.
$ean = "ean_example"; // string | European Article Number. An EAN is a unique 8 or 13-digit identifier that many industries (such as book publishers) use to identify products.
$gtin = "gtin_example"; // string | Global Trade Item Number. An GTIN is an identifier for trade items.
$upc = "upc_example"; // string | Universal Product Code. A UPC (UPC-A) is a commonly used identifer for many different products.
$mpn = "mpn_example"; // string | Manufacturer Part Number. A MPN is an identifier of a particular part design or material used.
$isbn = "isbn_example"; // string | International Standard Book Number. An ISBN is a unique identifier for books.
$params = "force_all"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve

try {
    $result = $apiInstance->marketplaceProductFind($store_id, $count, $page_cursor, $categories_ids, $keyword, $asin, $ean, $gtin, $upc, $mpn, $isbn, $params, $exclude, $response_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MarketplaceApi->marketplaceProductFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_id** | **string**| Store Id | [optional]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **categories_ids** | **string**| Defines product add that is specified by comma-separated categories id | [optional]
 **keyword** | **string**| Defines search keyword | [optional]
 **asin** | **string**| Amazon Standard Identification Number. | [optional]
 **ean** | **string**| European Article Number. An EAN is a unique 8 or 13-digit identifier that many industries (such as book publishers) use to identify products. | [optional]
 **gtin** | **string**| Global Trade Item Number. An GTIN is an identifier for trade items. | [optional]
 **upc** | **string**| Universal Product Code. A UPC (UPC-A) is a commonly used identifer for many different products. | [optional]
 **mpn** | **string**| Manufacturer Part Number. A MPN is an identifier of a particular part design or material used. | [optional]
 **isbn** | **string**| International Standard Book Number. An ISBN is a unique identifier for books. | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to force_all]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20051**](../Model/InlineResponse20051.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

