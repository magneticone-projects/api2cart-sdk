# Api2Cart\Client\WebhookApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**webhookCount**](WebhookApi.md#webhookCount) | **GET** /webhook.count.json | 
[**webhookCreate**](WebhookApi.md#webhookCreate) | **POST** /webhook.create.json | 
[**webhookDelete**](WebhookApi.md#webhookDelete) | **DELETE** /webhook.delete.json | 
[**webhookEvents**](WebhookApi.md#webhookEvents) | **GET** /webhook.events.json | 
[**webhookList**](WebhookApi.md#webhookList) | **GET** /webhook.list.json | 
[**webhookUpdate**](WebhookApi.md#webhookUpdate) | **PUT** /webhook.update.json | 


# **webhookCount**
> \Api2Cart\Client\Model\InlineResponse20096 webhookCount($entity, $action, $active)



Count registered webhooks on the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\WebhookApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$entity = "entity_example"; // string | The entity you want to filter webhooks by (e.g. order or product)
$action = "action_example"; // string | The action you want to filter webhooks by (e.g. order or product)
$active = true; // bool | The webhook status you want to filter webhooks by

try {
    $result = $apiInstance->webhookCount($entity, $action, $active);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->webhookCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity** | **string**| The entity you want to filter webhooks by (e.g. order or product) | [optional]
 **action** | **string**| The action you want to filter webhooks by (e.g. order or product) | [optional]
 **active** | **bool**| The webhook status you want to filter webhooks by | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20096**](../Model/InlineResponse20096.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **webhookCreate**
> \Api2Cart\Client\Model\InlineResponse20018 webhookCreate($entity, $action, $callback, $label, $fields, $active, $store_id)



Create webhook on the store and subscribe to it.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\WebhookApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$entity = "entity_example"; // string | Specify the entity that you want to enable webhooks for (e.g product, order, customer, category)
$action = "action_example"; // string | Specify what action (event) will trigger the webhook (e.g add, delete, or update)
$callback = "callback_example"; // string | Callback url that returns shipping rates. It should be able to accept POST requests with json data.
$label = "label_example"; // string | The name you give to the webhook
$fields = "force_all"; // string | Fields the webhook should send
$active = true; // bool | Webhook status
$store_id = "store_id_example"; // string | Defines store id where the webhook should be assigned

try {
    $result = $apiInstance->webhookCreate($entity, $action, $callback, $label, $fields, $active, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->webhookCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity** | **string**| Specify the entity that you want to enable webhooks for (e.g product, order, customer, category) |
 **action** | **string**| Specify what action (event) will trigger the webhook (e.g add, delete, or update) |
 **callback** | **string**| Callback url that returns shipping rates. It should be able to accept POST requests with json data. | [optional]
 **label** | **string**| The name you give to the webhook | [optional]
 **fields** | **string**| Fields the webhook should send | [optional] [default to force_all]
 **active** | **bool**| Webhook status | [optional] [default to true]
 **store_id** | **string**| Defines store id where the webhook should be assigned | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20018**](../Model/InlineResponse20018.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **webhookDelete**
> \Api2Cart\Client\Model\InlineResponse2009 webhookDelete($id)



Delete registered webhook on the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\WebhookApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Webhook id

try {
    $result = $apiInstance->webhookDelete($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->webhookDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Webhook id |

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **webhookEvents**
> \Api2Cart\Client\Model\InlineResponse20098 webhookEvents()



List all Webhooks that are available on this store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\WebhookApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->webhookEvents();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->webhookEvents: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Api2Cart\Client\Model\InlineResponse20098**](../Model/InlineResponse20098.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **webhookList**
> \Api2Cart\Client\Model\InlineResponse20097 webhookList($params, $start, $count, $entity, $action, $active, $ids)



List registered webhook on the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\WebhookApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$params = "id,entity,action,callback"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$entity = "entity_example"; // string | The entity you want to filter webhooks by (e.g. order or product)
$action = "action_example"; // string | The action you want to filter webhooks by (e.g. add, update, or delete)
$active = true; // bool | The webhook status you want to filter webhooks by
$ids = "ids_example"; // string | List of сomma-separated webhook ids

try {
    $result = $apiInstance->webhookList($params, $start, $count, $entity, $action, $active, $ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->webhookList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,entity,action,callback]
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **entity** | **string**| The entity you want to filter webhooks by (e.g. order or product) | [optional]
 **action** | **string**| The action you want to filter webhooks by (e.g. add, update, or delete) | [optional]
 **active** | **bool**| The webhook status you want to filter webhooks by | [optional]
 **ids** | **string**| List of сomma-separated webhook ids | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20097**](../Model/InlineResponse20097.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **webhookUpdate**
> \Api2Cart\Client\Model\InlineResponse20077 webhookUpdate($id, $callback, $label, $fields, $active)



Update Webhooks parameters.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\WebhookApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Webhook id
$callback = "callback_example"; // string | Callback url that returns shipping rates. It should be able to accept POST requests with json data.
$label = "label_example"; // string | The name you give to the webhook
$fields = "fields_example"; // string | Fields the webhook should send
$active = true; // bool | Webhook status

try {
    $result = $apiInstance->webhookUpdate($id, $callback, $label, $fields, $active);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->webhookUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Webhook id |
 **callback** | **string**| Callback url that returns shipping rates. It should be able to accept POST requests with json data. | [optional]
 **label** | **string**| The name you give to the webhook | [optional]
 **fields** | **string**| Fields the webhook should send | [optional]
 **active** | **bool**| Webhook status | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20077**](../Model/InlineResponse20077.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

