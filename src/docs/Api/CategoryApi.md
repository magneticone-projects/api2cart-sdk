# Api2Cart\Client\CategoryApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**categoryAdd**](CategoryApi.md#categoryAdd) | **POST** /category.add.json | 
[**categoryAddBatch**](CategoryApi.md#categoryAddBatch) | **POST** /category.add.batch.json | 
[**categoryAssign**](CategoryApi.md#categoryAssign) | **POST** /category.assign.json | 
[**categoryCount**](CategoryApi.md#categoryCount) | **GET** /category.count.json | 
[**categoryDelete**](CategoryApi.md#categoryDelete) | **DELETE** /category.delete.json | 
[**categoryFind**](CategoryApi.md#categoryFind) | **GET** /category.find.json | 
[**categoryImageAdd**](CategoryApi.md#categoryImageAdd) | **POST** /category.image.add.json | 
[**categoryImageDelete**](CategoryApi.md#categoryImageDelete) | **DELETE** /category.image.delete.json | 
[**categoryInfo**](CategoryApi.md#categoryInfo) | **GET** /category.info.json | 
[**categoryList**](CategoryApi.md#categoryList) | **GET** /category.list.json | 
[**categoryUnassign**](CategoryApi.md#categoryUnassign) | **POST** /category.unassign.json | 
[**categoryUpdate**](CategoryApi.md#categoryUpdate) | **PUT** /category.update.json | 


# **categoryAdd**
> \Api2Cart\Client\Model\InlineResponse20041 categoryAdd($name, $parent_id, $stores_ids, $store_id, $lang_id, $avail, $sort_order, $created_time, $modified_time, $description, $short_description, $meta_title, $meta_description, $meta_keywords, $seo_url)



Add new category in store

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$name = "name_example"; // string | Defines category's name that has to be added
$parent_id = "parent_id_example"; // string | Adds categories specified by parent id
$stores_ids = "stores_ids_example"; // string | Create category in the stores that is specified by comma-separated stores' id
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id
$avail = true; // bool | Defines category's visibility status
$sort_order = 0; // int | Sort number in the list
$created_time = "created_time_example"; // string | Entity's date creation
$modified_time = "modified_time_example"; // string | Entity's date modification
$description = "description_example"; // string | Defines category's description
$short_description = "short_description_example"; // string | Defines short description
$meta_title = "meta_title_example"; // string | Defines unique meta title for each entity
$meta_description = "meta_description_example"; // string | Defines unique meta description of a entity
$meta_keywords = "meta_keywords_example"; // string | Defines unique meta keywords for each entity
$seo_url = "seo_url_example"; // string | Defines unique category's URL for SEO

try {
    $result = $apiInstance->categoryAdd($name, $parent_id, $stores_ids, $store_id, $lang_id, $avail, $sort_order, $created_time, $modified_time, $description, $short_description, $meta_title, $meta_description, $meta_keywords, $seo_url);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**| Defines category&#39;s name that has to be added |
 **parent_id** | **string**| Adds categories specified by parent id | [optional]
 **stores_ids** | **string**| Create category in the stores that is specified by comma-separated stores&#39; id | [optional]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]
 **avail** | **bool**| Defines category&#39;s visibility status | [optional] [default to true]
 **sort_order** | **int**| Sort number in the list | [optional] [default to 0]
 **created_time** | **string**| Entity&#39;s date creation | [optional]
 **modified_time** | **string**| Entity&#39;s date modification | [optional]
 **description** | **string**| Defines category&#39;s description | [optional]
 **short_description** | **string**| Defines short description | [optional]
 **meta_title** | **string**| Defines unique meta title for each entity | [optional]
 **meta_description** | **string**| Defines unique meta description of a entity | [optional]
 **meta_keywords** | **string**| Defines unique meta keywords for each entity | [optional]
 **seo_url** | **string**| Defines unique category&#39;s URL for SEO | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20041**](../Model/InlineResponse20041.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoryAddBatch**
> \Api2Cart\Client\Model\InlineResponse20042 categoryAddBatch($body)



Add new categories to the store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Api2Cart\Client\Model\CategoryAddBatch(); // \Api2Cart\Client\Model\CategoryAddBatch | 

try {
    $result = $apiInstance->categoryAddBatch($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryAddBatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Api2Cart\Client\Model\CategoryAddBatch**](../Model/CategoryAddBatch.md)|  |

### Return type

[**\Api2Cart\Client\Model\InlineResponse20042**](../Model/InlineResponse20042.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoryAssign**
> \Api2Cart\Client\Model\InlineResponse20030 categoryAssign($product_id, $category_id, $store_id)



Assign category to product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$product_id = "product_id_example"; // string | Defines category assign to the product, specified by product id
$category_id = "category_id_example"; // string | Defines category assign, specified by category id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->categoryAssign($product_id, $category_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryAssign: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **string**| Defines category assign to the product, specified by product id |
 **category_id** | **string**| Defines category assign, specified by category id |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20030**](../Model/InlineResponse20030.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoryCount**
> \Api2Cart\Client\Model\InlineResponse20039 categoryCount($parent_id, $store_id, $lang_id, $created_from, $created_to, $modified_from, $modified_to, $avail, $product_type, $find_value, $find_where, $report_request_id, $disable_report_cache)



Count categories in store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$parent_id = "parent_id_example"; // string | Counts categories specified by parent id
$store_id = "store_id_example"; // string | Counts category specified by store id
$lang_id = "lang_id_example"; // string | Counts category specified by language id
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$avail = true; // bool | Defines category's visibility status
$product_type = "product_type_example"; // string | A categorization for the product
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "find_where_example"; // string | Counts categories that are searched specified by field
$report_request_id = "report_request_id_example"; // string | Report request id
$disable_report_cache = false; // bool | Disable report cache for current request

try {
    $result = $apiInstance->categoryCount($parent_id, $store_id, $lang_id, $created_from, $created_to, $modified_from, $modified_to, $avail, $product_type, $find_value, $find_where, $report_request_id, $disable_report_cache);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parent_id** | **string**| Counts categories specified by parent id | [optional]
 **store_id** | **string**| Counts category specified by store id | [optional]
 **lang_id** | **string**| Counts category specified by language id | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **avail** | **bool**| Defines category&#39;s visibility status | [optional] [default to true]
 **product_type** | **string**| A categorization for the product | [optional]
 **find_value** | **string**| Entity search that is specified by some value | [optional]
 **find_where** | **string**| Counts categories that are searched specified by field | [optional]
 **report_request_id** | **string**| Report request id | [optional]
 **disable_report_cache** | **bool**| Disable report cache for current request | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20039**](../Model/InlineResponse20039.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoryDelete**
> \Api2Cart\Client\Model\InlineResponse20014 categoryDelete($id, $store_id)



Delete category in store

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Defines category removal, specified by category id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->categoryDelete($id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Defines category removal, specified by category id |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20014**](../Model/InlineResponse20014.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoryFind**
> \Api2Cart\Client\Model\InlineResponse20040 categoryFind($find_value, $find_where, $find_params, $store_id, $lang_id)



Search category in store. \"Laptop\" is specified here by default.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "name"; // string | Entity search that is specified by the comma-separated unique fields
$find_params = "whole_words"; // string | Entity search that is specified by comma-separated parameters
$store_id = "store_id_example"; // string | Store Id
$lang_id = "lang_id_example"; // string | Language id

try {
    $result = $apiInstance->categoryFind($find_value, $find_where, $find_params, $store_id, $lang_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryFind: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **find_value** | **string**| Entity search that is specified by some value |
 **find_where** | **string**| Entity search that is specified by the comma-separated unique fields | [optional] [default to name]
 **find_params** | **string**| Entity search that is specified by comma-separated parameters | [optional] [default to whole_words]
 **store_id** | **string**| Store Id | [optional]
 **lang_id** | **string**| Language id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20040**](../Model/InlineResponse20040.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoryImageAdd**
> \Api2Cart\Client\Model\InlineResponse20043 categoryImageAdd($category_id, $image_name, $url, $type, $label, $mime, $position, $store_id)



Add image to category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$category_id = "category_id_example"; // string | Defines category id where the image should be added
$image_name = "image_name_example"; // string | Defines image's name
$url = "url_example"; // string | Defines URL of the image that has to be added
$type = "type_example"; // string | Defines image's types that are specified by comma-separated list
$label = "label_example"; // string | Defines alternative text that has to be attached to the picture
$mime = "mime_example"; // string | Mime type of image http://en.wikipedia.org/wiki/Internet_media_type.
$position = 0; // int | Defines image’s position in the list
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->categoryImageAdd($category_id, $image_name, $url, $type, $label, $mime, $position, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryImageAdd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_id** | **string**| Defines category id where the image should be added |
 **image_name** | **string**| Defines image&#39;s name |
 **url** | **string**| Defines URL of the image that has to be added |
 **type** | **string**| Defines image&#39;s types that are specified by comma-separated list |
 **label** | **string**| Defines alternative text that has to be attached to the picture | [optional]
 **mime** | **string**| Mime type of image http://en.wikipedia.org/wiki/Internet_media_type. | [optional]
 **position** | **int**| Defines image’s position in the list | [optional] [default to 0]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20043**](../Model/InlineResponse20043.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoryImageDelete**
> \Api2Cart\Client\Model\InlineResponse2009 categoryImageDelete($category_id, $image_id, $store_id)



Delete image

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$category_id = "category_id_example"; // string | Defines category id where the image should be deleted
$image_id = "image_id_example"; // string | Define image id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->categoryImageDelete($category_id, $image_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryImageDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_id** | **string**| Defines category id where the image should be deleted |
 **image_id** | **string**| Define image id |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoryInfo**
> \Api2Cart\Client\Model\InlineResponse20038 categoryInfo($id, $params, $response_fields, $exclude, $store_id, $lang_id, $schema_type, $report_request_id, $disable_report_cache)



Get category info about category ID*** or specify other category ID.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Retrieves category's info specified by category id
$params = "id,parent_id,name,description"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$store_id = "store_id_example"; // string | Retrieves category info  specified by store id
$lang_id = "lang_id_example"; // string | Retrieves category info  specified by language id
$schema_type = "schema_type_example"; // string | The name of the requirements set for the provided schema.
$report_request_id = "report_request_id_example"; // string | Report request id
$disable_report_cache = false; // bool | Disable report cache for current request

try {
    $result = $apiInstance->categoryInfo($id, $params, $response_fields, $exclude, $store_id, $lang_id, $schema_type, $report_request_id, $disable_report_cache);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Retrieves category&#39;s info specified by category id |
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,parent_id,name,description]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **store_id** | **string**| Retrieves category info  specified by store id | [optional]
 **lang_id** | **string**| Retrieves category info  specified by language id | [optional]
 **schema_type** | **string**| The name of the requirements set for the provided schema. | [optional]
 **report_request_id** | **string**| Report request id | [optional]
 **disable_report_cache** | **bool**| Disable report cache for current request | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20038**](../Model/InlineResponse20038.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoryList**
> \Api2Cart\Client\Model\ModelResponseCategoryList categoryList($start, $count, $page_cursor, $parent_id, $params, $response_fields, $exclude, $store_id, $lang_id, $created_from, $created_to, $modified_from, $modified_to, $avail, $product_type, $find_value, $find_where, $report_request_id, $disable_report_cache, $disable_cache)



Get list of categories from store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$parent_id = "parent_id_example"; // string | Retrieves categories specified by parent id
$params = "id,parent_id,name,description"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$store_id = "store_id_example"; // string | Retrieves categories specified by store id
$lang_id = "lang_id_example"; // string | Retrieves categorys specified by language id
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$avail = true; // bool | Defines category's visibility status
$product_type = "product_type_example"; // string | A categorization for the product
$find_value = "find_value_example"; // string | Entity search that is specified by some value
$find_where = "find_where_example"; // string | Category search that is specified by field
$report_request_id = "report_request_id_example"; // string | Report request id
$disable_report_cache = false; // bool | Disable report cache for current request
$disable_cache = false; // bool | Disable cache for current request

try {
    $result = $apiInstance->categoryList($start, $count, $page_cursor, $parent_id, $params, $response_fields, $exclude, $store_id, $lang_id, $created_from, $created_to, $modified_from, $modified_to, $avail, $product_type, $find_value, $find_where, $report_request_id, $disable_report_cache, $disable_cache);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **parent_id** | **string**| Retrieves categories specified by parent id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,parent_id,name,description]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **store_id** | **string**| Retrieves categories specified by store id | [optional]
 **lang_id** | **string**| Retrieves categorys specified by language id | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **avail** | **bool**| Defines category&#39;s visibility status | [optional] [default to true]
 **product_type** | **string**| A categorization for the product | [optional]
 **find_value** | **string**| Entity search that is specified by some value | [optional]
 **find_where** | **string**| Category search that is specified by field | [optional]
 **report_request_id** | **string**| Report request id | [optional]
 **disable_report_cache** | **bool**| Disable report cache for current request | [optional] [default to false]
 **disable_cache** | **bool**| Disable cache for current request | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\ModelResponseCategoryList**](../Model/ModelResponseCategoryList.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoryUnassign**
> \Api2Cart\Client\Model\InlineResponse20030 categoryUnassign($category_id, $product_id, $store_id)



Unassign category to product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$category_id = "category_id_example"; // string | Defines category unassign, specified by category id
$product_id = "product_id_example"; // string | Defines category unassign to the product, specified by product id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->categoryUnassign($category_id, $product_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryUnassign: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_id** | **string**| Defines category unassign, specified by category id |
 **product_id** | **string**| Defines category unassign to the product, specified by product id |
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20030**](../Model/InlineResponse20030.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **categoryUpdate**
> \Api2Cart\Client\Model\InlineResponse2004 categoryUpdate($id, $name, $parent_id, $stores_ids, $avail, $sort_order, $modified_time, $description, $short_description, $meta_title, $meta_description, $meta_keywords, $seo_url, $lang_id, $store_id)



Update category in store

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\CategoryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Defines category update specified by category id
$name = "name_example"; // string | Defines new category’s name
$parent_id = "parent_id_example"; // string | Defines new parent category id
$stores_ids = "stores_ids_example"; // string | Update category in the stores that is specified by comma-separated stores' id
$avail = true; // bool | Defines category's visibility status
$sort_order = 56; // int | Sort number in the list
$modified_time = "modified_time_example"; // string | Entity's date modification
$description = "description_example"; // string | Defines new category's description
$short_description = "short_description_example"; // string | Defines short description
$meta_title = "meta_title_example"; // string | Defines unique meta title for each entity
$meta_description = "meta_description_example"; // string | Defines unique meta description of a entity
$meta_keywords = "meta_keywords_example"; // string | Defines unique meta keywords for each entity
$seo_url = "seo_url_example"; // string | Defines unique category's URL for SEO
$lang_id = "lang_id_example"; // string | Language id
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->categoryUpdate($id, $name, $parent_id, $stores_ids, $avail, $sort_order, $modified_time, $description, $short_description, $meta_title, $meta_description, $meta_keywords, $seo_url, $lang_id, $store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoryApi->categoryUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Defines category update specified by category id |
 **name** | **string**| Defines new category’s name | [optional]
 **parent_id** | **string**| Defines new parent category id | [optional]
 **stores_ids** | **string**| Update category in the stores that is specified by comma-separated stores&#39; id | [optional]
 **avail** | **bool**| Defines category&#39;s visibility status | [optional]
 **sort_order** | **int**| Sort number in the list | [optional]
 **modified_time** | **string**| Entity&#39;s date modification | [optional]
 **description** | **string**| Defines new category&#39;s description | [optional]
 **short_description** | **string**| Defines short description | [optional]
 **meta_title** | **string**| Defines unique meta title for each entity | [optional]
 **meta_description** | **string**| Defines unique meta description of a entity | [optional]
 **meta_keywords** | **string**| Defines unique meta keywords for each entity | [optional]
 **seo_url** | **string**| Defines unique category&#39;s URL for SEO | [optional]
 **lang_id** | **string**| Language id | [optional]
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

