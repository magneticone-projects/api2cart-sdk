# Api2Cart\Client\ReturnApi

All URIs are relative to *https://api.api2cart.com/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**returnActionList**](ReturnApi.md#returnActionList) | **GET** /return.action.list.json | 
[**returnCount**](ReturnApi.md#returnCount) | **GET** /return.count.json | 
[**returnInfo**](ReturnApi.md#returnInfo) | **GET** /return.info.json | 
[**returnList**](ReturnApi.md#returnList) | **GET** /return.list.json | 
[**returnReasonList**](ReturnApi.md#returnReasonList) | **GET** /return.reason.list.json | 
[**returnStatusList**](ReturnApi.md#returnStatusList) | **GET** /return.status.list.json | 


# **returnActionList**
> \Api2Cart\Client\Model\InlineResponse20092 returnActionList()



Retrieve list of return actions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ReturnApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->returnActionList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReturnApi->returnActionList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Api2Cart\Client\Model\InlineResponse20092**](../Model/InlineResponse20092.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **returnCount**
> \Api2Cart\Client\Model\InlineResponse20090 returnCount($order_ids, $customer_id, $store_id, $status, $return_type, $created_from, $created_to, $modified_from, $modified_to, $report_request_id, $disable_report_cache)



Count returns in store

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ReturnApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$order_ids = "order_ids_example"; // string | Counts return requests specified by order ids
$customer_id = "customer_id_example"; // string | Counts return requests quantity specified by customer id
$store_id = "store_id_example"; // string | Store Id
$status = "status_example"; // string | Defines status
$return_type = "return_type_example"; // string | Retrieves returns specified by return type
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$report_request_id = "report_request_id_example"; // string | Report request id
$disable_report_cache = false; // bool | Disable report cache for current request

try {
    $result = $apiInstance->returnCount($order_ids, $customer_id, $store_id, $status, $return_type, $created_from, $created_to, $modified_from, $modified_to, $report_request_id, $disable_report_cache);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReturnApi->returnCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_ids** | **string**| Counts return requests specified by order ids | [optional]
 **customer_id** | **string**| Counts return requests quantity specified by customer id | [optional]
 **store_id** | **string**| Store Id | [optional]
 **status** | **string**| Defines status | [optional]
 **return_type** | **string**| Retrieves returns specified by return type | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **report_request_id** | **string**| Report request id | [optional]
 **disable_report_cache** | **bool**| Disable report cache for current request | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20090**](../Model/InlineResponse20090.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **returnInfo**
> \Api2Cart\Client\Model\InlineResponse20089 returnInfo($id, $order_id, $store_id, $params, $exclude, $response_fields)



Retrieve return information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ReturnApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | Entity id
$order_id = "order_id_example"; // string | Defines the order id
$store_id = "store_id_example"; // string | Store Id
$params = "id,order_products"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve

try {
    $result = $apiInstance->returnInfo($id, $order_id, $store_id, $params, $exclude, $response_fields);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReturnApi->returnInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Entity id |
 **order_id** | **string**| Defines the order id | [optional]
 **store_id** | **string**| Store Id | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,order_products]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20089**](../Model/InlineResponse20089.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **returnList**
> \Api2Cart\Client\Model\InlineResponse20091 returnList($start, $count, $page_cursor, $params, $exclude, $response_fields, $order_id, $order_ids, $customer_id, $store_id, $status, $return_type, $created_from, $created_to, $modified_from, $modified_to, $report_request_id, $disable_report_cache)



Get list of return requests from store.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ReturnApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start = 0; // int | This parameter sets the number from which you want to get entities
$count = 10; // int | This parameter sets the entity amount that has to be retrieved. Max allowed count=250
$page_cursor = "page_cursor_example"; // string | Used to retrieve entities via cursor-based pagination (it can't be used with any other filtering parameter)
$params = "id,order_products"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$exclude = "exclude_example"; // string | Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter `params` equal force_all
$response_fields = "response_fields_example"; // string | Set this parameter in order to choose which entity fields you want to retrieve
$order_id = "order_id_example"; // string | Defines the order id
$order_ids = "order_ids_example"; // string | Retrieves return requests specified by order ids
$customer_id = "customer_id_example"; // string | Retrieves return requests specified by customer id
$store_id = "store_id_example"; // string | Store Id
$status = "status_example"; // string | Defines status
$return_type = "return_type_example"; // string | Retrieves returns specified by return type
$created_from = "created_from_example"; // string | Retrieve entities from their creation date
$created_to = "created_to_example"; // string | Retrieve entities to their creation date
$modified_from = "modified_from_example"; // string | Retrieve entities from their modification date
$modified_to = "modified_to_example"; // string | Retrieve entities to their modification date
$report_request_id = "report_request_id_example"; // string | Report request id
$disable_report_cache = false; // bool | Disable report cache for current request

try {
    $result = $apiInstance->returnList($start, $count, $page_cursor, $params, $exclude, $response_fields, $order_id, $order_ids, $customer_id, $store_id, $status, $return_type, $created_from, $created_to, $modified_from, $modified_to, $report_request_id, $disable_report_cache);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReturnApi->returnList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| This parameter sets the number from which you want to get entities | [optional] [default to 0]
 **count** | **int**| This parameter sets the entity amount that has to be retrieved. Max allowed count&#x3D;250 | [optional] [default to 10]
 **page_cursor** | **string**| Used to retrieve entities via cursor-based pagination (it can&#39;t be used with any other filtering parameter) | [optional]
 **params** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional] [default to id,order_products]
 **exclude** | **string**| Set this parameter in order to choose which entity fields you want to ignore. Works only if parameter &#x60;params&#x60; equal force_all | [optional]
 **response_fields** | **string**| Set this parameter in order to choose which entity fields you want to retrieve | [optional]
 **order_id** | **string**| Defines the order id | [optional]
 **order_ids** | **string**| Retrieves return requests specified by order ids | [optional]
 **customer_id** | **string**| Retrieves return requests specified by customer id | [optional]
 **store_id** | **string**| Store Id | [optional]
 **status** | **string**| Defines status | [optional]
 **return_type** | **string**| Retrieves returns specified by return type | [optional]
 **created_from** | **string**| Retrieve entities from their creation date | [optional]
 **created_to** | **string**| Retrieve entities to their creation date | [optional]
 **modified_from** | **string**| Retrieve entities from their modification date | [optional]
 **modified_to** | **string**| Retrieve entities to their modification date | [optional]
 **report_request_id** | **string**| Report request id | [optional]
 **disable_report_cache** | **bool**| Disable report cache for current request | [optional] [default to false]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20091**](../Model/InlineResponse20091.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **returnReasonList**
> \Api2Cart\Client\Model\InlineResponse20093 returnReasonList($store_id)



Retrieve list of return reasons

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ReturnApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$store_id = "store_id_example"; // string | Store Id

try {
    $result = $apiInstance->returnReasonList($store_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReturnApi->returnReasonList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **store_id** | **string**| Store Id | [optional]

### Return type

[**\Api2Cart\Client\Model\InlineResponse20093**](../Model/InlineResponse20093.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **returnStatusList**
> \Api2Cart\Client\Model\InlineResponse20094 returnStatusList()



Retrieve list of statuses

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-api-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-api-key', 'Bearer');
// Configure API key authorization: store_key
$config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKey('x-store-key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Api2Cart\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('x-store-key', 'Bearer');

$apiInstance = new Api2Cart\Client\Api\ReturnApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->returnStatusList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReturnApi->returnStatusList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Api2Cart\Client\Model\InlineResponse20094**](../Model/InlineResponse20094.md)

### Authorization

[api_key](../../README.md#api_key), [store_key](../../README.md#store_key)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

