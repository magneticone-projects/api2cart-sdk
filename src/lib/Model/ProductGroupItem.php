<?php
/**
 * ProductGroupItem
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Api2Cart\Client\Model;

use \ArrayAccess;
use \Api2Cart\Client\ObjectSerializer;

/**
 * ProductGroupItem Class Doc Comment
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ProductGroupItem implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'Product_GroupItem';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'child_item_id' => 'string',
        'product_id' => 'string',
        'default_qty_in_pack' => 'string',
        'is_qty_in_pack_fixed' => 'bool',
        'price' => 'float',
        'additional_fields' => 'object',
        'custom_fields' => 'object'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'child_item_id' => null,
        'product_id' => null,
        'default_qty_in_pack' => null,
        'is_qty_in_pack_fixed' => null,
        'price' => null,
        'additional_fields' => null,
        'custom_fields' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'child_item_id' => 'child_item_id',
        'product_id' => 'product_id',
        'default_qty_in_pack' => 'default_qty_in_pack',
        'is_qty_in_pack_fixed' => 'is_qty_in_pack_fixed',
        'price' => 'price',
        'additional_fields' => 'additional_fields',
        'custom_fields' => 'custom_fields'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'child_item_id' => 'setChildItemId',
        'product_id' => 'setProductId',
        'default_qty_in_pack' => 'setDefaultQtyInPack',
        'is_qty_in_pack_fixed' => 'setIsQtyInPackFixed',
        'price' => 'setPrice',
        'additional_fields' => 'setAdditionalFields',
        'custom_fields' => 'setCustomFields'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'child_item_id' => 'getChildItemId',
        'product_id' => 'getProductId',
        'default_qty_in_pack' => 'getDefaultQtyInPack',
        'is_qty_in_pack_fixed' => 'getIsQtyInPackFixed',
        'price' => 'getPrice',
        'additional_fields' => 'getAdditionalFields',
        'custom_fields' => 'getCustomFields'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['child_item_id'] = isset($data['child_item_id']) ? $data['child_item_id'] : null;
        $this->container['product_id'] = isset($data['product_id']) ? $data['product_id'] : null;
        $this->container['default_qty_in_pack'] = isset($data['default_qty_in_pack']) ? $data['default_qty_in_pack'] : null;
        $this->container['is_qty_in_pack_fixed'] = isset($data['is_qty_in_pack_fixed']) ? $data['is_qty_in_pack_fixed'] : null;
        $this->container['price'] = isset($data['price']) ? $data['price'] : null;
        $this->container['additional_fields'] = isset($data['additional_fields']) ? $data['additional_fields'] : null;
        $this->container['custom_fields'] = isset($data['custom_fields']) ? $data['custom_fields'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets child_item_id
     *
     * @return string
     */
    public function getChildItemId()
    {
        return $this->container['child_item_id'];
    }

    /**
     * Sets child_item_id
     *
     * @param string $child_item_id child_item_id
     *
     * @return $this
     */
    public function setChildItemId($child_item_id)
    {
        $this->container['child_item_id'] = $child_item_id;

        return $this;
    }

    /**
     * Gets product_id
     *
     * @return string
     */
    public function getProductId()
    {
        return $this->container['product_id'];
    }

    /**
     * Sets product_id
     *
     * @param string $product_id product_id
     *
     * @return $this
     */
    public function setProductId($product_id)
    {
        $this->container['product_id'] = $product_id;

        return $this;
    }

    /**
     * Gets default_qty_in_pack
     *
     * @return string
     */
    public function getDefaultQtyInPack()
    {
        return $this->container['default_qty_in_pack'];
    }

    /**
     * Sets default_qty_in_pack
     *
     * @param string $default_qty_in_pack default_qty_in_pack
     *
     * @return $this
     */
    public function setDefaultQtyInPack($default_qty_in_pack)
    {
        $this->container['default_qty_in_pack'] = $default_qty_in_pack;

        return $this;
    }

    /**
     * Gets is_qty_in_pack_fixed
     *
     * @return bool
     */
    public function getIsQtyInPackFixed()
    {
        return $this->container['is_qty_in_pack_fixed'];
    }

    /**
     * Sets is_qty_in_pack_fixed
     *
     * @param bool $is_qty_in_pack_fixed is_qty_in_pack_fixed
     *
     * @return $this
     */
    public function setIsQtyInPackFixed($is_qty_in_pack_fixed)
    {
        $this->container['is_qty_in_pack_fixed'] = $is_qty_in_pack_fixed;

        return $this;
    }

    /**
     * Gets price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->container['price'];
    }

    /**
     * Sets price
     *
     * @param float $price price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->container['price'] = $price;

        return $this;
    }

    /**
     * Gets additional_fields
     *
     * @return object
     */
    public function getAdditionalFields()
    {
        return $this->container['additional_fields'];
    }

    /**
     * Sets additional_fields
     *
     * @param object $additional_fields additional_fields
     *
     * @return $this
     */
    public function setAdditionalFields($additional_fields)
    {
        $this->container['additional_fields'] = $additional_fields;

        return $this;
    }

    /**
     * Gets custom_fields
     *
     * @return object
     */
    public function getCustomFields()
    {
        return $this->container['custom_fields'];
    }

    /**
     * Sets custom_fields
     *
     * @param object $custom_fields custom_fields
     *
     * @return $this
     */
    public function setCustomFields($custom_fields)
    {
        $this->container['custom_fields'] = $custom_fields;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


