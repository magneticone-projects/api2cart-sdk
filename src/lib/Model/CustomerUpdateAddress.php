<?php
/**
 * CustomerUpdateAddress
 *
 * PHP version 5
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Swagger API2Cart
 *
 * API2Cart
 *
 * OpenAPI spec version: 1.1
 * Contact: contact@api2cart.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.33
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Api2Cart\Client\Model;

use \ArrayAccess;
use \Api2Cart\Client\ObjectSerializer;

/**
 * CustomerUpdateAddress Class Doc Comment
 *
 * @category Class
 * @package  Api2Cart\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class CustomerUpdateAddress implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'CustomerUpdate_address';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'address_book_id' => 'string',
        'address_book_first_name' => 'string',
        'address_book_last_name' => 'string',
        'address_book_company' => 'string',
        'address_book_fax' => 'string',
        'address_book_phone' => 'string',
        'address_book_phone_mobile' => 'string',
        'address_book_address1' => 'string',
        'address_book_address2' => 'string',
        'address_book_city' => 'string',
        'address_book_country' => 'string',
        'address_book_state' => 'string',
        'address_book_postcode' => 'string',
        'address_book_tax_id' => 'string',
        'address_book_identification_number' => 'string',
        'address_book_gender' => 'string',
        'address_book_alias' => 'string',
        'address_book_type' => 'string',
        'address_book_default' => 'bool'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'address_book_id' => null,
        'address_book_first_name' => null,
        'address_book_last_name' => null,
        'address_book_company' => null,
        'address_book_fax' => null,
        'address_book_phone' => null,
        'address_book_phone_mobile' => null,
        'address_book_address1' => null,
        'address_book_address2' => null,
        'address_book_city' => null,
        'address_book_country' => null,
        'address_book_state' => null,
        'address_book_postcode' => null,
        'address_book_tax_id' => null,
        'address_book_identification_number' => null,
        'address_book_gender' => null,
        'address_book_alias' => null,
        'address_book_type' => null,
        'address_book_default' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'address_book_id' => 'address_book_id',
        'address_book_first_name' => 'address_book_first_name',
        'address_book_last_name' => 'address_book_last_name',
        'address_book_company' => 'address_book_company',
        'address_book_fax' => 'address_book_fax',
        'address_book_phone' => 'address_book_phone',
        'address_book_phone_mobile' => 'address_book_phone_mobile',
        'address_book_address1' => 'address_book_address1',
        'address_book_address2' => 'address_book_address2',
        'address_book_city' => 'address_book_city',
        'address_book_country' => 'address_book_country',
        'address_book_state' => 'address_book_state',
        'address_book_postcode' => 'address_book_postcode',
        'address_book_tax_id' => 'address_book_tax_id',
        'address_book_identification_number' => 'address_book_identification_number',
        'address_book_gender' => 'address_book_gender',
        'address_book_alias' => 'address_book_alias',
        'address_book_type' => 'address_book_type',
        'address_book_default' => 'address_book_default'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'address_book_id' => 'setAddressBookId',
        'address_book_first_name' => 'setAddressBookFirstName',
        'address_book_last_name' => 'setAddressBookLastName',
        'address_book_company' => 'setAddressBookCompany',
        'address_book_fax' => 'setAddressBookFax',
        'address_book_phone' => 'setAddressBookPhone',
        'address_book_phone_mobile' => 'setAddressBookPhoneMobile',
        'address_book_address1' => 'setAddressBookAddress1',
        'address_book_address2' => 'setAddressBookAddress2',
        'address_book_city' => 'setAddressBookCity',
        'address_book_country' => 'setAddressBookCountry',
        'address_book_state' => 'setAddressBookState',
        'address_book_postcode' => 'setAddressBookPostcode',
        'address_book_tax_id' => 'setAddressBookTaxId',
        'address_book_identification_number' => 'setAddressBookIdentificationNumber',
        'address_book_gender' => 'setAddressBookGender',
        'address_book_alias' => 'setAddressBookAlias',
        'address_book_type' => 'setAddressBookType',
        'address_book_default' => 'setAddressBookDefault'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'address_book_id' => 'getAddressBookId',
        'address_book_first_name' => 'getAddressBookFirstName',
        'address_book_last_name' => 'getAddressBookLastName',
        'address_book_company' => 'getAddressBookCompany',
        'address_book_fax' => 'getAddressBookFax',
        'address_book_phone' => 'getAddressBookPhone',
        'address_book_phone_mobile' => 'getAddressBookPhoneMobile',
        'address_book_address1' => 'getAddressBookAddress1',
        'address_book_address2' => 'getAddressBookAddress2',
        'address_book_city' => 'getAddressBookCity',
        'address_book_country' => 'getAddressBookCountry',
        'address_book_state' => 'getAddressBookState',
        'address_book_postcode' => 'getAddressBookPostcode',
        'address_book_tax_id' => 'getAddressBookTaxId',
        'address_book_identification_number' => 'getAddressBookIdentificationNumber',
        'address_book_gender' => 'getAddressBookGender',
        'address_book_alias' => 'getAddressBookAlias',
        'address_book_type' => 'getAddressBookType',
        'address_book_default' => 'getAddressBookDefault'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['address_book_id'] = isset($data['address_book_id']) ? $data['address_book_id'] : null;
        $this->container['address_book_first_name'] = isset($data['address_book_first_name']) ? $data['address_book_first_name'] : null;
        $this->container['address_book_last_name'] = isset($data['address_book_last_name']) ? $data['address_book_last_name'] : null;
        $this->container['address_book_company'] = isset($data['address_book_company']) ? $data['address_book_company'] : null;
        $this->container['address_book_fax'] = isset($data['address_book_fax']) ? $data['address_book_fax'] : null;
        $this->container['address_book_phone'] = isset($data['address_book_phone']) ? $data['address_book_phone'] : null;
        $this->container['address_book_phone_mobile'] = isset($data['address_book_phone_mobile']) ? $data['address_book_phone_mobile'] : null;
        $this->container['address_book_address1'] = isset($data['address_book_address1']) ? $data['address_book_address1'] : null;
        $this->container['address_book_address2'] = isset($data['address_book_address2']) ? $data['address_book_address2'] : null;
        $this->container['address_book_city'] = isset($data['address_book_city']) ? $data['address_book_city'] : null;
        $this->container['address_book_country'] = isset($data['address_book_country']) ? $data['address_book_country'] : null;
        $this->container['address_book_state'] = isset($data['address_book_state']) ? $data['address_book_state'] : null;
        $this->container['address_book_postcode'] = isset($data['address_book_postcode']) ? $data['address_book_postcode'] : null;
        $this->container['address_book_tax_id'] = isset($data['address_book_tax_id']) ? $data['address_book_tax_id'] : null;
        $this->container['address_book_identification_number'] = isset($data['address_book_identification_number']) ? $data['address_book_identification_number'] : null;
        $this->container['address_book_gender'] = isset($data['address_book_gender']) ? $data['address_book_gender'] : null;
        $this->container['address_book_alias'] = isset($data['address_book_alias']) ? $data['address_book_alias'] : null;
        $this->container['address_book_type'] = isset($data['address_book_type']) ? $data['address_book_type'] : null;
        $this->container['address_book_default'] = isset($data['address_book_default']) ? $data['address_book_default'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets address_book_id
     *
     * @return string
     */
    public function getAddressBookId()
    {
        return $this->container['address_book_id'];
    }

    /**
     * Sets address_book_id
     *
     * @param string $address_book_id The ID of the address.
     *
     * @return $this
     */
    public function setAddressBookId($address_book_id)
    {
        $this->container['address_book_id'] = $address_book_id;

        return $this;
    }

    /**
     * Gets address_book_first_name
     *
     * @return string
     */
    public function getAddressBookFirstName()
    {
        return $this->container['address_book_first_name'];
    }

    /**
     * Sets address_book_first_name
     *
     * @param string $address_book_first_name Specifies customer's first name in the address book
     *
     * @return $this
     */
    public function setAddressBookFirstName($address_book_first_name)
    {
        $this->container['address_book_first_name'] = $address_book_first_name;

        return $this;
    }

    /**
     * Gets address_book_last_name
     *
     * @return string
     */
    public function getAddressBookLastName()
    {
        return $this->container['address_book_last_name'];
    }

    /**
     * Sets address_book_last_name
     *
     * @param string $address_book_last_name Specifies customer's last name in the address book
     *
     * @return $this
     */
    public function setAddressBookLastName($address_book_last_name)
    {
        $this->container['address_book_last_name'] = $address_book_last_name;

        return $this;
    }

    /**
     * Gets address_book_company
     *
     * @return string
     */
    public function getAddressBookCompany()
    {
        return $this->container['address_book_company'];
    }

    /**
     * Sets address_book_company
     *
     * @param string $address_book_company Specifies customer's company name in the address book
     *
     * @return $this
     */
    public function setAddressBookCompany($address_book_company)
    {
        $this->container['address_book_company'] = $address_book_company;

        return $this;
    }

    /**
     * Gets address_book_fax
     *
     * @return string
     */
    public function getAddressBookFax()
    {
        return $this->container['address_book_fax'];
    }

    /**
     * Sets address_book_fax
     *
     * @param string $address_book_fax Specifies customer's fax in the address book
     *
     * @return $this
     */
    public function setAddressBookFax($address_book_fax)
    {
        $this->container['address_book_fax'] = $address_book_fax;

        return $this;
    }

    /**
     * Gets address_book_phone
     *
     * @return string
     */
    public function getAddressBookPhone()
    {
        return $this->container['address_book_phone'];
    }

    /**
     * Sets address_book_phone
     *
     * @param string $address_book_phone Specifies customer's phone number in the address book
     *
     * @return $this
     */
    public function setAddressBookPhone($address_book_phone)
    {
        $this->container['address_book_phone'] = $address_book_phone;

        return $this;
    }

    /**
     * Gets address_book_phone_mobile
     *
     * @return string
     */
    public function getAddressBookPhoneMobile()
    {
        return $this->container['address_book_phone_mobile'];
    }

    /**
     * Sets address_book_phone_mobile
     *
     * @param string $address_book_phone_mobile Specifies customer's mobile phone number in the address book
     *
     * @return $this
     */
    public function setAddressBookPhoneMobile($address_book_phone_mobile)
    {
        $this->container['address_book_phone_mobile'] = $address_book_phone_mobile;

        return $this;
    }

    /**
     * Gets address_book_address1
     *
     * @return string
     */
    public function getAddressBookAddress1()
    {
        return $this->container['address_book_address1'];
    }

    /**
     * Sets address_book_address1
     *
     * @param string $address_book_address1 Specifies customer's first address in the address book
     *
     * @return $this
     */
    public function setAddressBookAddress1($address_book_address1)
    {
        $this->container['address_book_address1'] = $address_book_address1;

        return $this;
    }

    /**
     * Gets address_book_address2
     *
     * @return string
     */
    public function getAddressBookAddress2()
    {
        return $this->container['address_book_address2'];
    }

    /**
     * Sets address_book_address2
     *
     * @param string $address_book_address2 Specifies customer's second address in the address book
     *
     * @return $this
     */
    public function setAddressBookAddress2($address_book_address2)
    {
        $this->container['address_book_address2'] = $address_book_address2;

        return $this;
    }

    /**
     * Gets address_book_city
     *
     * @return string
     */
    public function getAddressBookCity()
    {
        return $this->container['address_book_city'];
    }

    /**
     * Sets address_book_city
     *
     * @param string $address_book_city Specifies customer's city in the address book
     *
     * @return $this
     */
    public function setAddressBookCity($address_book_city)
    {
        $this->container['address_book_city'] = $address_book_city;

        return $this;
    }

    /**
     * Gets address_book_country
     *
     * @return string
     */
    public function getAddressBookCountry()
    {
        return $this->container['address_book_country'];
    }

    /**
     * Sets address_book_country
     *
     * @param string $address_book_country ISO code or name of country
     *
     * @return $this
     */
    public function setAddressBookCountry($address_book_country)
    {
        $this->container['address_book_country'] = $address_book_country;

        return $this;
    }

    /**
     * Gets address_book_state
     *
     * @return string
     */
    public function getAddressBookState()
    {
        return $this->container['address_book_state'];
    }

    /**
     * Sets address_book_state
     *
     * @param string $address_book_state ISO code or name of state.
     *
     * @return $this
     */
    public function setAddressBookState($address_book_state)
    {
        $this->container['address_book_state'] = $address_book_state;

        return $this;
    }

    /**
     * Gets address_book_postcode
     *
     * @return string
     */
    public function getAddressBookPostcode()
    {
        return $this->container['address_book_postcode'];
    }

    /**
     * Sets address_book_postcode
     *
     * @param string $address_book_postcode Specifies customer's postcode
     *
     * @return $this
     */
    public function setAddressBookPostcode($address_book_postcode)
    {
        $this->container['address_book_postcode'] = $address_book_postcode;

        return $this;
    }

    /**
     * Gets address_book_tax_id
     *
     * @return string
     */
    public function getAddressBookTaxId()
    {
        return $this->container['address_book_tax_id'];
    }

    /**
     * Sets address_book_tax_id
     *
     * @param string $address_book_tax_id Add Tax Id
     *
     * @return $this
     */
    public function setAddressBookTaxId($address_book_tax_id)
    {
        $this->container['address_book_tax_id'] = $address_book_tax_id;

        return $this;
    }

    /**
     * Gets address_book_identification_number
     *
     * @return string
     */
    public function getAddressBookIdentificationNumber()
    {
        return $this->container['address_book_identification_number'];
    }

    /**
     * Sets address_book_identification_number
     *
     * @param string $address_book_identification_number The national ID card number of this person, or a unique tax identification number.
     *
     * @return $this
     */
    public function setAddressBookIdentificationNumber($address_book_identification_number)
    {
        $this->container['address_book_identification_number'] = $address_book_identification_number;

        return $this;
    }

    /**
     * Gets address_book_gender
     *
     * @return string
     */
    public function getAddressBookGender()
    {
        return $this->container['address_book_gender'];
    }

    /**
     * Sets address_book_gender
     *
     * @param string $address_book_gender Specifies customer's gender
     *
     * @return $this
     */
    public function setAddressBookGender($address_book_gender)
    {
        $this->container['address_book_gender'] = $address_book_gender;

        return $this;
    }

    /**
     * Gets address_book_alias
     *
     * @return string
     */
    public function getAddressBookAlias()
    {
        return $this->container['address_book_alias'];
    }

    /**
     * Sets address_book_alias
     *
     * @param string $address_book_alias Specifies customer's alias in the address book
     *
     * @return $this
     */
    public function setAddressBookAlias($address_book_alias)
    {
        $this->container['address_book_alias'] = $address_book_alias;

        return $this;
    }

    /**
     * Gets address_book_type
     *
     * @return string
     */
    public function getAddressBookType()
    {
        return $this->container['address_book_type'];
    }

    /**
     * Sets address_book_type
     *
     * @param string $address_book_type Specifies customer's address type
     *
     * @return $this
     */
    public function setAddressBookType($address_book_type)
    {
        $this->container['address_book_type'] = $address_book_type;

        return $this;
    }

    /**
     * Gets address_book_default
     *
     * @return bool
     */
    public function getAddressBookDefault()
    {
        return $this->container['address_book_default'];
    }

    /**
     * Sets address_book_default
     *
     * @param bool $address_book_default Defines whether the address is used by default
     *
     * @return $this
     */
    public function setAddressBookDefault($address_book_default)
    {
        $this->container['address_book_default'] = $address_book_default;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


